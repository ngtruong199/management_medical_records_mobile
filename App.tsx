import React from 'react';
import Home from './src/app/containers/home';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Chat from './src/app/containers/chat';
import Setting from './src/app/containers/setting';
import Notification from './src/app/containers/notfication';
import Tabs from './src/app/navigation/tabs';
import Login from './src/app/containers/auth/Login';
import Users from './src/app/containers/users/users';
import {useRef} from 'react';
import {navigationRef} from './src/app/navigation/rootNavigation';
import {Provider, useSelector} from 'react-redux';
import store, {dispatch} from './src/app/store';
import Medical_Request from './src/app/containers/users/Medical_Request';
import CustomAlertComponent from './src/app/ui/components/CustomAlert';
import Medicine_Request from './src/app/containers/users/Medicine_Request';
import {useEffect} from 'react';
import {getGeneralDataMedical} from './src/app/action/data_general';
import {firebaseAuth} from './src/app/config/firebaseConfig';
import RegisterUsers from './src/app/containers/users/RegisterUsers';
import Medical_List from './src/app/containers/users/Medical_List';
import CreateChat from './src/app/containers/chat/CreateChat';
import ChatUser from './src/app/containers/chat/ChatUser';
import {useState} from 'react';
import {getItemStorage, setItemStorage} from './src/app/storage';
import {typeStorage} from './src/app/storage/types';
import actionTypes from './src/app/action/types';
import {getUserProfile, setUserId, user_id} from './src/app/action/auth';
import {getAllChat} from './src/app/action/chat';
import {pushNotificationLocal} from './src/app/action/notification';
import {PersonalData} from './src/app/containers/setting/PersonalData';
import {Intro} from './src/app/containers/auth/Intro';
import {ListTypeName} from './src/app/containers/users/components/ListTypeName';
import Register from './src/app/containers/auth/Register';
import {BottomOption} from './src/app/ui/components/BottomOption';
import {SearchMedical} from './src/app/containers/users/Search';
import {ListDateMedicine} from './src/app/containers/users/components/ListDateMedicine';
import {requestCameraPermission} from './src/app/ui/components/SelectPhoto';
export type RootStackParamList = {
  Home: undefined;
  Chat: undefined;
  Notification: undefined;
  Setting: {
    item?: string;
  };
  Login: undefined;
};
const Stack = createStackNavigator<RootStackParamList>();
const App = () => {
  useEffect(() => {
    getGeneralDataMedical();
  }, []);
  return (
    <Provider store={store}>
      <Route />
    </Provider>
  );
};
const Route = () => {
  const [isLogin, setIsLogin] = useState(false);
  const idUser: any = useSelector(state => state.auth._id);
  useEffect(() => {
    setIsLogin(!!idUser);
  }, [idUser]);

  useEffect(() => {
    // getItemStorage(typeStorage.ID_USER, (item: any) => {
    //   getAllChat();
    //   setUserId(item);
    //   getUserProfile();
    //   setIsLogin(!!item);
    //   dispatch({type: actionTypes.LOGIN_SUCCESS, payload: {_id: item}});
    // });
  }, []);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{headerShown: false, gestureDirection: 'horizontal'}}>
        <Stack.Screen name={'Intro'} component={Intro} />
        <Stack.Screen name={'Login'} component={Login} />
        <Stack.Screen name={'Register'} component={Register} />
        <Stack.Screen name={'Chat'} component={Tabs} />
        <Stack.Screen name={'Notification'} component={Tabs} />
        <Stack.Screen name={'Setting'} component={Tabs} />
        <Stack.Screen name={'Users'} component={Tabs} />
        <Stack.Screen name={'Register_User'} component={RegisterUsers} />
        <Stack.Screen name={'Medical_List'} component={Medical_List} />
        <Stack.Screen name={'ListTypeName'} component={ListTypeName} />
        <Stack.Screen name={'Medical_Request'} component={Medical_Request} />
        <Stack.Screen name={'CreateChat'} component={CreateChat} />
        <Stack.Screen name={'Medicine_Request'} component={Medicine_Request} />
        <Stack.Screen name={'ChatUser'} component={ChatUser} />
        <Stack.Screen name={'PersonalData'} component={PersonalData} />
        <Stack.Screen name={'SearchMedical'} component={SearchMedical} />
        <Stack.Screen name={'ListDateMedicine'} component={ListDateMedicine} />

        {/* <Stack.Screen name={'Home'} component={Tabs} /> */}
      </Stack.Navigator>
      <CustomAlertComponent />
      <BottomOption />
    </NavigationContainer>
  );
};
export default App;
