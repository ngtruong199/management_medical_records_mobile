# Management Medical Records Mobile 

# enviroment 
- install NodeJS: (search, download and install)
- install React-native-cli: npm i -g react-native-cli
- install cocoapod (iOS only): brew install cocoapod
- install Android Studio (android only): (search, download and install)
- install jdk (android only): (search, download and install)
- install XCode (iOS only): via apple app store

# steps to build for debug
1. npm install
2. build to android:
   - run simulator or connect real device by cable
   - react-native run-android: to run debug on device
   - npm run android: to build apk debug
   - debug no host: 
      + react-native bundle --dev false --platform android --entry-file index.js --bundle-output ./android/app/src/main/assets/index.android.bundle --assets-dest ./android/app/src/main/res
      + cd android
      + ./gradlew app:assembleDebug
3. build to iOS:
   - cd ios
   - pod install
   - cd ..
   - react-native run-ios --simulator="iPhone X"

# build release mode
- Android: npm run android-release
- iOS: build via XCode


# build enviroment
# DEV MODE
- run: sh copyAssetsCustomer dev
- run: sh copyAssetsDriver dev
# STAGING MODE
- run: sh copyAssetsCustomer stg
- run: sh copyAssetsDriver stg

# PRODUCTION MODE
- run: sh copyAssetsCustomer prod
- run: sh copyAssetsDriver prod