import {
  firebase,
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';
import moment from 'moment';
import 'moment/locale/vi';

export const locate_Date = 'vi-VN';
export const time_now = firebase.firestore.Timestamp.fromDate(
  moment().toDate(),
);
export const toTimeStamp = (date: Date) =>
  firebase.firestore.Timestamp.fromDate(date);
export const displayStringDate = (
  times_tamp?: FirebaseFirestoreTypes.Timestamp | null,
  mode?: 'date' | 'datetime' | 'time',
) => {
  if (!times_tamp) return '';
  mode = !mode ? 'datetime' : mode;
  const date = mode?.includes('date') && times_tamp?.toDate();
  const time = mode?.includes('time') && times_tamp?.toDate();

  return times_tamp
    ? `${date ? moment(date).format('l') : ''}${
        time ? `   ${moment(time).format('LT')}` : ''
      }`
    : '';
};
export const toTimeCompare = (
  _date: Date | FirebaseFirestoreTypes.Timestamp,
  isStart = false,
) => {
  const date: Date =
    _date.constructor.name === 'Date'
      ? _date
      : (_date as FirebaseFirestoreTypes.Timestamp).toDate();

  isStart ? date.setHours(0, 0, 0, 0) : date.setHours(23, 59, 59, 0);
  return date;
};
export const toMoment = (time_stamp?: FirebaseFirestoreTypes.Timestamp) =>
  time_stamp && moment(toDate(time_stamp));
export const toDate = (times_tamp?: FirebaseFirestoreTypes.Timestamp) =>
  times_tamp?.toDate() || time_now.toDate();
export const formatDate = (date: Date) => {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  let strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

export const isBeforeDay_Now = (
  date: Date | FirebaseFirestoreTypes.Timestamp,
) => moment().isAfter(new Date(date.toString()));
