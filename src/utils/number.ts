export const formatValidPhoneNumber = (phoneNum?: string) => {
  phoneNum && phoneNum[0] === '0' && (phoneNum = phoneNum.substring(1));
  return phoneNum && '+84' + phoneNum;
};
export const validatePhoneNumber = (phoneNum?: string) => {
  var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/;
  return phoneNum && regexp.test(phoneNum);
};
export function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
    /[xy]/g,
    function (c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
    },
  );
  return uuid;
}
