import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {windowHeight, windowWidth} from '../../../utils/platform';
import {showLoading} from '../../action/alert';
import {getUserProfile, setUserId, updateLogin} from '../../action/auth';
import actionTypes from '../../action/types';
import {firebaseAuth} from '../../config/firebaseConfig';
import {navigate} from '../../navigation/rootNavigation';
import {setItemStorage} from '../../storage';
import {typeStorage} from '../../storage/types';
import {dispatch} from '../../store';
import {Container, TextMedium, TouchWhite} from '../../ui/themes/styles';

interface Props {}

export const Intro = (props: Props) => {
  useEffect(() => {
    const listenerAuth = firebaseAuth.onAuthStateChanged(_user => {
      _user && updateLogin(_user);
    });
    return () => listenerAuth();
  }, []);
  return (
    <Container>
      <ImageView source={require('../../../../assets/imageStart.png')} />
      <TouchWhite
        onPress={() => navigate('Login')}
        style={{width: windowWidth * 0.9}}>
        <TextButton>Đăng Nhập</TextButton>
      </TouchWhite>
      <TouchWhite
        onPress={() => navigate('Register')}
        style={{width: windowWidth * 0.9}}>
        <TextButton>Đăng Kí</TextButton>
      </TouchWhite>
    </Container>
  );
};
const ImageView = styled.Image`
  width: ${windowWidth}px;
  height: ${windowHeight * 0.7}px;
`;
const TextButton = styled(TextMedium)`
  margin: 8px;
  font-weight: bold;
  color: #333;
`;
