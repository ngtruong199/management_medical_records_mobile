import React from 'react';
import styled from 'styled-components/native';
import InputText from '../../ui/components/InputText';
import {
  CenterContainer,
  RoundViewWithoutBorder,
  TextMedium,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import RoundTextButton from '../../ui/components/RoundTextButton';
import GoogleIcon from '../../ui/icons/GoogleIcon';
import IconsButtonText from '../../ui/components/IconsButtonText';
import FacebookIcon from '../../ui/icons/FacebookIcon';
import {
  loginAuth,
  verifyCodeLogin,
  writeAuthFirestore,
  logoutAuth,
  setUserId,
  getUserProfile,
  signIn,
  updateLogin,
} from '../../action/auth';
import {SeverAuthType} from '../../store/types/auth';
import {getString} from '../../locate';
import withKeyboardAwareView from '../../ui/hoc/withKeyboardAwareView';
import {Animated, View} from 'react-native';
import {connect} from 'react-redux';
import Header from '../../ui/components/Header';
import {navigate} from '../../navigation/rootNavigation';
import {windowHeight} from '../../../utils/platform';
import getIconWithName from '../../ui/utils/Icons';
import {showAlert} from '../../action/alert';
import {FirebaseAuthTypes} from '@react-native-firebase/auth';
import {setItemStorage} from '../../storage';
import {typeStorage} from '../../storage/types';
import {dispatch} from '../../store';
import actionTypes from '../../action/types';
import {firebaseAuth} from '../../config/firebaseConfig';

interface Props {
  logoutAuth: () => void;
  loginAuth: () => void;
}

interface State {
  phone_num: string;
  verify_code: string;
  userId: string;
  confirmLogin: FirebaseAuthTypes.ConfirmationResult;
}
class RegisterComponent extends React.Component<Props, State> {
  onTextChangeField(value: string, field?: string) {
    field && this.setState({[field]: value} as any);
  }
  listenerAuth = null;
  componentDidMount() {
    this.listenerAuth = firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        updateLogin(user);
      } else {
        this.setState({
          phone_num: '',
          confirmLogin: null,
        });
      }
    });
  }
  componentWillUnmount() {
    this.listenerAuth();
  }
  pressLogin() {
    const callback = (confirmLogin: FirebaseAuthTypes.ConfirmationResult) => {
      this.setState({confirmLogin});
    };
    loginAuth({phoneNumber: this.state?.phone_num, callback});
  }
  handleVerifyCode = () => {
    const {verify_code, confirmLogin} = this.state;
    if (verify_code.length === 6) {
      confirmLogin
        .confirm(verify_code)
        .then(_user => {
          updateLogin(_user.user);
        })
        .catch(err => {
          showAlert('Login', 'Mã xác nhận không chính xác');
          console.log(err);
        });
    } else {
      alert('Please enter a 6 digit OTP code.');
    }
  };
  render() {
    const {phone_num, verify_code, confirmLogin} = this.state || {};
    const disableButton: boolean = confirmLogin ? !verify_code : !phone_num;
    const pressNextButton = !confirmLogin
      ? this.pressLogin.bind(this)
      : this.handleVerifyCode.bind(this);
    return (
      <Container style={{backgroundColor: '#E6EAF5'}}>
        <Header text={'Đăng Kí'} isCenter />
        <ContainerImage>
          <RoundImage source={require('../../../../assets/logo.jpeg')} />
        </ContainerImage>

        <RoundOption>
          <InputText
            placeholder={'0123456789'}
            inputType={'number-pad'}
            label={'Nhập Số điện thoại'}
            value={phone_num}
            fieldName={'phone_num'}
            iconName={'graduation-cap'}
            onChangeText={this.onTextChangeField.bind(this)}
            disable={confirmLogin}
          />
          {confirmLogin && (
            <View style={{justifyContent: 'center'}}>
              <InputText
                inputType={'number-pad'}
                label={'Verify Code'}
                value={verify_code}
                fieldName={'verify_code'}
                iconName={'lock'}
                onChangeText={this.onTextChangeField.bind(this)}
              />
              <TouchIconCancel
                onPress={() => this.onTextChangeField(null, 'confirmLogin')}>
                {getIconWithName({
                  name: 'times-circle',
                  size: 30,
                  color: 'red',
                })}
              </TouchIconCancel>
            </View>
          )}
          <RoundTextButton
            text={
              confirmLogin
                ? getString('auth', 'verifyCode')
                : getString('auth', 'sendCode')
            }
            disable={disableButton}
            onPress={pressNextButton}
          />
          <TextMedium style={{color: 'red'}}>OR</TextMedium>
          <IconsButtonText
            onPress={() => signIn()}
            style={{
              backgroundColor: '#fff',
            }}
            Icon={GoogleIcon}
            styleText={{color: '#AEAEAF'}}
            text={'Sign in with Google'}
          />
        </RoundOption>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: state.auth.user,
});

const mapDispatchToProps = {
  logoutAuth,
  loginAuth,
};
const Register = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withKeyboardAwareView(RegisterComponent));
export default Register;
const TouchIconCancel = styled.TouchableOpacity`
  position: absolute;
  right: 10px;
  padding: 5px;
`;
const Container = styled(CenterContainer)`
  margin: 0;
  padding: 0;
  background-color: #4d627a;
`;
const RoundOption = styled(RoundViewWithoutBorder)`
  padding-horizontal: 20px;
  margin-top: auto;
  flex: 0.65;
  padding-vertical: 30px;
`;
const ContainerImage = styled.View`
  flex: 0.35;
  align-items: center;
  justify-content: center;
`;
const RoundImage = styled.Image`
  width: 150px;
  height: 150px;
  border-radius: 75px;
`;
