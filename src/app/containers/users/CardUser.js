import React from 'react';
import {View, Text} from 'react-native';
import styled from 'styled-components/native';
import {windowHeight, windowWidth} from '../../../utils/platform';
import ArrowRight from '../../ui/icons/ArrowRight';
import PlusIcon from '../../ui/icons/PlusIcon';
import _ from 'lodash';
import {
  ShadowString,
  TextMedium,
  TextSmall,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import {navigate} from '../../navigation/rootNavigation';
import {deleteUser, updateCurrentUser} from '../../action/auth-management';
import {UserItem} from '../../action/auth-management';
import {displayStringDate} from '../../../utils/date';
import {showBottomOption} from '../../action/bottom-option';
import {showAlert} from '../../action/alert';
const imageSize = windowWidth / 5;
const CardUser = (item: UserItem) => {
  const {avatarRender, name, age, id, note, gender, birthday} = item;
  const onPressCard = () => {
    _.isEmpty(item) ? navigate('Register_User') : updateCurrentUser(id);
  };
  const isEmpty = _.isEmpty(item);
  return isEmpty ? (
    <RowContainer
      onPress={onPressCard}
      style={{justifyContent: 'center', alignItems: 'center'}}>
      <PlusIcon width={70} height={70} />
    </RowContainer>
  ) : (
    <RowContainer
      onLongPress={() =>
        showBottomOption([
          {
            onPress: () => navigate('Register_User', {user: item}),
            option: 'edit',
          },
          {
            onPress: () => deleteUser(id),
            option: 'delete',
          },
        ])
      }
      onPress={onPressCard}>
      <ImageItem
        source={
          !_.isEmpty(avatarRender)
            ? avatarRender
            : require('../../../../assets/user.png')
        }
      />
      <ContentContainer>
        <TextTitleName numberOfLines={1}>{name}</TextTitleName>
        <InfoText>{`${gender} - ${displayStringDate(
          birthday,
          'date',
        )}`}</InfoText>
        <NoteText>{note}</NoteText>
      </ContentContainer>
      <ArrowRight style={{marginLeft: 'auto'}} width={30} height={100} />
    </RowContainer>
  );
};
const RowContainer = styled(TouchWhiteNoShadow)`
  align-self: center;
  min-height: ${windowHeight / 8}px;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: #f6f6f8;
  ${ShadowString}
`;
// const
const TextTitleName = styled(TextMedium)`
  font-size: 20px;
  flex-wrap: wrap;
  color: #121;
`;
const InfoText = styled(TextSmall)`
  font-size: 14px;
  flex-wrap: wrap;
  color: #444;
  margin-top: 5px;
  font-weight: 500;
`;
const NoteText = styled(InfoText)`
  font-size: 12px;
  color: #777;
`;
const ContentContainer = styled.View`
  flex: 0.9;
  margin-left: 10px;
  align-items: flex-start;
`;
const ImageItem = styled.Image`
  width: ${imageSize}px;
  height: ${imageSize}px;
  border-radius: ${imageSize / 2}px;
  border-width: 1px;
  border-color: #888;
`;

export default CardUser;
