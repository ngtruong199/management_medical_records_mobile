import React, {Component, PureComponent, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {
  safeAreaHeight,
  safeBottomPadding,
  windowWidth,
} from '../../../utils/platform';
import {getString} from '../../locate';
import DatePicked, {ListDatePicked} from '../../ui/components/Date_Picked';
import InputField from '../../ui/components/InputField';
import RoundTextButton from '../../ui/components/RoundTextButton';
import AmbulanceIcon from '../../ui/icons/AmbulanceIcon';
import PillsIcons from '../../ui/icons/PillsIcons';
import NoteIcon from '../../ui/icons/NoteIcons';
import {getStyleWrap, _numberStyled} from '../../ui/themes/dimens';
import {
  containerStyle,
  Description,
  RowArea,
  ScrollViewContainer,
  TextLarge,
  Title,
  TitleInput,
  TouchWhite,
} from '../../ui/themes/styles';
import Header from '../../ui/components/Header';
import {
  createMedical,
  Profile,
  updateDataProfile,
  updateMedical,
} from '../../action/medical-profile';
import {connect} from 'react-redux';
import CardMedical from './CardMedical';
import {getCardNumber, listCard_UI} from '../../ui/imagePath';
import BottomPopup from '../../ui/components/BottomPopup';
import {FlatList} from 'react-native';
import {time_now} from '../../../utils/date';
import {navigate} from '../../navigation/rootNavigation';
import DropdownIcon from '../../animation/DropdownIcon';
import {
  getListDisease,
  _general,
  StatusRequest,
} from '../../action/data_general';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
// import InputAddress from '../../ui/components/InputAddress';

interface Props {
  current_medical: Profile;
  route: Route;
}
interface InSideState {
  openChoose?: boolean;
  isEdit?: boolean;
  listMedicalName?: string[];
  medicine_start: string;
  medicine_end: string;
}
export interface Route {
  key: string;
  name: string;
  params?: Params;
}

export interface Params {
  isEdit: boolean;
  typeDisease: string;
  current_medical: Profile;
}
type State = InSideState & Profile;
export class Medical_Request extends PureComponent<Props, State> {
  state: State = {
    openChoose: false,
    select_card: Math.floor(Math.random() * 11 + 1),
    date_create: time_now,
    date_re_examination: [],
    listMedicalName: [],
    medicine_time: [],
    medicine_start: '',
    medicine_end: '',
  };
  onPressSubmit = () => {
    const {isEdit, openChoose, listMedicalName, ...res} = this.state;
    const {current_medical} = this.props;
    updateMedical({profile: res, oldProfile: current_medical});
  };
  fillEditComponent() {
    const {current_medical, route} = this.props;
    const {
      isEdit,
      typeDisease,
      current_medical: _current_medical,
    } = route?.params || {};
    typeDisease && this.setFieldState('type_name', typeDisease);
    isEdit && _current_medical && this.setState({..._current_medical, isEdit});
    isEdit && current_medical && this.setState({...current_medical, isEdit});
  }

  componentDidMount() {
    this.fillEditComponent();
  }
  componentDidUpdate(prevProps: Props) {
    if (prevProps != this.props) {
      this.fillEditComponent();
    }
  }
  setFieldState = (
    field: string,
    value:
      | string
      | boolean
      | FirebaseFirestoreTypes.Timestamp
      | Profile
      | number,
  ) => {
    this.setState({[field]: value});
  };
  setStateObject = (state: State) => {
    this.setState(state);
  };
  render() {
    const {
      type_name,
      clinic_address,
      date_create,
      date_re_examination,
      disease_name,
      doctor_name,
      doctor_phone,
      select_card,
      openChoose,
      isEdit,
    } = this.state;
    return (
      <Container>
        <ScrollViewContainer
          nestedScrollEnabled={true}
          contentContainerStyle={containerStyle}
          style={{paddingBottom: 0, paddingTop: 5}}>
          <Header text={getString('medical', 'title_Input')} />
          <Description>{getString('medical', 'description_Input')}</Description>
          <CardMedical
            onPress={() => this.setFieldState('openChoose', !openChoose)}
            source={getCardNumber(select_card)}
            title={type_name}
            textName={disease_name}
            date_create={date_create}
            date_re_examination={
              (date_re_examination?.length && date_re_examination[0]) || null
            }
          />
          <InputField
            dropdownData={_general.type_disease}
            value={type_name}
            onChangeText={(text: string) =>
              this.setFieldState('type_name', text)
            }
            onSubmitDropdown={value => {
              const callback = (listMedicalName: string[]): void => {
                this.setStateObject({listMedicalName, type_name: value});
              };
              getListDisease(value, callback);
            }}
            label={getString('medical', 'type_name_disease')}
          />
          <TitleInput>
            {getString('medical', 'date_create').toUpperCase()}
          </TitleInput>
          <DatePicked
            isLastTimeNow
            mode={'date'}
            date={date_create}
            setDate={(text: any) => this.setFieldState('date_create', text)}
          />
          <InputField
            value={disease_name}
            onChangeText={value => {
              this.setFieldState('disease_name', value);
            }}
            onSubmitDropdown={value => {
              this.setFieldState('disease_name', value);
            }}
            dropdownData={_general.name_disease}
            label={getString('medical', 'name_disease')}
          />
          {/* <InputAddress /> */}
          <InputField
            value={clinic_address}
            onChangeText={(value: any) => {
              this.setFieldState('clinic_address', value);
            }}
            label={getString('medical', 'clinic_address')}
          />
          <InputField
            value={doctor_name}
            onChangeText={(value: any) => {
              this.setFieldState('doctor_name', value);
            }}
            label={getString('medical', 'doctor_name')}
          />
          <InputField
            isNumber
            value={doctor_phone}
            onChangeText={(value: any) => {
              this.setFieldState('doctor_phone', value);
            }}
            label={getString('medical', 'doctor_phone_num')}
          />

          <ListDatePicked
            title={getString('medical', 'date_re_examination')}
            listDate={date_re_examination}
            onSetDate={(value: any) => {
              this.setFieldState('date_re_examination', value);
            }}
          />
          {isEdit && (
            <>
              <Title>{getString('medical', 'title_need')}</Title>
              <RowArea>
                <TouchWhite
                  onPress={() => navigate('Medicine_Request')}
                  style={styleItemPill}>
                  <PillsIcons {...sizeItemPill} />
                </TouchWhite>
                <TouchWhite
                  onPress={() => navigate('ListDateMedicine')}
                  style={styleItemPill}>
                  <NoteIcon {...sizeItemPill} />
                </TouchWhite>
                <TouchWhite style={styleItemPill}>
                  <AmbulanceIcon {...sizeItemPill} />
                </TouchWhite>
              </RowArea>
            </>
          )}
          <CreateButton
            onPress={this.onPressSubmit}
            disable={!type_name || !disease_name}
            text={getString(
              'medical',
              isEdit ? 'save' : 'create_medical',
            ).toUpperCase()}
            textSize={'LARGE'}
          />
        </ScrollViewContainer>

        {openChoose && (
          <BottomPopup
            visible={openChoose}
            onToggle={opened => this.setFieldState('openChoose', opened)}>
            <FlatList
              contentContainerStyle={{
                marginBottom: safeBottomPadding + 5,
              }}
              horizontal
              data={listCard_UI}
              renderItem={({item, index}) => (
                <CardMedical
                  onPress={() => {
                    this.setFieldState('select_card', index + 1);
                    this.setFieldState('openChoose', false);
                  }}
                  style={{marginRight: 10}}
                  source={item}
                  title={type_name}
                  textName={disease_name}
                  textDateCreate={date_create}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </BottomPopup>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  current_medical: state.medical_profile.current_medical,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Medical_Request);

const sizeItemPill = {width: 60, height: 60};
const numberItemPills = 3;
const styleItemPill = getStyleWrap(numberItemPills);
const Container = styled.View`
  flex: 1;
`;
const CenterView = styled.View`
  justify-content: center;
`;
const CreateButton = styled(RoundTextButton)`
  margin-top: auto;
  border-radius: 10px;
  margin-bottom: ${safeBottomPadding}px;
`;
