import React, {useEffect, useState} from 'react';
import {Text} from 'react-native-svg';
import styled from 'styled-components/native';
import {windowWidth} from '../../../utils/platform';
import Header from '../../ui/components/Header';
import InputField from '../../ui/components/InputField';
import ScrollContainer from '../../ui/components/ScrollContainer';
import SelectPhoto from '../../ui/components/SelectPhoto';
import _ from 'lodash';
import withKeyboardAwareView from '../../ui/hoc/withKeyboardAwareView';
import {registerUserMedicalProfile} from '../../action/auth-management';
import DatePicked from '../../ui/components/Date_Picked';
import {isBeforeDay_Now, time_now} from '../../../utils/date';
import {_general} from '../../action/data_general';

import {UserItem} from '../../action/auth-management';
interface Props {
  route: {
    params: {
      user: UserItem,
    },
  };
}
const RegisterUsers = (props: Props) => {
  useEffect(() => {
    if (props?.route?.params?.user) {
      const {name, avatar, birthday, gender, note, avatarRender, id} =
        props.route.params.user;
      setName(name);
      setAvatar(avatar);
      setBirthday(birthday);
      setNote(note);
      setGender(gender);
      setAvatar(avatarRender);
      console.log(props?.route?.params?.user);
    }
  }, [props.route?.params]);
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  const [birthday, setBirthday] = useState(time_now);
  const [note, setNote] = useState('');
  const [gender, setGender] = useState('Nam');
  const [openChooseAvatar, setChooseAvatar] = useState(false);
  const onRegister = () => {
    if (!name || !isBeforeDay_Now(birthday?.toDate())) return;
    registerUserMedicalProfile({
      id: props?.route?.params?.user?.id,
      name,
      avatar,
      birthday,
      gender,
      note,
    });
  };
  return (
    <ScrollContainer
      style={{flex: 1}}
      button={{text: 'Tạo Thành Viên', onPress: onRegister}}>
      <Header text={'Tạo Thành Viên'} />
      <TouchImage onPress={() => setChooseAvatar(true)}>
        <ImageView
          source={
            !_.isEmpty(avatar) ? avatar : require('../../../../assets/user.png')
          }
        />
      </TouchImage>
      <InputField
        checkError={'NotEmpty'}
        label={'Tên người dùng'}
        value={name}
        onChangeText={setName}
      />
      <InputField
        label={'Giới tính'}
        isEdit={false}
        dropdownData={_general.gender}
        value={gender}
        onSubmitDropdown={setGender}
      />
      <DatePicked
        isLastTimeNow
        mode={'date'}
        title={'Ngày sinh nhật'}
        date={birthday}
        setDate={setBirthday}
      />
      <InputField
        numberOfLines={3}
        minHeight={100}
        label={'Ghi chú'}
        value={note}
        onChangeText={setNote}
      />
      <SelectPhoto
        visible={openChooseAvatar}
        onDismiss={() => setChooseAvatar(false)}
        onSelectPhoto={item => setAvatar(item)}
      />
    </ScrollContainer>
  );
};
const TouchImage = styled.TouchableOpacity`
  align-self: center;
`;
const ImageView = styled.Image`
  width: ${windowWidth / 3}px;
  height: ${windowWidth / 3}px;
  border-radius: ${windowWidth / 3 / 2}px;
`;
export default withKeyboardAwareView(RegisterUsers);
