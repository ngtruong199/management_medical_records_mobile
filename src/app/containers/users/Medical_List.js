import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {windowHeight, windowWidth} from '../../../utils/platform';
import {hideAlert, showLoading} from '../../action/alert';
import {
  current_id_medical,
  current_type_name,
  getAllCardMedical,
  getFilterCardPrescription,
  setCurrent_Id_Medical,
  updateCurrentMedical,
} from '../../action/medical-profile';
import {navigate} from '../../navigation/rootNavigation';
import {getCardNumber} from '../../ui/imagePath';
import {
  ScrollContainer,
  TextName,
  ViewCard,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import CardMedical from './CardMedical';
import Header from '../../ui/components/Header';
import getIconWithName from '../../ui/utils/Icons';
import {currentID_User_Profile} from '../../action/auth-management';
import {TouchPlus} from '../../ui/themes/styles';

const Medical_List = ({
  getAllCardMedical,
  medicals_card,
  updateCurrentMedical,
  route,
}) => {
  const [listCard, setListCard] = useState([{}]);
  useEffect(() => {
    if (route?.params?.isFilter) return;
    showLoading();
    getAllCardMedical();
  }, []);
  useEffect(() => {
    setListCard(medicals_card);
  }, [medicals_card]);
  const renderItem = ({item}) =>
    typeof item === 'string' ? (
      <ViewCard>
        <TextName>{item}</TextName>
      </ViewCard>
    ) : (
      <CardMedical
        style={{alignSelf: 'center'}}
        onPress={() => {
          setCurrent_Id_Medical(item.id);
          updateCurrentMedical(item.id);
          navigate('Medical_Request', {isEdit: true});
        }}
        id={item.id}
        source={getCardNumber(item?.select_card)}
        title={item?.type_name}
        textName={item?.disease_name}
        date_create={item?.date_create}
        date_re_examination={
          (item?.date_re_examination?.length && item?.date_re_examination[0]) ||
          null
        }
      />
    );
  return (
    <>
      <ScrollContainer
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: 50,
          paddingHorizontal: 20,
        }}
        data={listCard}
        ListHeaderComponent={<Header text={'Danh Sách Hồ Sơ'}></Header>}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
      {!route?.params?.isFilter && (
        <TouchPlus
          onPress={() =>
            navigate('Medical_Request', {typeDisease: current_type_name})
          }>
          {getIconWithName({name: 'plus', size: 50, color: '#1B9768'})}
        </TouchPlus>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  medicals_card: state.medical_profile.medicals,
});

const mapDispatchToProps = {
  getAllCardMedical,
  updateCurrentMedical,
};

export default connect(mapStateToProps, mapDispatchToProps)(Medical_List);
