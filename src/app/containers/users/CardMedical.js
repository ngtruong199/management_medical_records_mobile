import React from 'react';
import {View, Text} from 'react-native';
import styled from 'styled-components/native';
import {displayStringDate, time_now} from '../../../utils/date';
import {windowHeight, windowWidth} from '../../../utils/platform';
import {showBottomOption} from '../../action/bottom-option';
import {deleteMedical} from '../../action/medical-profile';
import {getString} from '../../locate';
import {navigate} from '../../navigation/rootNavigation';
import {getDateString} from '../../ui/components/Date_Picked';
import {_numberStyled} from '../../ui/themes/dimens';
import {ShadowString} from '../../ui/themes/styles';
import getIconWithName from '../../ui/utils/Icons';
import _ from 'lodash';

const CardMedical = ({
  onPress,
  source,
  title,
  textName,
  date_create = time_now,
  date_re_examination = time_now,
  style,
  id,
}) => {
  if (!source) {
    return (
      <TouchCard
        style={style}
        activeOpacity={0.5}
        isEmpty
        onPress={() => navigate('Medical_Request')}>
        {getIconWithName({name: 'plus-circle', size: 90, color: '#bbb'})}
      </TouchCard>
    );
  }
  return (
    <TouchCard
      onLongPress={() =>
        showBottomOption([
          {
            onPress: () => deleteMedical(id),
            option: 'delete',
          },
        ])
      }
      activeOpacity={0.7}
      onPress={onPress}
      style={style}>
      <TextTitle>{title?.toUpperCase()}</TextTitle>
      <TextName>{textName || ''}</TextName>
      <TextDate>{`${getString('medical', 'date_create')}: ${displayStringDate(
        date_create,
        'datetime',
      )}`}</TextDate>
      {!_.isEmpty(date_re_examination) ? (
        <TextDate>{`${getString(
          'medical',
          'date_re_examination',
        )}: ${displayStringDate(date_re_examination, 'datetime')}`}</TextDate>
      ) : null}
      <ImageCard style={style} source={source} />
    </TouchCard>
  );
};

export default CardMedical;

const TouchCard = styled.TouchableOpacity`
  margin-vertical: 15px;
  ${ShadowString};
  width: ${windowWidth * 0.9}px;
  height: ${windowHeight / 4}px;
  border-radius: 20px;
  padding-horizontal: 15px;
  padding-vertical: 15px;
  background-color: #e1fcf6;
  align-items: ${({isEmpty}) => (isEmpty ? 'center' : 'flex-start')};
  justify-content: ${({isEmpty}) => (isEmpty ? 'center' : 'flex-start')}; ;
`;
const TextTitle = styled.Text`
  font-size: ${_numberStyled['f-title']};
  color: white;
  font-weight: bold;
`;
const TextName = styled.Text`
  font-size: 18px;
  color: #67d2a2;
  font-weight: bold;
  margin-bottom: 5px;
`;
const TextDate = styled.Text`
  font-size: ${_numberStyled['f-m']};
  color: #eee;
  font-weight: 500;
  font-style: italic;
`;
const ImageCard = styled.Image`
  border-radius: 20px;
  position: absolute;
  width: ${windowWidth * 0.9}px;
  height: ${windowHeight / 4}px;
  z-index: -1;
`;
