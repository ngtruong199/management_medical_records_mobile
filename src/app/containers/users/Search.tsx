import React, {useEffect, useState} from 'react';
import {TextInputProps} from 'react-native';
import {add} from 'react-native-reanimated';
import styled from 'styled-components/native';
import {time_now, toDate} from '../../../utils/date';
import {safeBottomPadding, windowWidth} from '../../../utils/platform';
import {getListDisease, _general} from '../../action/data_general';
import {getString} from '../../locate';
import DatePicked from '../../ui/components/Date_Picked';
import Header from '../../ui/components/Header';
import InputField from '../../ui/components/InputField';
import ScrollContainer from '../../ui/components/ScrollContainer';
import SelectPhoto from '../../ui/components/SelectPhoto';
import {
  RowArea,
  RowView,
  ShadowString,
  Title,
  TitleInput,
  Touch,
  TouchWhite,
} from '../../ui/themes/styles';
import _ from 'lodash';
import {AvatarProfileDefault} from '../../ui/imagePath';
import {Profile, updateProfile} from '../../action/auth';
import {useSelector} from 'react-redux';
import moment from 'moment';
import {
  getAllCardMedicalNoDP,
  getFilterCardMedical,
} from '../../action/medical-profile';
import {popNavigate} from '../../navigation/rootNavigation';
import getIconWithName from '../../ui/utils/Icons';
import {getListUser} from '../../action/auth-management';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';

interface Props {}

export const SearchMedical = (props: Props) => {
  const [name, setName] = useState('');
  const [dateStart, setDateStart] =
    useState<FirebaseFirestoreTypes.Timestamp>();
  const [dateEnd, setDateEnd] = useState<FirebaseFirestoreTypes.Timestamp>();
  const [listUser, setListUser] = useState({});
  const [userName, setUserName] = useState('');
  const [typeMedical, setTypeMedical] = useState('');
  const [diseaseName, setDiseaseName] = useState('');
  const [startMedicine, setStartMedicine] =
    useState<FirebaseFirestoreTypes.Timestamp>();
  const [endMedicine, setEndMedicine] =
    useState<FirebaseFirestoreTypes.Timestamp>();

  useEffect(() => {
    getListUser(setListUser);
  }, []);
  const onPressUpdate = () => {
    getFilterCardMedical({
      dateStart: dateStart?.toDate(),
      dateEnd: dateEnd?.toDate(),
      user_id: userName && listUser[userName],
      type_name: typeMedical,
      endMedicine: endMedicine,
      startMedicine: startMedicine,
      diseaseName,
    });
  };
  return (
    <ScrollContainer
      button={{text: 'Tìm kiếm', onPress: onPressUpdate}}
      styleContainer={{paddingHorizontal: 10, paddingBottom: safeBottomPadding}}
      renderTop={() => (
        <Header text="Tìm kiếm hồ sơ">
          <Touch
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 'auto',
            }}
            onPress={() => {
              getAllCardMedicalNoDP();
              popNavigate();
            }}>
            {getIconWithName({name: 'clear', size: 30})}
          </Touch>
        </Header>
      )}>
      <InputField
        placeholder="Tất cả mọi người"
        dropdownData={Object.keys(listUser)}
        value={userName}
        onSubmitDropdown={setUserName}
        isEdit={false}
        label={'Tên thành viên'}
        width={windowWidth * 0.85}
      />
      <TitleInput style={{color: '#777', fontWeight: '800'}}>
        *Lọc theo Hồ sơ bệnh án
      </TitleInput>
      <InputField
        dropdownData={_general.type_disease}
        value={typeMedical}
        onChangeText={setTypeMedical}
        onSubmitDropdown={value => {
          getListDisease(value, () => setTypeMedical(value));
        }}
        label={getString('medical', 'type_name_disease')}
        width={windowWidth * 0.85}
      />
      <InputField
        dropdownData={_general.name_disease}
        value={diseaseName}
        onChangeText={setDiseaseName}
        onSubmitDropdown={setDiseaseName}
        label={getString('medical', 'name_disease')}
        width={windowWidth * 0.85}
      />
      <TitleInput>Ngày Tạo</TitleInput>
      <RowView style={{alignItems: 'center'}}>
        <DatePicked mode={'date'} setDate={setDateStart} date={dateStart} />
        <TitleInput style={{color: '#777', fontWeight: '800', marginLeft: 15}}>
          -&gt;
        </TitleInput>
        <DatePicked
          style={{marginLeft: 20}}
          mode={'date'}
          setDate={setDateEnd}
          date={dateEnd}
        />
      </RowView>
      <TitleInput>Ngày Uống Thuốc</TitleInput>
      <RowView style={{alignItems: 'center'}}>
        <DatePicked
          mode={'date'}
          setDate={setStartMedicine}
          date={startMedicine}
        />
        <TitleInput style={{color: '#777', fontWeight: '800', marginLeft: 15}}>
          -&gt;
        </TitleInput>
        <DatePicked
          style={{marginLeft: 20}}
          mode={'date'}
          setDate={setEndMedicine}
          date={endMedicine}
        />
      </RowView>
    </ScrollContainer>
  );
};
const Input = (props: TextInputProps & {title?: string}) => {
  return (
    <ViewEdt>
      <TitleInput>{props.title}</TitleInput>
      <TextInp {...props} placeholderTextColor="#555" />
    </ViewEdt>
  );
};
const TouchAvatar = styled.TouchableOpacity`
  align-self: center;
  ${ShadowString}
  margin-bottom: 20px;
`;
const ViewEdt = styled.View`
  margin-bottom: 20px;
`;
const ImgAvatar = styled.Image`
  width: 100px;
  height: 100px;
  border-radius: 5px;
`;
const TextInp = styled.TextInput`
  margin-top: 10px;
  padding-left: 12px;
  border-radius: 7px;
  background-color: #e1e3ed;
  border-width: 0.5px;
  border-color: #a1a1a1;
  font-size: 19px;
  padding-vertical: 17px;
  font-weight: 400;
`;
