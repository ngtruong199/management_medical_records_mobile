import React from 'react';
import {TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import styled from 'styled-components/native';
import {showBottomOption} from '../../../action/bottom-option';
import {
  deleteListType,
  getFilterCardPrescription,
  setCurrentType,
} from '../../../action/medical-profile';
import {navigate, popNavigate} from '../../../navigation/rootNavigation';
import Header from '../../../ui/components/Header';
import {
  RowView,
  ScrollContainer,
  TextMedium,
  TouchWhite,
  TouchWhiteNoShadow,
} from '../../../ui/themes/styles';
import getIconWithName from '../../../ui/utils/Icons';
import {SearchMedical} from '../Search';

interface Props {
  route: Route;
  listTypeName: string[];
}
export interface Route {
  key: string;
  name: string;
  params?: Params;
}

export interface Params {}

export const ListTypeName = (props: Props) => {
  const renderItem = ({item, index}) => (
    <ViewCard
      onLongPress={() =>
        showBottomOption([
          {
            onPress: () => deleteListType(item),
            option: 'delete',
          },
        ])
      }
      onPress={() => {
        setCurrentType(item);
      }}
      key={`${index}`}>
      <TextName>{item}</TextName>
    </ViewCard>
  );
  const type_disease = useSelector(state => state.auth_management.type_disease);
  return (
    <ScrollContainer
      contentContainerStyle={{
        flexGrow: 1,
        paddingBottom: 50,
        paddingHorizontal: 20,
      }}
      data={type_disease || []}
      ListHeaderComponent={
        <Header text={'Loại bệnh'}>
          <RowView style={{marginLeft: 'auto'}}>
            <Touch onPress={() => navigate('Medical_Request')}>
              <ImageAddMedical
                source={require('../../../../../assets/medical_add.jpeg')}
              />
            </Touch>
          </RowView>
        </Header>
      }
      renderItem={renderItem}
      keyExtractor={(item, index) => index.toString()}
    />
  );
};
const ViewCard = styled(TouchWhiteNoShadow)`
  background-color: #f9fafc;
  align-items: flex-start;
  margin: 5px;
`;
const TextName = styled(TextMedium)`
  padding: 10px;
  color: #111;
`;
const ImageAddMedical = styled.Image`
  width: 70px;
  height: 70px;
  border-radius: 8px;
`;
const Touch = styled.TouchableOpacity`
  margin-left: auto;
`;
