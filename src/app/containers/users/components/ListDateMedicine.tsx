import React, {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import styled from 'styled-components/native';
import {showBottomOption} from '../../../action/bottom-option';
import {
  DateMedicine,
  deleteDateMedicine,
  deleteListType,
  getListDateMedicine,
  setCurrentType,
} from '../../../action/medical-profile';
import {navigate} from '../../../navigation/rootNavigation';
import {StateMedical} from '../../../reducers/medical-profile';
import Header from '../../../ui/components/Header';
import {
  Description,
  RowView,
  ScrollContainer,
  TextLarge,
  TextMedium,
  TouchWhiteNoShadow,
} from '../../../ui/themes/styles';

interface Props {
  route: Route;
  listTypeName: string[];
}
export interface Route {
  key: string;
  name: string;
  params?: Params;
}

export interface Params {}

export const ListDateMedicine = (props: Props) => {
  useEffect(() => {
    getListDateMedicine();
  }, []);
  const renderItem = ({item, index}: {item: DateMedicine; index: number}) => (
    <ViewCard
      onLongPress={() =>
        showBottomOption([
          {
            onPress: () => deleteDateMedicine(item.id),
            option: 'delete',
          },
        ])
      }
      onPress={() => navigate('Medicine_Request', {medicine_id: item.id})}
      key={`${index}`}>
      <TextName>{item.time}</TextName>
      {item.name ? (
        <TextName style={{opacity: 0.8}}>{item.name}</TextName>
      ) : null}
    </ViewCard>
  );
  const list_date_medicine: DateMedicine[] = useSelector(
    ({medical_profile}: {medical_profile: StateMedical}) =>
      medical_profile.listDateMedicine,
  );
  return (
    <ScrollContainer
      contentContainerStyle={{
        flexGrow: 1,
        paddingBottom: 50,
        paddingHorizontal: 20,
      }}
      data={list_date_medicine || []}
      ListHeaderComponent={
        <>
          <Header text={'Các giai đoạn uống thuốc'} />
          <Description>
            Màn hình này hiển thị danh sách giai đoạn uống thuốc
          </Description>
        </>
      }
      renderItem={renderItem}
      keyExtractor={(item, index) => index.toString()}
    />
  );
};
const ViewCard = styled(TouchWhiteNoShadow)`
  background-color: #f9fafc;
  align-items: flex-start;
  margin: 5px;
`;
const TextName = styled(TextLarge)`
  padding: 10px;
  color: #555;
`;
const ImageAddMedical = styled.Image`
  width: 70px;
  height: 70px;
  border-radius: 8px;
`;
const Touch = styled.TouchableOpacity`
  margin-left: auto;
`;
