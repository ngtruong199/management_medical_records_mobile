import React, {useEffect, useState} from 'react';
import {connect, useSelector} from 'react-redux';
import {getAllUserManagement} from '../../action/auth-management';
import {
  getAllCardMedical,
  updateCurrentMedical,
} from '../../action/medical-profile';
import {navigate} from '../../navigation/rootNavigation';
import Header from '../../ui/components/Header';
import CreateMedicalUser from '../../ui/icons/CreateMedicalUser';
import SearchIcon from '../../ui/icons/Search';
import {_numberCss} from '../../ui/themes/dimens';
import {TouchPlus} from '../../ui/themes/styles';
import {
  RowView,
  ScrollContainer,
  Touch,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import getIconWithName from '../../ui/utils/Icons';
import CardMedical from './CardMedical';
import CardUser from './CardUser';

const Medical_List = ({}) => {
  const allUser = useSelector(state => state?.auth_management?.allUser);
  useEffect(() => {
    // console.log(allUser);
  }, [allUser]);

  useEffect(() => {
    getAllUserManagement();
  }, []);
  const renderItem = ({item}) => {
    return <CardUser {...item} />;
  };
  return (
    <>
      <ScrollContainer
        contentContainerStyle={{
          paddingHorizontal: _numberCss['pd-container'],
          flexGrow: 1,
          paddingBottom: 50,
        }}
        ListHeaderComponent={
          <Header showBack={false} text={'Quản Lý Thành Viên'}>
            <TouchWhiteNoShadow
              onPress={() => navigate('SearchMedical')}
              style={{marginVertical: 0, marginLeft: 'auto'}}>
              <SearchIcon width={50} height={50} />
            </TouchWhiteNoShadow>
          </Header>
        }
        data={allUser}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
      <TouchPlus
        style={{backgroundColor: '#00000000'}}
        onPress={() => navigate('Register_User')}>
        <CreateMedicalUser width={80} height={80} />
      </TouchPlus>
    </>
  );
};

const mapStateToProps = state => ({
  medicals_card: state.medical_profile.medicals,
});

const mapDispatchToProps = {
  getAllCardMedical,
  updateCurrentMedical,
};

export default connect(mapStateToProps, mapDispatchToProps)(Medical_List);
