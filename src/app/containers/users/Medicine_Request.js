import React, {useEffect, useState} from 'react';
import {FlatList, Image, Switch, Text} from 'react-native';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {time_now, toMoment} from '../../../utils/date';
import {
  getMedicine,
  updateMedicine,
  DrugData,
  Medicine,
} from '../../action/medical-profile';
import {getString} from '../../locate';
import DatePicked, {ListDatePicked} from '../../ui/components/Date_Picked';
import Header from '../../ui/components/Header';
import InputField from '../../ui/components/InputField';
import ScrollContainer from '../../ui/components/ScrollContainer';
import TouchIcons from '../../ui/components/TouchIcons';
import DefaultImage from '../../ui/icons/DefaultImage';
import PlusIcon from '../../ui/icons/PlusIcon';
import _user_color from '../../ui/themes/Colors';
import {getStyleWrap} from '../../ui/themes/dimens';
import {
  Description,
  RowArea,
  TextLarge,
  TitleInput,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import _ from 'lodash';
import {_general} from '../../action/data_general';
import SelectPhoto from '../../ui/components/SelectPhoto';
import {utils} from '@react-native-firebase/app';
import storage from '@react-native-firebase/storage';
import {create_UUID} from '../../../utils/number';
import ZoomImage from '../../ui/components/ShowImage';

const sizeItemNum = 45;
const propsIconsImage = {width: 90, height: 90};
const objPlus = {isPlus: true};
export const Medicine_Request = ({current_medical, route}) => {
  const [date_start, set_date_start] = useState(time_now);
  const [drug_name, set_drug_name] = useState('');
  const [note, setNote] = useState('');
  const [selected_index, setSelectedIndex] = useState(0);
  const [list_medicine, set_list_medicine] = useState([{}]);
  const [list_img, set_list_img] = useState([]);
  const [amount, set_amount] = useState({});
  const [date_end, set_date_end] = useState(time_now);
  const [day_medicine, set_day_medicine] = useState('1');
  const [indexTouchPhoto, setIndexPhoto] = useState(0);
  const [openPopupPhoto, setOpenPopup] = useState(false);
  const [medicine_time, setMedicine_time] = useState([]);
  const [old_medicine, setOld_medicine] = useState({});
  const [enableNotification, setEnableNotification] = useState(false);
  const [num_pills_day, setNum_pills_day] = useState('1');
  const [reminiscentName, setReminiscentName] = useState('');
  const [showImg, setShowImg] = useState('');

  useEffect(() => {
    const callback = (data: Medicine) => {
      if (!_.isEmpty(data)) {
        set_list_medicine(data.drug_data);
        setDataItem(data?.drug_data && data.drug_data[0]);
        set_date_start(data.date_start);
        set_date_end(data.date_end);
        set_day_medicine(data.day_medicine);
        setMedicine_time([...data.medicine_time]);
        setOld_medicine(data);
        setEnableNotification(data?.enableNotification || false);
        setReminiscentName(data?.reminiscentName);
      }
    };
    if (current_medical && route?.params?.medicine_id) {
      getMedicine({
        id: current_medical.id,
        callback,
        medicine_id: route.params.medicine_id,
      });
    }
  }, [current_medical, route]);
  useEffect(() => {
    list_medicine && set_list_medicine(list_medicine);
  }, [list_medicine]);

  const setSelected = ({item, index}, callback) => {
    //set Input Into List
    const _list = list_medicine.map((_item: DrugData, _index) => {
      _index === selected_index &&
        (_item = {drug_name, num_pills_day, list_img, note, amount});

      //Push Image File to Cloud Storage
      callback &&
        (_item.list_img = _item.list_img?.map(itemImg => {
          if (itemImg?.uri.includes('file://')) {
            //Random name file
            const fileName = `${create_UUID()}.jpg`;
            storage()
              .ref(fileName)
              .putFile(itemImg.uri)
              .catch(err => console.log(err));
            return {uri: fileName};
          }
          return itemImg;
        }));

      return _item;
    });
    callback ? callback(_list) : set_list_medicine(_list);
    setDataItem(item || {});

    setSelectedIndex(index);
  };
  const setDataItem = (item?: DrugData) => {
    if (item) {
      //Get data item in list
      set_drug_name(item.drug_name || '');
      set_amount(item.amount || {amount: '', unit: ''});
      setNote(item.note || '');
      set_list_img(item.list_img || []);
      setNum_pills_day(item.num_pills_day);
    }
  };
  const medicineSubmit = () => {
    let drug_data = null;
    setSelected({index: selected_index}, data => (drug_data = data));
    updateMedicine(
      current_medical,
      {
        date_start,
        date_end,
        day_medicine,
        drug_data,
        medicine_time,
        enableNotification,
        reminiscentName,
      },
      old_medicine,
      route?.params?.medicine_id,
    );
  };
  //Renter Touch Horizontal
  const renderItemChooseMedicine = ({item, index}) => {
    const isSelect = selected_index === index && !item.isPlus;
    return (
      <TouchNumber
        isSelect={isSelect}
        onPress={() => {
          item.isPlus
            ? set_list_medicine([...list_medicine, {}])
            : setSelected({item, index});
        }}>
        <TextNumber isSelect={isSelect}>
          {item.isPlus ? '+' : index + 1}
        </TextNumber>
      </TouchNumber>
    );
  };

  return (
    <ScrollContainer button={{text: 'Lưu', onPress: medicineSubmit}}>
      <Header text={getString('medical', 'medicine_title')} />
      <Description>
        {getString('medical', 'description_Input_medicine')}
      </Description>
      <InputField
        value={reminiscentName}
        onSubmitText={setReminiscentName}
        label={'Tên gợi nhớ'}
      />
      <RowArea style={{justifyContent: 'space-around'}}>
        <DatePicked
          mode={'date'}
          setDate={date => {
            toMoment(date).isAfter(toMoment(date_end)) && set_date_end(date);
            set_date_start(date);
          }}
          date={date_start}
          title={getString('medical', 'medicine_date_start')}
        />
        <DatePicked
          mode={'date'}
          setDate={date => {
            toMoment(date).isBefore(toMoment(date_start)) &&
              set_date_start(date);
            set_date_end(date);
          }}
          date={date_end}
          title={getString('medical', 'medicine_date_end')}
        />
      </RowArea>

      <RowArea>
        <InputField
          isRow
          isNumber
          value={day_medicine}
          onSubmitText={set_day_medicine}
          label={getString('medical', 'day_way')}
        />
      </RowArea>
      <ListDatePicked
        title={`Thời gian uống thuốc`}
        mode={'time'}
        listDate={medicine_time}
        onSetDate={(value: any) => {
          setMedicine_time(value);
        }}
      />
      <RowChat>
        <TextTitle>Bật thông báo</TextTitle>
        <Switch
          value={enableNotification}
          onValueChange={() => setEnableNotification(!enableNotification)}
          style={{marginLeft: 'auto'}}
        />
      </RowChat>
      <FlatList
        style={{flexGrow: 0}}
        horizontal
        data={[...list_medicine, objPlus]}
        renderItem={renderItemChooseMedicine}
        keyExtractor={(item, index) => index.toString()}
      />
      {list_medicine?.length ? (
        <>
          <InputField
            value={drug_name}
            onChangeText={set_drug_name}
            label={getString('medical', 'drug_name')}
          />
          <InputField
            isNumber
            value={num_pills_day}
            onChangeText={setNum_pills_day}
            label={'Số lần uống / ngày'}
          />
          <TitleInput>{getString('medical', 'dosage')}</TitleInput>
          <AmountMedicine amount_data={amount} onSubmit={set_amount} />
          <InputField
            minHeight={90}
            numberOfLines={3}
            value={note}
            onChangeText={setNote}
            label={getString('medical', 'note')}
          />
          <TitleInput>{getString('medical', 'url_drug')}</TitleInput>
          <RowArea>
            {[...list_img, null].map((item, index) => {
              return (
                <TouchIcons
                  onLongPress={() => {
                    if (item) {
                      setOpenPopup(true);
                      setIndexPhoto(index);
                    }
                  }}
                  onPress={() => {
                    if (item && typeof item === 'object') {
                      setShowImg(item);
                    } else if (typeof item === 'function') {
                      setOpenPopup(true);
                      setIndexPhoto(index);
                    }
                    !item && set_list_img([...list_img, DefaultImage]);
                  }}
                  style={getStyleWrap(3)}
                  key={index.toString()}
                  item={item}
                />
              );
            })}
          </RowArea>
        </>
      ) : null}
      <SelectPhoto
        visible={openPopupPhoto}
        onDismiss={() => setOpenPopup(false)}
        onSelectPhoto={res => {
          list_img[indexTouchPhoto] = res;
          setOpenPopup(false);
        }}
      />
      <ZoomImage url={showImg} onClose={() => setShowImg('')} />
    </ScrollContainer>
  );
};
const AmountMedicine = ({amount_data, isEnd, onSubmit}) => {
  const onSubmitText = (field, value) => {
    amount_data[field] = value;
    const data = Object.values(amount_data);

    onSubmit &&
      data.length === 2 &&
      !data.includes('') &&
      onSubmit(amount_data);
  };

  return (
    <RowArea>
      <InputField
        isNumber
        value={amount_data?.amount || ''}
        number={4}
        placeholder={'10'}
        onChangeText={text => onSubmitText('amount', text)}
      />
      <InputField
        width={150}
        value={amount_data?.unit || ''}
        number={4}
        isEdit={false}
        isRow
        dropdownData={_general.medicine_amount}
        onChangeText={text => onSubmitText('unit', text)}
      />
    </RowArea>
  );
};
const TextNumber = styled.Text`
  color: ${({isSelect}) => (isSelect ? 'white' : 'black')};
`;
const RowChat = styled.View`
  align-items: center;
  flex-direction: row;
  margin-vertical: 10px;
`;
const TextTitle = styled(TextLarge)`
  color: #111;
`;
const ImageView = styled.Image`
  width: 105px;
  height: 105px;
  border-radius: 15px;
`;
const TextAmount = styled(Description)`
  align-self: center;
  font-size: 30px;
  margin-horizontal: 2px;
`;
const TouchNumber = styled(TouchWhiteNoShadow)`
  height: ${sizeItemNum}px;
  width: ${sizeItemNum}px;
  margin-end: 10px;
  background-color: ${({isSelect}) =>
    isSelect ? _user_color.primaryColor : 'white'};
`;
const mapStateToProps = state => ({
  current_medical: state.medical_profile.current_medical,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Medicine_Request);
