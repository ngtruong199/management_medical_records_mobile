import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import {TouchWhite, ScrollWithSearch} from '../../ui/themes/styles';
import ItemChat from './itemChat';
import SearchInput from '../../ui/components/SearchInput';

import getIconWithName from '../../ui/utils/Icons';
import {navigate} from '../../navigation/rootNavigation';
import {useSelector} from 'react-redux';
import {getListChat} from '../../action/chat';
import {ChatCard} from '../../action/chat';

const Chat = () => {
  const renderItem = ({item}) => <ItemChat {...item} />;
  const allChat: ChatCard[] = useSelector(state => state.chat.allChat);
  const listChatSearch: ChatCard[] =
    useSelector(state => state.chat.listSearch) || [];

  const [listRenderSearch, setListRenderSearch] = useState();
  useEffect(() => {
    getListChat();
  }, []);

  useEffect(() => {
    listChatSearch &&
      allChat &&
      setListRenderSearch(
        listChatSearch.map(item => ({
          ...item,
          linked: !!allChat.find(inside_chat => inside_chat.id === item.id),
        })),
      );
  }, [listChatSearch, allChat]);

  const renderListItemSearch = (item, index) => {
    return <ItemChat isJoin key={`Search_${index}`} {...item} />;
  };
  const renderTouchClose = () => {
    return (
      <TouchWhite
        onPress={() => navigate('CreateChat')}
        style={{
          marginVertical: 5,
          alignSelf: 'center',
          padding: 0,
          marginLeft: 'auto',
        }}>
        {getIconWithName({
          name: 'facebook-messenger',
          size: 30,
          color: '#777',
        })}
      </TouchWhite>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <SearchInput rightItem={renderTouchClose}>
        {listRenderSearch?.map(renderListItemSearch)}
      </SearchInput>
      <ScrollWithSearch
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: 50,
          paddingTop: 0,
        }}
        data={allChat}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};
export default Chat;
