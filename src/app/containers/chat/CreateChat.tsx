import React, {useState} from 'react';
import {Text} from 'react-native-svg';
import styled from 'styled-components/native';
import {windowWidth} from '../../../utils/platform';
import Header from '../../ui/components/Header';
import InputField from '../../ui/components/InputField';
import ScrollContainer from '../../ui/components/ScrollContainer';
import SelectPhoto from '../../ui/components/SelectPhoto';
import _ from 'lodash';
import withKeyboardAwareView from '../../ui/hoc/withKeyboardAwareView';
import {registerUserMedicalProfile} from '../../action/auth-management';
import DatePicked from '../../ui/components/Date_Picked';
import {time_now} from '../../../utils/date';
import {_general} from '../../action/data_general';
import {_color} from '../../ui/themes/Colors';
import {getString} from '../../locate';
import {Switch} from 'react-native';
import {ShadowString, TextLarge, TextMedium} from '../../ui/themes/styles';
import {createChatGroup} from '../../action/chat';
import {DefaultImgChatGroup} from '../../ui/imagePath';
import getIconWithName from '../../ui/utils/Icons';

const CreateChat = () => {
  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  const [note, setNote] = useState('');
  const [isPublic, setIsPublic] = useState(true);
  const [openChooseAvatar, setChooseAvatar] = useState(false);
  const onRegister = () => {
    createChatGroup({
      name,
      avatar,
      note,
      status: isPublic ? 'public' : 'private',
    });
  };

  return (
    <ScrollContainer
      style={{flex: 1, backgroundColor: '#fff'}}
      button={{text: 'Register', onPress: onRegister, disable: !name}}>
      <Header
        text={'Create Chat'}
        styleBack={{backgroundColor: _color.background_Container}}
      />
      <TouchImage onPress={() => setChooseAvatar(true)}>
        <ImageView source={!_.isEmpty(avatar) ? avatar : DefaultImgChatGroup} />
      </TouchImage>
      <EditText
        placeholder={getString('chat', 'name')}
        onChangeText={setName}
        style={{fontWeight: '600'}}
      />
      <EditText
        onChangeText={setNote}
        placeholder={getString('chat', 'note')}
        numberOfLines={3}
        multiline={true}
        style={{minHeight: 100}}
      />
      <RowChat>
        <TextTitle>{getString('chat', 'status')}</TextTitle>
        <Switch
          value={isPublic}
          onValueChange={() => setIsPublic(!isPublic)}
          style={{marginLeft: 'auto'}}
        />
      </RowChat>

      <SelectPhoto
        visible={openChooseAvatar}
        onDismiss={() => setChooseAvatar(false)}
        onSelectPhoto={item => setAvatar(item)}
      />
    </ScrollContainer>
  );
};
const RowChat = styled.View`
  align-items: center;
  flex-direction: row;
`;
const TextTitle = styled(TextLarge)`
  color: #111;
`;
const TouchImage = styled.TouchableOpacity`
  align-self: center;
  ${ShadowString}
  margin-bottom: 20px;
`;
const EditText = styled.TextInput`
  padding: 12px;
  font-size: 16px;
  border-radius: 10px;
  background-color: #e9e9e9;
  margin-vertical: 10px;
  color: #111;
`;
const ImageView = styled.Image`
  width: ${windowWidth / 3}px;
  height: ${windowWidth / 3}px;
  border-radius: ${windowWidth / 3 / 2}px;
`;
export default withKeyboardAwareView(CreateChat);
