import React, {useState, useEffect, useCallback} from 'react';
import {
  Composer,
  GiftedChat,
  InputToolbar,
  Send,
} from 'react-native-gifted-chat';
import styled from 'styled-components/native';
import Header from '../../ui/components/Header';
import SendIcon from '../../ui/icons/send';
import {isIOS} from '../../../utils/platform';
import {formatDate} from '../../../utils/date';
import ItemMessage from './ItemMessage';
import {getChatRef, sendNewMess} from '../../action/chat';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {user_id} from '../../action/auth';
import {_color} from '../../ui/themes/Colors';
import {useSelector} from 'react-redux';
interface Props {
  route: {
    key: string;
    name: string;
    params: {
      idChat: string;
    };
  };
}
const ChatUser = ({route}: Props) => {
  const [messages, setMessages] = useState([]);
  const [chatRef, setChatRef] =
    useState<
      FirebaseFirestoreTypes.CollectionReference<FirebaseFirestoreTypes.DocumentData>
    >();
  let user = useSelector(state => state.auth) || {};
  user.avatar = user?.avatarRender;
  useEffect(() => {
    const _chatRef = getChatRef(route.params.idChat);
    setChatRef(_chatRef);
    const unsubcribe = _chatRef.onSnapshot(querySnapshot => {
      if (!querySnapshot) {
        return;
      }
      const messageFirestore = querySnapshot
        .docChanges()
        .filter(({type}) => type === 'added')
        .map(({doc}) => {
          const message = doc.data();
          try {
            return {...message, createdAt: message.createdAt.toDate()};
          } catch (error) {
            return message;
          }
        })
        .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());

      setMessages(prevMess => GiftedChat.append(prevMess, messageFirestore));
    });
    return () => unsubcribe();
  }, [route]);
  const handleSend = async (message: any[]) => {
    const writes = message.filter(
      item => item.text.trim() && chatRef.add(item),
    );
    sendNewMess(
      route.params.idChat,
      `User : ${message[message.length - 1].text}`,
    );

    await Promise.all(writes);
  };
  const renderInputToolbar = props => {
    return (
      <InputToolbar
        {...props}
        containerStyle={{
          borderRadius: 20,
        }}
      />
    );
  };
  const renderBottomBar = (props: any) => {
    return (
      <BottomView>
        <ViewBottomRound>
          <Composer
            {...props}
            placeholder={'Input Message'}
            multiline={false}
            textInputStyle={{color: '#111'}}
          />
        </ViewBottomRound>
        <Send
          {...props}
          containerStyle={{alignItems: 'center', marginRight: 5}}>
          <SendIcon />
        </Send>
      </BottomView>
    );
  };
  const isShowName = (currentMessage, previousMessage) => {
    if (!Object.keys(previousMessage).length) {
      return true;
    }
    try {
      if (currentMessage.user.name === previousMessage.user.name) {
        return false;
      }
    } catch (error) {}
    return true;
  };
  const renderItemMess = props => {
    const {currentMessage = {}, previousMessage = {}} = props;
    const {user = {}, createdAt} = currentMessage;
    // console.log(currentMessage);
    return (
      <ItemMessage
        key={currentMessage._id}
        isShowTitle={isShowName(currentMessage, previousMessage)}
        user={user}
        message={currentMessage.text || ''}
        timeText={formatDate(createdAt)}
      />
    );
  };
  const renderSend = () => null;
  return (
    <Container>
      <Header styleBack={{marginLeft: 20}} text={'Chat'} />
      <GiftedChat
        keyboardShouldPersistTaps={'handled'}
        renderMessage={renderItemMess}
        messages={messages}
        user={user}
        alwaysShowSend
        onSend={handleSend}
        scrollToBottom
        renderSend={renderSend}
        // renderLoading={renderLoading}
        renderInputToolbar={renderInputToolbar}
        renderComposer={renderBottomBar}
        renderAvatar={null}
        minComposerHeight={40}
        minInputToolbarHeight={isIOS ? 62 : 55}
      />
    </Container>
  );
};
const Container = styled.View`
  flex: 1;
`;

export default ChatUser;

const BottomView = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-left: 30px;
  margin-right: 55px;
`;
const ViewBottomRound = styled.View`
  padding-left: 5px;
  flex-direction: row;
  border-radius: 20px;
  background-color: #e9e9e9;
  align-items: center;
  margin-vertical: 5px;
`;
