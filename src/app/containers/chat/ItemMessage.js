import React from 'react';
import {Image} from 'react-native';
import styled from 'styled-components';
import BubbleMessage from '../../ui/components/BubbleMessage';
import AvatarIcon from '../../ui/icons/avatar';
import {user_id} from '../../action/auth';

export default class ItemMessage extends React.PureComponent {
  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const {
      user = {},
      message = '',
      timeText = '',
      isShowTitle = true,
    } = this.props;
    const isMe = user._id === user_id;
    const avatar = user?.avatar?.uri;
    return (
      <Container>
        <Message isYour={isMe}>
          <TextName
            isShowTitle={isShowTitle}
            marginLeft={isMe ? 0 : 50}
            marginRight={isMe ? 50 : 0}>
            {user.name || 'unknow'}
          </TextName>
          <ViewItemMessage isYour={isMe}>
            <ViewAvatar>
              {isShowTitle &&
                (!avatar ? (
                  <ViewDefaultAvatar>
                    <AvatarIcon />
                  </ViewDefaultAvatar>
                ) : (
                  <ImageAvatar source={{uri: avatar}} />
                ))}
            </ViewAvatar>

            {isMe ? (
              <BubbleMessage
                message={message}
                timeText={timeText}
                isRight={true}
              />
            ) : (
              <BubbleMessage message={message} timeText={timeText} />
            )}
          </ViewItemMessage>
        </Message>
      </Container>
    );
  }
}

const Container = styled.View`
  margin-bottom: 10px;
`;
const Message = styled.View`
  align-items: ${({isYour}) => (isYour ? 'flex-end' : 'flex-start')};
`;
const TextName = styled.Text`
  display: ${({isShowTitle}) => (isShowTitle ? 'flex' : 'none')};
  color: #9c9494;
  font-size: 14px;
  margin-left: ${({marginLeft}) => marginLeft}px;
  margin-right: ${({marginRight}) => marginRight}px;
  margin-vertical: 5px;
`;
const ViewItemMessage = styled.View`
  flex-direction: ${({isYour}) => (isYour ? 'row-reverse;' : 'row')};
  justify-content: flex-start;
`;
const ViewAvatar = styled.View`
  margin-top: 10px;
  width: 40px;
  height: 40px;
  border-radius: 40px;
`;
const ViewDefaultAvatar = styled.View`
  background-color: #4294d8;
  padding: 10px;
  border-radius: 30px;
`;
const ImageAvatar = styled(Image)`
  width: 40px;
  height: 40px;
  border-radius: 40px;
`;
