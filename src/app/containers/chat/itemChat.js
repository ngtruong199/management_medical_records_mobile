import React from 'react';
import {View, Text} from 'react-native';
import styled from 'styled-components/native';
import {windowWidth} from '../../../utils/platform';
import {
  RowTouch,
  RowView,
  TextLarge,
  TextMedium,
  TextSmall,
  TouchWhiteNoShadow,
} from '../../ui/themes/styles';
import _ from 'lodash';
import {DefaultImgChatGroup} from '../../ui/imagePath';
import {getString} from '../../locate';
import {navigate} from '../../navigation/rootNavigation';
import LinkIcon from '../../ui/icons/Link';
import CheckedIcon from '../../ui/icons/Checked';
import {linkedChat, unlinkChat} from '../../action/chat';
import {showAlert} from '../../action/alert';

const imageSize = windowWidth / 8;
const ItemChat = props => {
  const {
    avatarRender,
    name,
    date,
    content,
    id,
    unread = false,
    isJoin = false,
    linked = false,
  } = props;
  const Container = isJoin ? ViewItemContainer : ItemContainer;
  const onHandleLinked = () => {
    const {isJoin, linked, ...res} = props;
    const title = 'Tin nhắn';
    const message = linked
      ? 'Bạn có muốn rời khỏi nhóm chat này'
      : 'Bạn có muốn tham gia vào nhóm chat này ?';
    showAlert(
      title,
      message,
      () => (!linked ? linkedChat(res) : unlinkChat(id)),
      () => {},
    );
  };
  return (
    <Container
      onPress={() => navigate('ChatUser', {idChat: id})}
      activeOpacity={0.7}>
      <ImageItem
        source={!_.isEmpty(avatarRender) ? avatarRender : DefaultImgChatGroup}
      />
      <ColumnView>
        <NameChat unread={unread || false}>{name}</NameChat>
        <TimeChat>{date}</TimeChat>
        <ContentChat unread={unread} numberOfLines={1}>
          {content || getString('chat', 'no_message')}
        </ContentChat>
      </ColumnView>
      {isJoin && (
        <TouchWhiteNoShadow
          onPress={onHandleLinked}
          style={{alignSelf: 'center', marginVertical: 0}}>
          {linked ? (
            <CheckedIcon width={50} height={30} />
          ) : (
            <LinkIcon width={50} height={30} />
          )}
        </TouchWhiteNoShadow>
      )}
    </Container>
  );
};
const ItemContainer = styled(RowTouch)`
  padding-horizontal: 10px;
  border-top-width: 0.5px;
  border-bottom-width: 0.5px;
  border-color: #cacaca;
  z-index: -1;
`;
const ViewItemContainer = styled(RowView)`
  padding-horizontal: 10px;
  border-top-width: 0.5px;
  border-bottom-width: 0.5px;
  border-color: #cacaca;
  z-index: -1;
`;
const ImageItem = styled.Image`
  width: ${imageSize}px;
  height: ${imageSize}px;
  border-radius: ${imageSize / 2}px;
`;
const ColumnView = styled.View`
  flex: 1;
  margin-left: 10px;
  justify-content: flex-start;
  align-items: flex-start;
`;
const NameChat = styled(TextLarge)`
  color: black;
  font-weight: ${({unread}) => (unread ? 'bold' : '400')};
`;
const ContentChat = styled(TextMedium)`
  color: ${({unread}) => (unread ? '#111' : '#777')};
  padding-right: 30px;
  font-weight: 500;
`;
const TimeChat = styled(TextSmall)`
  position: absolute;
  top: 5px;
  right: 10px;
  color: #19b5d4;
`;
export default ItemChat;
