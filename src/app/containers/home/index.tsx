import React from 'react';
import styled from 'styled-components/native';
import {
  CenterContainer,
  ShadowString,
  shadow_with_color,
  TextMedium,
} from '../../ui/themes/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import {RootStackParamList} from '../../../../App';
import {_numberCss} from '../../ui/themes/dimens';
import {header, HeaderItem, Post} from '../../store/object/home';
import {FlatList} from 'react-native-gesture-handler';
import getIconWithName from '../../ui/utils/Icons';
import _user_color, {_color} from '../../ui/themes/Colors';
import InputAddress from '../../ui/components/InputAddress';
import {Text} from 'react-native-svg';

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Home'>;
interface Props {
  navigation: HomeScreenNavigationProp;
}

interface State {
  selectedID?: string;
  listPost: Post[];
}
export default class Home extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      listPost: [],
    };
  }
  onTextChangeField(value: string, field?: string) {
    field && this.setState({[field]: value} as any);
  }
  pressHeaderItem = (item: HeaderItem) => {
    this.setState({listPost: item.item || []});
  };
  renderItemHeaded = ({item}: {item: HeaderItem}) => {
    return (
      <TouchItemHeader
        style={{backgroundColor: item.color}}
        color={item.color}
        onPress={() => {
          this.props.navigation.navigate('Chat');
          this.pressHeaderItem(item);
        }}>
        <ViewItemHeader>
          {getIconWithName({name: item.icon, color: item.color, size: 30})}
        </ViewItemHeader>
        <TextMedium>{item.name}</TextMedium>
      </TouchItemHeader>
    );
  };
  renderItemPost = ({item}: {item: Post}) => {
    return (
      <ViewPost>
        <ImageRound source={{uri: item.urlImage}} />
      </ViewPost>
    );
  };
  render() {
    const {listPost} = this.state;
    return (
      <Container>
        <HeaderListArea
          style={{paddingVertical: 15}}
          horizontal
          data={header}
          renderItem={this.renderItemHeaded}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        />

        <InputAddress />
        {/* <FlatList
          extraData={listPost}
          data={listPost}
          renderItem={this.renderItemPost}
          keyExtractor={(item, index) => index.toString()}
        /> */}
        {/* <ViewPost>
          <ImageRound
            source={{
              uri: 'https://reactnative.dev/img/tiny_logo.png',
            }}
          />
        </ViewPost> */}
      </Container>
    );
  }
}
const SizePadding = 10;
const SizeViewIcons = '50px';
const Container = styled(CenterContainer)`
  justify-content: flex-start;
  margin: 0;
  padding: 0;
  background-color: ${_color.background_Container};
`;
const ViewPost = styled.View`
  margin: 0;
  padding: 0;
  align-items: center;
`;
const ImageRound = styled.Image`
  border-radius: ${_numberCss['bd-radius']};
  width: 100%;
  height: 200px;
`;
const HeaderListArea = styled.FlatList`
  flex-grow: 0;
`;
const TouchItemHeader = styled.TouchableOpacity`
  padding: ${SizePadding}px;
  padding-bottom: ${SizePadding * 2}px;
  background-color: ${_user_color.primaryColor};
  margin-horizontal: 10px;
  align-items: center;
  justify-content: center;
  height: 100px;
  margin-top: 100px;
  border-radius: 20px;
  ${ShadowString}
`;
const ViewItemHeader = styled.View`
  width: ${SizeViewIcons};
  height: ${SizeViewIcons};
  align-items: center;
  justify-content: center;
  background-color: white;
  margin-bottom: 5px;
  border-radius: 20px;
`;
