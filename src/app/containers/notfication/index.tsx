import React, {Props, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {
  TextLarge,
  TextMedium,
  ShadowString,
  TextSmall,
  CenterAreaContainer,
  Touch,
} from '../../ui/themes/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import _user_color, {_color} from '../../ui/themes/Colors';
import IconsButtonText from '../../ui/components/IconsButtonText';
import getIconWithName from '../../ui/utils/Icons';
import GoogleIcon from '../../ui/icons/GoogleIcon';
import RowIconButton from '../../ui/components/RowIconButton';
import {
  safeBottomPadding,
  safePaddingTop,
  windowWidth,
} from '../../../utils/platform';
import {_numberStyled} from '../../ui/themes/dimens';
import withKeyboardAwareView, {
  passProps,
} from '../../ui/hoc/withKeyboardAwareView';
import {connect, useSelector} from 'react-redux';
import {getAuthCollection, logoutAuth, Profile} from '../../action/auth';
import {navigate} from '../../navigation/rootNavigation';
import _ from 'lodash';
import {AvatarProfileDefault} from '../../ui/imagePath';
import {displayStringDate, toDate, toTimeStamp} from '../../../utils/date';
import Header from '../../ui/components/Header';
import ScrollContainer from '../../ui/components/ScrollContainer';
import {fireStore} from '../../config/firebaseConfig';
import moment from 'moment';
import {updateCurrentUser} from '../../action/auth-management';
import {clickNotification, delAllNotification} from '../../action/notification';

interface Props {
  item: string;
}

interface State {}

const NotificationComponent: React.FC<Props> = (props: Props) => {
  const [listDisplay, setListDisplay] = useState([]);
  useEffect(() => {
    setListNoti();
    const interval = setInterval(setListNoti, 10000);
    return () => clearInterval(interval);
  }, []);
  const setListNoti = () => {
    getAuthCollection()
      .collection('Notification')
      .get({source: 'server'})
      .then(listDoc => {
        let listRenderNoti = [];
        listDoc.empty && setListDisplay([]);
        listDoc.forEach(doc => {
          const noti = doc.data()?.notification || {};
          const info = noti.info;
          const user_id = doc.data()?.user_id;
          const listNoti = noti.listTime?.map(item => ({
            time: item,
            ...info,
            id: doc.id,
            user_id,
          }));

          Array.prototype.push.apply(listRenderNoti, listNoti);
        });
        listRenderNoti.sort((a, b) => toDate(a.time) - toDate(b.time));
        listRenderNoti.length &&
          setListDisplay(
            listRenderNoti.filter(item => {
              return toTimeStamp(moment().toDate()).seconds > item.time.seconds;
            }),
          );
      });
  };
  const user: Profile = useSelector(state => state.auth) || {};
  return (
    <ScrollContainer
      styleContainer={{
        paddingBottom: 70,
      }}
      style={{
        paddingLeft: 0,
        paddingRight: windowWidth / 15,
        paddingTop: safePaddingTop,
      }}>
      <Header showBack={false} text={'Notification'}>
        <Touch
          onPress={() => delAllNotification(setListNoti)}
          style={{marginLeft: 'auto'}}>
          {getIconWithName({name: 'delete', size: 30})}
        </Touch>
      </Header>
      {listDisplay
        .slice(0)
        .reverse()
        .map((item, index) => (
          <RowIconButton
            isNotification
            onPress={() => {
              clickNotification(item);
            }}
            key={`noti_${index}`}
            icon={{name: 'heartbeat', size: 30, isRound: true}}
            text={item.title}
            description={item.message}
            rightText={displayStringDate(item.time, 'datetime')}
          />
        ))}
    </ScrollContainer>
  );
};
export default NotificationComponent;
const ContainerImage = styled.View`
  ${ShadowString}
`;
const ImageAvatar = styled.Image`
  width: 70px;
  height: 70px;
  border-radius: 10px;
  margin-right: 20px;
`;
const AbsContainer = styled.TouchableOpacity`
  position: absolute;
  right: 20px;
  top: 20px;
`;
const ContainerAvatar = styled.View`
  padding-top: 30px;
  flex-direction: row;
  margin-bottom: 5px;
`;
const LineView = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  padding-bottom: 20px;
`;
const ControlContainer = styled.View`
  border-radius: 20px;
  padding-top: ${safePaddingTop}px;
  justify-content: center;
`;
const ContainerName = styled.View``;

const Container = styled.ScrollView``;
