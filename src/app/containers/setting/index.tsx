import React, {Props} from 'react';
import styled from 'styled-components/native';
import {
  TextLarge,
  TextMedium,
  ShadowString,
  TextSmall,
  CenterAreaContainer,
} from '../../ui/themes/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import _user_color, {_color} from '../../ui/themes/Colors';
import IconsButtonText from '../../ui/components/IconsButtonText';
import getIconWithName from '../../ui/utils/Icons';
import GoogleIcon from '../../ui/icons/GoogleIcon';
import RowIconButton from '../../ui/components/RowIconButton';
import {safePaddingTop, windowWidth} from '../../../utils/platform';
import {_numberStyled} from '../../ui/themes/dimens';
import withKeyboardAwareView, {
  passProps,
} from '../../ui/hoc/withKeyboardAwareView';
import {connect, useSelector} from 'react-redux';
import {logoutAuth, Profile} from '../../action/auth';
import {navigate} from '../../navigation/rootNavigation';
import _ from 'lodash';
import {AvatarProfileDefault} from '../../ui/imagePath';
import {displayStringDate} from '../../../utils/date';
import {BottomOption} from '../../ui/components/BottomOption';
import {iconsBottomOption} from '../../ui/icons/typeBottom';
import {showBottomOption} from '../../action/bottom-option';

interface Props {
  item: string;
}

interface State {}

const SettingComponent: React.FC<Props> = (props: Props) => {
  const user: Profile = useSelector(state => state.auth) || {};
  return (
    <ControlContainer style={{paddingHorizontal: windowWidth / 15}}>
      <ContainerAvatar>
        <ContainerImage>
          <ImageAvatar
            source={
              !_.isEmpty(user.avatarRender?.uri)
                ? user.avatarRender
                : AvatarProfileDefault
            }
          />
        </ContainerImage>
        <ContainerName>
          <TextLarge
            style={{
              textAlign: 'left',
              color: 'black',
              marginTop: 5,
              marginBottom: 5,
              fontSize: 20,
            }}>
            {user.name || 'Tên Của bạn'}
          </TextLarge>
          <TextSmall style={{textAlign: 'left', color: '#777'}}>
            {displayStringDate(user?.dateOfBirth, 'date')}
          </TextSmall>
        </ContainerName>
      </ContainerAvatar>
      <LineView />
      <RowIconButton
        icon={{name: 'th-large', size: 30, isRound: true}}
        text={'Thông Tin Cá Nhân'}
        onPress={() => navigate('PersonalData')}
      />

      <LineView />
      <RowIconButton
        icon={{name: 'align-center', size: 30, isRound: true}}
        text={'FAQs'}
      />
      <RowIconButton
        icon={{name: 'logout', size: 30, isRound: true}}
        text={'Đăng Xuất'}
        onPress={logoutAuth}
      />
    </ControlContainer>
  );
};
export default SettingComponent;
const ContainerImage = styled.View`
  ${ShadowString}
`;
const ImageAvatar = styled.Image`
  width: 70px;
  height: 70px;
  border-radius: 10px;
  margin-right: 20px;
`;
const AbsContainer = styled.TouchableOpacity`
  position: absolute;
  right: 20px;
  top: 20px;
`;
const ContainerAvatar = styled.View`
  padding-top: 30px;
  flex-direction: row;
  margin-bottom: 5px;
`;
const LineView = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  padding-bottom: 20px;
`;
const ControlContainer = styled.View`
  flex: 1;
  border-radius: 20px;
  padding-top: ${safePaddingTop}px;
  background-color: ${_color.white};
`;
const ContainerName = styled.View``;

const Container = styled.ScrollView``;
