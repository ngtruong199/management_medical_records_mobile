import React, {useEffect, useState} from 'react';
import {TextInputProps} from 'react-native';
import {add} from 'react-native-reanimated';
import styled from 'styled-components/native';
import {isBeforeDay_Now, time_now, toDate} from '../../../utils/date';
import {safeBottomPadding, windowWidth} from '../../../utils/platform';
import {_general} from '../../action/data_general';
import {getString} from '../../locate';
import DatePicked from '../../ui/components/Date_Picked';
import Header from '../../ui/components/Header';
import InputField from '../../ui/components/InputField';
import ScrollContainer from '../../ui/components/ScrollContainer';
import SelectPhoto from '../../ui/components/SelectPhoto';
import {
  CheckError,
  CheckErrorInput,
  ErrorText,
  RowArea,
  ShadowString,
  Title,
  TitleInput,
  TouchWhite,
} from '../../ui/themes/styles';
import _ from 'lodash';
import {AvatarProfileDefault} from '../../ui/imagePath';
import {Profile, updateProfile} from '../../action/auth';
import {useSelector} from 'react-redux';
import ZoomImage from '../../ui/components/ShowImage';

interface Props {}

export const PersonalData = (props: Props) => {
  const user = useSelector(state => state.auth);

  useEffect(() => {
    const _us = (user || {}) as Profile;
    console.log(_us);
    if (_.isEmpty(_us)) return;
    setName(_us.name || '');
    setAvatar({uri: _us.avatarRender?.uri || ''});
    setDateOfBirth(_us.dateOfBirth || time_now);
    setGender(_us.gender || '');
    setNote(_us.note || '');
    setAddress(_us.address || '');
    setHeight(_us.height || '');
    setWeight(_us.weight || '');
    setBloodPressure(_us.bloodPressure || '');
    setGlycemic_index(_us.glycemic_index || '');
  }, [user]);

  const [name, setName] = useState('');
  const [avatar, setAvatar] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState(time_now);
  const [gender, setGender] = useState('Nam');
  const [note, setNote] = useState('');
  const [address, setAddress] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [bloodPressure, setBloodPressure] = useState('');
  const [glycemic_index, setGlycemic_index] = useState('');
  const [openChooseAvatar, setChooseAvatar] = useState(false);
  const onPressUpdate = () => {
    if (
      !isBeforeDay_Now(toDate(dateOfBirth)) ||
      !parseInt(height) ||
      !parseInt(weight) ||
      !parseInt(bloodPressure) ||
      !parseInt(glycemic_index) ||
      !address
    )
      return;
    const profile = {
      avatar,
      name,
      dateOfBirth,
      gender,
      address,
      weight,
      height,
      bloodPressure,
      glycemic_index,
    };
    updateProfile(profile);
  };
  return (
    <ScrollContainer
      button={{text: 'Cập nhật thông tin cá nhân', onPress: onPressUpdate}}
      styleContainer={{paddingHorizontal: 10, paddingBottom: safeBottomPadding}}
      renderTop={() => <Header text="Hồ sơ người dùng" />}>
      <TouchAvatar onPress={() => setChooseAvatar(true)}>
        <ImgAvatar
          source={!_.isEmpty(avatar?.uri) ? avatar : AvatarProfileDefault}
        />
      </TouchAvatar>
      <Input
        checkError="NotEmpty"
        title={'Tên của bạn'}
        value={name}
        onChangeText={setName}
        placeholder={'Name '}
      />
      <InputField
        width={windowWidth * 0.86}
        label={'Giới tính'}
        isEdit={false}
        dropdownData={_general.gender}
        value={gender}
        onSubmitDropdown={setGender}
      />
      <DatePicked
        isLastTimeNow
        mode={'date'}
        setDate={setDateOfBirth}
        date={dateOfBirth}
        title={'Ngày Sinh'}
      />
      <Input
        checkError="NotEmpty"
        title={'Địa chỉ'}
        value={address}
        onChangeText={setAddress}
        placeholder={'TP.Hồ Chí Minh'}
      />
      <RowArea style={{justifyContent: 'space-between'}}>
        <InputField
          checkError="PositiveNumber"
          width={windowWidth / 3}
          label={'Chiều cao(cm)'}
          value={`${height}`}
          onChangeText={setHeight}
          isNumber
        />
        <InputField
          checkError="PositiveNumber"
          width={windowWidth / 3}
          label={'Cân nặng(kg)'}
          value={`${weight}`}
          onChangeText={setWeight}
          isNumber
        />
        {/* <DatePicked
          mode={'date'}
          setDate={set_date_end}
          date={date_end}
          title={getString('medical', 'medicine_date_end')}
        /> */}
      </RowArea>
      <Input
        checkError="PositiveNumber"
        title={'Huyết áp(mm/Hg)'}
        value={bloodPressure}
        onChangeText={setBloodPressure}
        placeholder={'100'}
        keyboardType={'numeric'}
      />
      <Input
        keyboardType="numeric"
        checkError="PositiveNumber"
        title={'Chỉ số đường huyết(mg/dL) '}
        value={`${glycemic_index}`}
        onChangeText={setGlycemic_index}
        placeholder={'120'}
      />
      <Input
        title={'Ghi chú'}
        value={note}
        onChangeText={setNote}
        placeholder={'Note '}
        numberOfLines={3}
        style={{height: 100, paddingTop: 10}}
        multiline
      />
      <SelectPhoto
        visible={openChooseAvatar}
        onDismiss={() => setChooseAvatar(false)}
        onSelectPhoto={item => setAvatar(item)}
      />
    </ScrollContainer>
  );
};
const Input = (
  props: TextInputProps & {
    title?: string;
    checkError?: CheckErrorInput;
  },
) => {
  return (
    <ViewEdt>
      <TitleInput>{props.title}</TitleInput>
      <TextInp {...props} placeholderTextColor="#ccc" />
      <CheckError {...props} />
    </ViewEdt>
  );
};
const TouchAvatar = styled.TouchableOpacity`
  align-self: center;
  ${ShadowString}
  margin-bottom: 20px;
`;
const ViewEdt = styled.View`
  margin-bottom: 20px;
`;
const ImgAvatar = styled.Image`
  width: 100px;
  height: 100px;
  border-radius: 5px;
`;
const TextInp = styled.TextInput`
  margin-top: 10px;
  padding-left: 12px;
  border-radius: 7px;
  background-color: #e1e3ed;
  border-width: 0.5px;
  border-color: #a1a1a1;
  font-size: 19px;
  padding-vertical: 17px;
  font-weight: 400;
  color: #111;
`;
