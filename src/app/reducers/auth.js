import types, {DataDispatch} from '../action/types';
import {navigate} from '../navigation/rootNavigation';

const initState = {};
export default (state = initState, {type, payload}: DataDispatch) => {
  switch (type) {
    case types.LOGOUT_SUCCESS: {
      setTimeout(() => {
        navigate('Intro');
      }, 500);
      return {};
    }
    case types.LOGIN_SUCCESS: {
      setTimeout(() => {
        navigate('Users');
      }, 500);
      return {...payload};
    }
    case types.UPDATE_USER_INFO: {
      return {...state, ...payload};
    }
    default:
      return state;
  }
};
