import types, {DataDispatch} from '../action/types';
const initState = {
  showAlert: false,
};
export default (state = initState, {type, payload}: DataDispatch) => {
  switch (type) {
    case types.SHOW_ALERT:
      return {...payload, showAlert: true};
    case types.HIDE_ALERT:
      return {showAlert: false};
    case types.SHOW_LOADING:
      return {showAlert: true};
    case types.HIDE_LOADING: {
      const {title, message, onPressPositiveButton} = state;
      return message || title || onPressPositiveButton
        ? state
        : {showAlert: false};
    }
    default:
      return state;
  }
};
