import types, {DataDispatch} from '../action/types';
import {UserItem} from '../action/auth-management';
interface State {
  allUser?: UserItem[];
  currentUser?: UserItem;
  type_disease?: string[];
  user_id: string;
}
const initState: State = {
  allUser: [],
};
export default (state = initState, {type, payload}: DataDispatch<State>) => {
  switch (type) {
    case types.CREATE_USER_MEDICAL_PROFILE: {
      const {currentUser} = payload;
      return {allUser: [currentUser, ...state.allUser]};
    }
    case types.GET_ALL_USER_MEDICAL_PROFILE: {
      const {allUser} = payload;
      return {...state, allUser};
    }
    case types.UPDATE_USER_MEDICAL: {
      const {currentUser} = payload;
      const {allUser: _allUser} = state;
      const allUser = _allUser?.map(item =>
        item?.id === currentUser.id ? currentUser : item,
      );
      return {...state, allUser};
    }
    case types.SET_LIST_TYPE: {
      const {type_disease} = payload;
      return {...state, type_disease};
    }
    case types.DELETE_USER_MEDICAL: {
      const {user_id} = payload;
      const {allUser: _allUser} = state;
      const allUser = _allUser?.filter(item => item?.id !== user_id);
      return {...state, allUser};
    }
    default:
      return state;
  }
};
