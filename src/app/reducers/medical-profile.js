import {showAlert} from '../action/alert';
import types, {DataDispatch} from '../action/types';
import {getString} from '../locate';
import {navigate, popNavigate} from '../navigation/rootNavigation';
import {Profile} from '../action/auth';
export interface StateMedical {
  medicals: Profile[];
  current_medical: Profile;
  listDateMedicine: {
    time: string,
    id: string,
  };
}
const initState = {
  medical: null,
  medicals: [],
};
export default (state = initState, {type, payload}: DataDispatch) => {
  switch (type) {
    case types.CREATE_MEDICAL_PROFILE: {
      let {medicals} = state;
      const {data} = payload;
      medicals = [data, ...medicals];
      return {...state, medicals};
    }
    case types.GET_ALL_MEDICAL_PROFILE: {
      let {medicals} = payload;
      return {medicals};
    }
    case types.UPDATE_CURRENT_MEDICAL: {
      const {id} = payload;
      const {medicals} = state;
      let current_medical = null;
      medicals.forEach(it => {
        it.id === id && (current_medical = it);
      });
      return {...state, current_medical};
    }
    case types.DELETE_CARD_MEDICAL: {
      const {id} = payload;
      let {medicals} = state;
      medicals = medicals.filter(it => it.id !== id);
      return {...state, medicals};
    }
    case types.UPDATE_PROFILE: {
      let {current_medical, medicals} = state;
      const {profile, id} = payload;
      medicals = medicals.map(item => {
        item.id === id && (item = {...item, ...profile});
        return item;
      });
      current_medical?.id === id &&
        (current_medical = {...current_medical, ...profile});
      return {...state, medicals, current_medical};
    }
    case types.SET_LIST_DATE_MEDICINE: {
      return {...state, listDateMedicine: payload};
    }
    case types.UPDATE_ITEM_DATE_MEDICINE: {
      let {listDateMedicine} = state;
      listDateMedicine = listDateMedicine.map(item =>
        item.id === payload.id ? payload : item,
      );

      return {...state, listDateMedicine: [...listDateMedicine]};
    }
    default:
      return state;
  }
};
