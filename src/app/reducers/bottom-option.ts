import types, {DataDispatch} from '../action/types';
import {ListDataBottomOption} from '../action/bottom-option';
export interface StateBottomOption {
  isShow?: boolean;
  listData?: ListDataBottomOption[];
}
const initState: StateBottomOption = {
  isShow: false,
  listData: [],
};
export default (
  state = initState,
  {type, payload}: DataDispatch<StateBottomOption>,
): StateBottomOption => {
  switch (type) {
    case types.SHOW_BOTTOM_OPTION: {
      const {listData} = payload;
      return {isShow: true, listData};
    }
    case types.HIDE_BOTTOM_OPTION: {
      return {isShow: false};
    }
    default:
      return state;
  }
};
