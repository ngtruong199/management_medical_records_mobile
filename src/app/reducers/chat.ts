import types, {DataDispatch} from '../action/types';
import {ChatCard} from '../action/chat';
interface State {
  allChat?: ChatCard[];
  listSearch?: ChatCard[];
  currentChat?: ChatCard;
}
interface insideState {
  link: {uri: string};
}
const initState: State = {
  allChat: [],
  listSearch: [],
};
export default (
  state = initState,
  {type, payload}: DataDispatch<State & insideState>,
) => {
  switch (type) {
    case types.CREATE_CHAT_GROUP: {
      const {currentChat} = payload;
      return {...state, allChat: [currentChat, ...state.allChat]};
    }
    case types.GET_LIST_CHAT_GROUP: {
      const {allChat} = payload;
      return {...state, allChat};
    }
    case types.UPDATE_CHAT_GROUP: {
      const {currentChat} = payload;
      const {allChat: _allChat} = state;
      const allChat = _allChat?.map(item =>
        item?.id === currentChat.id ? currentChat : item,
      );
      return {...state, allChat};
    }
    case types.UPDATE_IMAGE_LINK_CHAT: {
      const {link, id} = payload;
      let {allChat} = state;
      allChat = allChat.map(item => {
        id === item.id && (item.avatarRender = link);
        return item;
      });
      return {...state, allChat};
    }
    case types.UPDATE_CONTENT_CHAT: {
      const {currentChat} = payload;
      let {allChat} = state;
      allChat = allChat.map(item => {
        currentChat.id === item.id && (item = {...item, ...currentChat});
        return item;
      });
      return {...state, allChat};
    }
    case types.GET_LIST_SEARCH_CHAT: {
      const {listSearch} = payload;
      return {...state, listSearch};
    }
    case types.UN_LINK_CHAT: {
      const {id} = payload;
      let {allChat} = state;
      allChat = allChat.filter(item => item.id !== id);

      return {...state, allChat};
    }
    default:
      return state;
  }
};
