import firebaseApp, {ReactNativeFirebase} from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import firestore, {firebase} from '@react-native-firebase/firestore';
// import '@firebase/auth';
// import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCofp5xQJlxga3rVDZlEBIn-x7Q7UdmXfg',
  authDomain: 'benhan-e5b9f.firebaseapp.com',
  databaseURL: 'https://benhan-e5b9f-default-rtdb.firebaseio.com',
  projectId: 'benhan-e5b9f',
  storageBucket: 'benhan-e5b9f.appspot.com',
  messagingSenderId: '532408129931',
  appId: '1:532408129931:web:18e2a5f48797c6d86f778e',
  measurementId: 'G-JW6D45BG9E',
};
if (!firebaseApp.apps.length) {
  firebaseApp.initializeApp(firebaseConfig).then(init => console.log('init'));
}
firestore().settings({
  persistence: true,
  cacheSizeBytes: firestore.CACHE_SIZE_UNLIMITED,
});
auth().languageCode = 'vi';
// firestore().disableNetwork();
// setTimeout(() => {
//   firestore().enableNetwork();
// }, 15000);
export const fireStore = firestore();
export const firebaseAuth = auth();
export default firebaseApp;
