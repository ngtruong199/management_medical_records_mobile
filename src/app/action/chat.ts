import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {create_UUID} from '../../utils/number';
import {fireStore} from '../config/firebaseConfig';
import {popNavigate} from '../navigation/rootNavigation';
import {dispatch} from '../store';
import {hideLoading, showLoading} from './alert';
import {getAuthCollection} from './auth';
import {
  getSingerFileInStorage,
  getUrlFileInStorage,
  pushFilesGotoStorage,
} from './img_firestore';
import actionTypes from './types';

const getChatUserCollection = () => getAuthCollection().collection('Chats');
const getChatDocument = (idChat: string) =>
  fireStore.collection('Chats').doc(idChat);
const getChatCollection = fireStore.collection('Chats');
export interface ChatCard {
  id?: string;
  name?: string;
  avatar?: {
    uri: string;
  };
  avatarRender?: {
    uri: string;
  };
  status: 'public' | 'private';
  note?: string;
  content?: string;
  timeSend?: FirebaseFirestoreTypes.Timestamp;
}
export interface ChatSearch {
  is_link: boolean;
}
export const createChatGroup = (chat: ChatCard) => {
  const callBack = async () => {
    const link = await getSingerFileInStorage(chat.avatar);
    dispatch({
      type: actionTypes.UPDATE_IMAGE_LINK_CHAT,
      payload: {link, id: doc_id},
    });
  };
  // showLoading();
  if (chat.avatar && typeof chat.avatar === 'object') {
    const [avatar] = pushFilesGotoStorage([chat.avatar], callBack);
    chat.avatar = avatar;
  }

  const doc_id = create_UUID();
  dispatch({
    type: actionTypes.CREATE_CHAT_GROUP,
    payload: {currentChat: {id: doc_id, ...chat}},
  });
  //Success
  popNavigate();
  hideLoading();
  //setData In Firebase

  getChatCollection.doc(doc_id).set(chat);
  getChatUserCollection().doc(doc_id).set({});
};
export const getListChat = async () => {
  showLoading();
  let allChat: ChatCard[] = [];
  const refChat = await getChatUserCollection().get();
  refChat.forEach(chatItem =>
    allChat.push({id: chatItem.id, ...chatItem.data()}),
  );
  allChat = await Promise.all(
    allChat.map(async item => {
      const chatItem: ChatCard = await (
        await getChatCollection.doc(item.id).get()
      ).data();
      const [avatarRender] = await getUrlFileInStorage([chatItem.avatar]);
      chatItem.avatarRender = avatarRender;

      return {id: item.id, ...chatItem};
    }),
  );
  dispatch({
    type: actionTypes.GET_LIST_CHAT_GROUP,
    payload: {allChat},
  });
  getAllChat();
  hideLoading();
};
export const getAllChat = () => {
  let listSearch = [];
  getChatCollection.get().then(async colRef => {
    colRef.forEach(item => {
      const data: ChatCard = item.data() as ChatCard;
      data.status === 'public' && listSearch.push({id: item.id, ...data});
    });
    listSearch = await Promise.all(
      listSearch.map(async (item, index) => {
        const [avatarRender] = await getUrlFileInStorage([item.avatar]);
        item.avatarRender = avatarRender;
        return item;
      }),
    );

    dispatch({
      type: actionTypes.GET_LIST_SEARCH_CHAT,
      payload: {listSearch},
    });
  });
};
export const linkedChat = (currentChat: ChatCard) => {
  dispatch({
    type: actionTypes.CREATE_CHAT_GROUP,
    payload: {currentChat},
  });
  getChatUserCollection().doc(currentChat.id).set({});
};
export const unlinkChat = (idChat: string) => {
  dispatch({
    type: actionTypes.UN_LINK_CHAT,
    payload: {id: idChat},
  });
  getChatUserCollection().doc(idChat).delete();
};
export const getChatRef = (idChat: string) =>
  getChatCollection.doc(idChat).collection('messages');
export const sendNewMess = (idChat: string, content: string) => {
  dispatch({
    type: actionTypes.UPDATE_CONTENT_CHAT,
    payload: {currentChat: {id: idChat, content}},
  });
  getChatCollection.doc(idChat).update({content});
};
