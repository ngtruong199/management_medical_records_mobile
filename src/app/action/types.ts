//
// Auth
//
const actionTypes = {
  /**
   * Auth
   */
  OPENED_REMEMBER: 'OPENED_REMEMBER',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  UPDATE_CURRENT_SCENE: 'UPDATE_CURRENT_SCENE',
  RESET_PASSWORD: 'RESET_PASSWORD',
  UPDATE_USER_INFO: 'UPDATE_USER_INFO',
  UPDATE_ME: 'UPDATE_ME',
  UPDATE_IMAGE_LINK_PROFILE: 'UPDATE_IMAGE_LINK_PROFILE',
  /**
   * language
   */
  CHANGE_LAGUAGE: 'CHANGE_LAGUAGE',
  /**
   * Post
   */
  /**
   * Medical
   */
  CREATE_MEDICAL_PROFILE: 'CREATE_MEDICAL_PROFILE',
  DELETE_CARD_MEDICAL: 'DELETE_CARD_MEDICAL',
  UPDATE_CURRENT_MEDICAL: 'UPDATE_CURRENT_MEDICAL',
  UPDATE_PROFILE: 'UPDATE_PROFILE',
  GET_ALL_MEDICAL_PROFILE: 'GET_ALL_MEDICAL_PROFILE',
  GET_POST: 'GET_POST',
  SCHEDULE_NOTIFICATION: 'SCHEDULE_NOTIFICATION',
  /**
   * User Management
   */
  CREATE_USER_MEDICAL_PROFILE: 'CREATE_USER_MEDICAL_PROFILE',
  GET_ALL_USER_MEDICAL_PROFILE: 'GET_ALL_USER_MEDICAL_PROFILE',
  UPDATE_USER_MEDICAL: 'UPDATE_USER_MEDICAL',
  DELETE_USER_MEDICAL: 'DELETE_USER_MEDICAL',
  /**
   * Chats
   */
  CREATE_CHAT_GROUP: 'CREATE_CHAT_GROUP',
  UPDATE_CHAT_GROUP: 'UPDATE_CHAT_GROUP',
  GET_LIST_CHAT_GROUP: 'GET_LIST_CHAT_GROUP',
  UPDATE_IMAGE_LINK_CHAT: 'UPDATE_IMG_LINK',
  UPDATE_CONTENT_CHAT: 'UPDATE_CONTENT_CHAT',
  GET_LIST_SEARCH_CHAT: 'GET_LIST_SEARCH_CHAT',
  UN_LINK_CHAT: 'UN_LINK_CHAT',
  /**
   * List type
   */
  SET_LIST_TYPE: 'SET_LIST_TYPE',
  SET_LIST_DATE_MEDICINE: 'SET_LIST_TYPE_MEDICINE',
  UPDATE_ITEM_DATE_MEDICINE: 'UPDATE_ITEM_DATE_MEDICINE',
  /**
   * Alert
   */
  SHOW_ALERT: 'S_ALERT',
  HIDE_ALERT: 'H_ALERT',
  /**
   * Loading
   */
  SHOW_LOADING: 'S_LOADING',
  HIDE_LOADING: 'H_LOADING',
  /**
   * Bottom Option
   */
  SHOW_BOTTOM_OPTION: 'S_BOTTOM_OPTION',
  HIDE_BOTTOM_OPTION: 'H_BOTTOM_OPTION',
};
export interface DataDispatch<Props> {
  type: string;
  payload: Props;
}
export default actionTypes;
