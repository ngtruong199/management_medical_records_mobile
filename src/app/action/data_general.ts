import {fireStore} from '../config/firebaseConfig';
import _ from 'lodash';
import {hideLoading, showLoading} from './alert';

export type StatusRequest = 'OK' | 'FAIL';

interface Medical_General {
  type_disease?: string[];
  name_disease?: string[];
  type_day_way?: Object[];
  medicine_amount?: string[];
  gender?: string[];
  notification?: {
    title: string;
    message: string;
  };
}
export let _general: Medical_General = {type_disease: []};
// const
const rootCollection = fireStore.collection('Medical_General');
export const getGeneralDataMedical = () => {
  fireStore
    .collection('Medical_General')
    .get()
    .then(snapShort => {
      snapShort.forEach(doc => {
        _general = {..._general, ...doc.data()};
      });
    });
};
let prev_type_disease: string = null;
export const getListDisease = async (
  type_disease: string,
  callback: (list?: string[]) => void,
) => {
  if (prev_type_disease === type_disease) {
    callback(_general.name_disease);
    return;
  }
  showLoading();
  prev_type_disease = type_disease;
  rootCollection
    .doc('Medical')
    .collection('type_disease')
    .doc(type_disease)
    .get()
    .then(data => {
      hideLoading();

      const list_name = data?.data()?.name_disease || [];
      _general.name_disease = !_.isEmpty(list_name) ? list_name : [];
      callback(list_name);
    })
    .catch(err => (_general.name_disease = []));
};
const updateDataMedicalGeneral = () => {
  //Type Disease
  // fireStore
  //   .collection('Medical_General')
  //   .doc('Medical')
  //   .update({
  //     type_disease: [
  //       'Bệnh Tim Mạch',
  //       'Bệnh Tiêu Hoá',
  //       'Bệnh Thần Kinh',
  //       'Bệnh Cơ Xương Khớp',
  //       'Bệnh Da',
  //       'Bệnh Hô Hấp',
  //       'Bệnh Răng Hàm Mặt',
  //       'Bệnh Phụ Khoa',
  //       'Bệnh Ung Bướu',
  //     ],
  //   });
};
