import {dispatch} from '../store';
import {iconsBottomOption} from '../ui/icons/typeBottom';
import actionTypes from './types';

// interface Props {
//   listData: {
//     title: string;
//     onPress: () => void;
//     icon?: {
//       name: string;
//       color: string;
//     };
//   }[];
// }
export interface ListDataBottomOption {
  title: string;
  icon?: {
    name: string;
    color: string;
  };
  onPress: () => void;
}
export interface BottomOptionInput {
  onPress: () => void;
  option: OptionBottomType;
  title_plus?: string;
}
export const showBottomOption = (options: BottomOptionInput[]) => {
  const listData = options.map(item => getDataOption(item));
  dispatch({type: actionTypes.SHOW_BOTTOM_OPTION, payload: {listData}});
};
export const hideBottomOption = () => {
  dispatch({type: actionTypes.HIDE_BOTTOM_OPTION});
};
const getDataOption = (objOption: BottomOptionInput) => ({
  title: `${titleObject[objOption.option]} ${objOption.title_plus || ''}`,
  icon: iconsBottomOption[objOption.option],
  onPress: objOption.onPress,
});
type OptionBottomType =
  | 'delete'
  | 'edit'
  | 'create'
  | 'camera'
  | 'choosePicture';
const titleObject = {
  ['delete']: 'Xóa',
  ['edit']: 'Chỉnh sửa',
  ['create']: 'Tạo mới',
  ['camera']: 'Chụp ảnh',
  ['choosePicture']: 'Chọn ảnh',
};
