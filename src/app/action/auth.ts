import {call} from 'react-native-reanimated';
import {
  create_UUID,
  formatValidPhoneNumber,
  validatePhoneNumber,
} from '../../utils/number';
import {firebaseAuth, fireStore} from '../config/firebaseConfig';
import {AuthInput} from '../store/types/auth';
import {Auth} from '../store/object/auth';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import types, {DataDispatch} from './types';
import {dispatch} from '../store';
import {setItemStorage} from '../storage';
import {typeStorage} from '../storage/types';
import actionTypes from './types';
import {hideLoading, showAlert, showLoading} from './alert';
import {getSingerFileInStorage, pushFilesGotoStorage} from './img_firestore';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {popNavigate} from '../navigation/rootNavigation';

GoogleSignin.configure({
  webClientId:
    '532408129931-j57i30u3jfaa45cse6i3fbkor1v4je78.apps.googleusercontent.com',
});
export let isGoogleSignin = false;

// Somewhere in your code
export const signIn = () => {
  showLoading();
  GoogleSignin.signIn()
    .then(user => {
      const googleCredential = auth.GoogleAuthProvider.credential(user.idToken);
      firebaseAuth.signInWithCredential(googleCredential);
    })
    .catch(error => showAlert('Hủy Đăng nhập', 'Bạn đã hủy đăng nhập google'))
    .finally(() => hideLoading());
};
export const GooleSignout = () => {
  try {
    GoogleSignin.revokeAccess();
    GoogleSignin.signOut();
  } catch (error) {
    console.error(error);
  }
};
interface User {
  id?: string;
  username?: string;
  password?: string;
  avatar?: string;
  phoneNumber?: string;
}
export interface Profile {
  address?: string;
  avatarRender?: {
    uri: string;
  };
  avatar?: {
    uri: string;
  };
  bloodPressure?: string;
  dateOfBirth?: FirebaseFirestoreTypes.Timestamp;
  gender?: string;
  glycemic_index?: string;
  height?: string;
  name?: string;
  weight?: string;
  note?: string;
}

export let user_id = 'phuongUserTest';
export const setUserId = (_user_id: string) => {
  user_id = _user_id;
};
// export const verifyCodeLogin = async (verifyCode: string) => {
//   const credential = auth.PhoneAuthProvider.credential(
//     confirmLogin.verificationId,
//     verifyCode,
//   );
//   firebaseAuth
//     .signInWithCredential(credential)
//     .then(_user => {
//       const {
//         user: {uid: _id, phoneNumber: phoneNumber},
//       } = _user;
//       setUserId(_id);
//       setItemStorage(typeStorage.ID_USER, user_id);

//       const user = {_id, phoneNumber};
//       dispatch({type: types.LOGIN_SUCCESS, payload: {...user}});
//       getUserProfile();
//     })
//     .catch(error => {
//       showAlert('Login', 'Mã xác nhận không chính xác');
//       console.log(error);
//     });
//   // confirmLogin
//   //   .confirm(verifyCode)
//   //   .then(_user => {
//   //     const {
//   //       user: {uid: _id, phoneNumber: phoneNumber},
//   //     } = _user;
//   //     user_id = _id;

//   //     setItemStorage(typeStorage.ID_USER, user_id);

//   //     const user = {_id, phoneNumber};
//   //     dispatch({type: types.LOGIN_SUCCESS, payload: {...user}});
//   //     getUserProfile();
//   //   })
//   //   .catch(err => {
//   //     showAlert('Login', 'Mã xác nhận không chính xác');
//   //     console.log(err);
//   //   });
// };

export const loginAuth = ({phoneNumber, callback}: AuthInput) => {
  showLoading();
  !validatePhoneNumber(phoneNumber) &&
    firebaseAuth
      .signInWithPhoneNumber(formatValidPhoneNumber(phoneNumber))
      .then(confirm => {
        callback && callback(confirm);
      })
      .catch(error => {
        showAlert(
          'Lỗi Đăng Nhập',
          error.code === 'auth/too-many-requests'
            ? 'Số điện thoại đã được gửi quá nhiều lần'
            : 'Số điện thoại không hợp lệ',
        );
        hideLoading();
        callback && callback(null);
      })
      .finally(() => hideLoading());
};
export const updateInfo = (auth: Auth) => {
  // const {displayName, photoURL, phoneNumber} = auth;

  getAuthCollection().collection('Profile').doc('data').update(auth);
};
export const getUserInfo = (id: string) => {
  getAuthCollection(id)
    .collection('Profile')
    .doc('data')
    .get()
    .then(user => {
      dispatch({type: actionTypes.UPDATE_USER_INFO, payload: {...user.data()}});
    });
};
export const getAuthCollection = (id?: string) =>
  fireStore.collection('Users').doc(id || user_id);

export const logoutAuth = () => {
  isGoogleSignin && GooleSignout();
  setItemStorage(typeStorage.ID_USER, '');
  firebaseAuth.signOut();
  dispatch({type: types.LOGOUT_SUCCESS, payload: {}});
};
export const updateLogin = (_user: FirebaseAuthTypes.User) => {
  const {uid: _id, phoneNumber: phoneNumber} = _user;
  setUserId(_id);

  setItemStorage(typeStorage.ID_USER, _id);
  !phoneNumber && (isGoogleSignin = true);
  const user = {_id, phoneNumber};
  dispatch({type: actionTypes.LOGIN_SUCCESS, payload: {...user}});
  getUserProfile();
};
export const getUserProfile = async () => {
  getAuthCollection()
    .collection('Profile')
    .doc('data')
    .get()
    .then(async data => {
      const dataProfile = data.data();
      dispatch({type: actionTypes.UPDATE_USER_INFO, payload: {...dataProfile}});
      const avatarRender =
        dataProfile?.avatar &&
        (await getSingerFileInStorage(dataProfile?.avatar));
      dispatch({
        type: actionTypes.UPDATE_USER_INFO,
        payload: {avatarRender, id: user_id},
      });
    });
};
export const updateProfile = (profile: Profile) => {
  showLoading();
  const callBack = async () => {
    const avatarRender = await getSingerFileInStorage(profile.avatar);
    dispatch({
      type: actionTypes.UPDATE_USER_INFO,
      payload: {avatarRender, id: user_id},
    });
  };

  if (profile.avatar && typeof profile.avatar === 'object') {
    const [avatar] = pushFilesGotoStorage([profile.avatar], callBack);
    profile.avatar = avatar;
  }
  showAlert('Thành Công', 'Cập nhật thông tin cá nhân thành công');
  popNavigate();
  dispatch({type: types.UPDATE_USER_INFO, payload: {...profile}});

  getAuthCollection().collection('Profile').doc('data').set(profile);
};
