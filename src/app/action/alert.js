import store from '../store'
import actionTypes from './types'

export const showAlert = (
  title,
  message,
  onPressPositiveButton,
  onNegativeButtonPress
) => {
  store.dispatch({
    type: actionTypes.SHOW_ALERT,
    payload: {
      title,
      message,
      onPressPositiveButton,
      onNegativeButtonPress
    }
  })
}
export const hideAlert = () => {
  store.dispatch({ type: actionTypes.HIDE_ALERT })
}
export const showLoading = () => {
  store.dispatch({ type: actionTypes.SHOW_LOADING })
}
export const hideLoading = () => {
  store.dispatch({ type: actionTypes.HIDE_LOADING })
}
