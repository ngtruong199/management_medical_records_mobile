import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {fireStore} from '../config/firebaseConfig';
import {getString} from '../locate';
import {navigate, popNavigate} from '../navigation/rootNavigation';
import store, {dispatch} from '../store';
import {hideAlert, hideLoading, showAlert, showLoading} from './alert';
import {getAuthCollection} from './auth';
import {
  currentID_User_Profile,
  getListTypeDisease,
  UserItem,
} from './auth-management';
import {pushNotificationLocal, removePushNoti} from './notification';
import actionTypes from './types';
import _ from 'lodash';
import moment from 'moment';
import {
  displayStringDate,
  isBeforeDay_Now,
  toDate,
  toMoment,
  toTimeCompare,
  toTimeStamp,
} from '../../utils/date';
import {create_UUID} from '../../utils/number';
export interface Profile {
  id?: string;
  type_name?: string;
  date_create?: FirebaseFirestoreTypes.Timestamp;
  disease_name?: string;
  date_re_examination?: FirebaseFirestoreTypes.Timestamp[];
  select_card?: number;
  clinic_address?: string;
  doctor_phone?: string;
  doctor_name?: string;
  medicine_time?: DateMedicine[];
  medicine_start?: FirebaseFirestoreTypes.Timestamp;
  medicine_end?: FirebaseFirestoreTypes.Timestamp;
  user_id: string;
}
export interface Medicine {
  date_start: FirebaseFirestoreTypes.Timestamp;
  date_end: FirebaseFirestoreTypes.Timestamp;
  drug_data: DrugData[];
  day_medicine: string;
  day_way_type: string;
  medicine_time: FirebaseFirestoreTypes.Timestamp[];
  enableNotification: boolean;
  reminiscentName: string;
  notification: {
    listTime: Date[];
    listId: number[];
  };
}
//medicine_start-medicine_end
export interface DrugData {
  drug_name: string;
  amount: {
    amount: string;
    unit: string;
  };
  note: string;
  list_img: {
    uri: string;
  }[];
  num_pills_day: string;
}
export interface DateMedicine {
  time: string;
  id: string;
  name: string;
}
export let current_type_name = 'medical_profile';
export let current_id_medical = '';
export const setCurrent_Id_Medical = (id: string) => (current_id_medical = id);
export const setCurrentType = (type: string, notOpen: boolean) => {
  current_type_name = type;
  !notOpen && navigate('Medical_List');
};
const _root_Collection = (type_name?: string) =>
  getAuthCollection().collection('ProfileMedical');
const getFilterCollection = () =>
  getAuthCollection()
    .collection('Management_User')
    .doc(currentID_User_Profile)
    .collection('FilterData');
export const getMedicalDocument = (data: any): Profile => {
  return {id: data.id, ...data.data()};
};
const deleteWithType = (type: string) => {
  _root_Collection()
    .where('type_name', '==', type)
    .get()
    .then(cards => {
      cards.forEach(card => card.ref.delete());
    });
};
export const deleteListType = (type_name: string) => {
  const callback = (listType: {}) => {
    delete listType[type_name];
    setTypeMedical(listType);
  };
  deleteWithType(type_name);
  getListTypeDisease(callback);
};
export const deleteMedical = (id: string) => {
  showLoading();
  _root_Collection().doc(id).delete();
  dispatch({type: actionTypes.DELETE_CARD_MEDICAL, payload: {id}});
  hideLoading();
};
export const createMedical = (_profile: Profile) => {
  showLoading();
  addTypeMedical({type: _profile.type_name});
  _root_Collection(_profile.type_name)
    .add(_profile)
    .then(docRef =>
      docRef.get().then(data => {
        dispatch({
          type: actionTypes.CREATE_MEDICAL_PROFILE,
          payload: {data: getMedicalDocument(data)},
        });
        showAlert(
          getString('medical', 'create_medical'),
          getString('medical', 'create_medical_success'),
        );
        getListTypeDisease();
        popNavigate();
      }),
    )
    .catch(err => console.log(err));
};
export const getFilterCardMedical = ({
  dateStart,
  dateEnd,
  user_id,
  type_name,
  startMedicine,
  endMedicine,
  diseaseName,
}: {
  dateStart: Date;
  dateEnd: Date;
  user_id: string;
  type_name: string;
  diseaseName: string;
  startMedicine: FirebaseFirestoreTypes.Timestamp;
  endMedicine: FirebaseFirestoreTypes.Timestamp;
}) => {
  let medicals: Profile[] = [];
  showLoading();
  let ref: FirebaseFirestoreTypes.Query<FirebaseFirestoreTypes.DocumentData> =
    _root_Collection();

  user_id && (ref = ref.where('user_id', '==', user_id));
  type_name && (ref = ref.where('type_name', '==', type_name));
  diseaseName && (ref = ref.where('disease_name', '==', diseaseName));
  dateStart &&
    (ref = ref.where('date_create', '>=', toTimeCompare(dateStart, true)));
  dateEnd && (ref = ref.where('date_create', '<=', toTimeCompare(dateEnd)));

  ref
    .orderBy('date_create', 'desc')
    .get()
    .then(collection => {
      collection.forEach(it => {
        const medical = getMedicalDocument(it);
        if (
          startMedicine
            ? medical.medicine_start &&
              moment(toTimeCompare(startMedicine, true)).isBefore(
                toMoment(medical.medicine_end),
              )
            : true && endMedicine
            ? medical.medicine_end &&
              moment(toTimeCompare(endMedicine)).isAfter(
                toMoment(medical.medicine_start),
              )
            : true
        )
          medicals.push(medical);
      });
      !user_id && (medicals = filterMultipleUser(medicals));
      dispatch({
        type: actionTypes.GET_ALL_MEDICAL_PROFILE,
        payload: {medicals},
      });
      popNavigate();
      navigate('Medical_List', {isFilter: true});
      hideAlert();
    });
};
const filterMultipleUser = (medicals: Profile[]) => {
  const _users = (store.getState()?.auth_management?.allUser ||
    []) as UserItem[];
  const usersObject = {};
  _users.forEach(({id, name}) => (usersObject[id] = name));
  const usersCard: any = {};
  medicals.map(item => {
    const name = usersObject[item.user_id];
    const oldProfile = usersCard[name] || [];
    usersCard[name] = [...oldProfile, item];
  });
  const medicals_result = [];
  Object.keys(usersCard).forEach(item => {
    medicals_result.push(item, ...usersCard[item]);
  });
  return medicals_result;
};
export const getAllCardMedicalNoDP = () => {
  showLoading();
  const medicals: Profile[] = [];
  _root_Collection()
    .orderBy('date_create', 'desc')
    .get()
    .then(collection => {
      collection.forEach(it => {
        medicals.push(getMedicalDocument(it));
      });
      dispatch({
        type: actionTypes.GET_ALL_MEDICAL_PROFILE,
        payload: {medicals},
      });
      hideAlert();
    });
};
export const deleteCardWithUser_ID = (user_id: string) => {
  _root_Collection()
    .where('user_id', '==', user_id)
    .get()
    .then(cards => {
      cards.forEach(user => {
        user.ref.delete();
      });
    });
};
export const getAllCardMedical = () => async (dispatch: any) => {
  const medicals: Profile[] = [];
  _root_Collection()
    .where('user_id', '==', currentID_User_Profile)
    .where('type_name', '==', current_type_name)
    .orderBy('date_create', 'desc')
    .get()
    .then(collection => {
      collection.forEach(it => {
        medicals.push(it.data());
      });
      dispatch({
        type: actionTypes.GET_ALL_MEDICAL_PROFILE,
        payload: {medicals},
      });
      hideAlert();
    });
};
export const updateCurrentMedical =
  (id: string, _profile: any) => async (dispatch: any) => {
    current_id_medical = id;
    dispatch({
      type: actionTypes.UPDATE_CURRENT_MEDICAL,
      payload: {id},
    });
  };
export const updateDataProfile = (oldProfile: Profile, _profile: Profile) => {
  if (!isBeforeDay_Now(_profile?.date_create.toDate())) return;
  if (oldProfile.type_name !== _profile.type_name) {
    _root_Collection(oldProfile.type_name)?.doc(oldProfile.id)?.delete();
  }
  addTypeMedical({type: _profile.type_name});
  showLoading();
  _root_Collection(_profile.type_name)
    .doc(oldProfile.id)
    .set(_profile)
    .then(() => {
      showAlert(
        getString('medical', 'save'),
        getString('medical', 'save_medical_success'),
      );
      dispatch({
        type: actionTypes.UPDATE_PROFILE,
        payload: {id: oldProfile.id, profile: _profile},
      });
    })
    .finally(() => hideLoading());
};
export const updateMedical = ({
  profile,
  oldProfile,
}: {
  profile?: Profile;
  oldProfile?: Profile;
}) => {
  showLoading();
  const isUpdate = !!oldProfile;
  !profile.id && (profile.id = create_UUID());
  addTypeMedical({type: profile.type_name, oldType: oldProfile?.type_name});

  getAuthCollection()
    .collection('ProfileMedical')
    .doc(profile.id)
    .set({...profile, user_id: currentID_User_Profile})
    .then(() => {
      !isUpdate
        ? dispatch({
            type: actionTypes.CREATE_MEDICAL_PROFILE,
            payload: {data: profile},
          })
        : dispatch({
            type: actionTypes.UPDATE_PROFILE,
            payload: {id: oldProfile.id, profile},
          });
      isUpdate
        ? showAlert(
            getString('medical', 'save'),
            getString('medical', 'save_medical_success'),
          )
        : showAlert(
            getString('medical', 'create_medical'),
            getString('medical', 'create_medical_success'),
          );
      getListTypeDisease();
      popNavigate();
    })
    .catch(() => showAlert('Lỗi', 'Tạo hồ sơ thất bại '));
};
const addTypeMedical = ({type, oldType}: {type: string; oldType?: string}) => {
  const refTypeMedical = getAuthCollection()
    .collection('Management_User')
    .doc(currentID_User_Profile);

  refTypeMedical.get().then(doc => {
    const type_disease = doc.data()?.type_disease || {};
    //Remove Old type in object
    if (oldType) {
      !type_disease[oldType] && --type_disease[oldType];
      !type_disease[oldType] && delete type_disease[oldType];
    }
    //Add New type in object

    type_disease[type] = (type_disease[type] || 0) + 1;
    //Put new medical in Database
    refTypeMedical.update({type_disease});
  });
};
const setTypeMedical = (type_disease: Object) => {
  getAuthCollection()
    .collection('Management_User')
    .doc(currentID_User_Profile)
    .update({type_disease});
  dispatch({
    type: actionTypes.SET_LIST_TYPE,
    payload: {type_disease: Object.keys(type_disease)},
  });
};
export const updateTimeFilterMedicine = (data: {
  timeStart: FirebaseFirestoreTypes.Timestamp;
  timeEnd: FirebaseFirestoreTypes.Timestamp;
}) => {};
export const updateMedicine = (
  currentMedical: Profile,
  medicine: Medicine,
  oldMedicine: Medicine,
  id: string,
) => {
  showLoading();
  if (
    !!oldMedicine?.enableNotification !== !!medicine?.enableNotification ||
    JSON.stringify(medicine.medicine_time) !==
      JSON.stringify(oldMedicine.medicine_time)
  ) {
    const time_medicine = medicine.medicine_time.map(item => {
      const _date = moment(toDate(item));
      return toTimeStamp(
        moment().set('h', _date.get('h')).set('m', _date.get('m')).toDate(),
      );
    });
    if (medicine.enableNotification) {
      let notification = pushNotificationLocal(
        time_medicine,
        medicine.date_start,
        medicine.date_end,
        medicine.day_medicine,
        currentMedical,
      );
      notification = {
        ...notification,
        info: {
          ...notification.info,
          type_name: currentMedical.type_name,
          ID_User_Profile: currentID_User_Profile,
          id_Medical: currentMedical.id,
        },
      };

      getAuthCollection()
        .collection('Notification')
        .doc(currentMedical.id)
        .set({
          user_id: currentID_User_Profile,
          notification,
        });
    } else {
      getAuthCollection()
        .collection('Notification')
        .doc(currentMedical.id)
        .get()
        .then(data => {
          const list_id = data?.data()?.listID || [];
          removePushNoti(list_id);
        });
    }
  }
  hideAlert();
  const idMedicine = id || create_UUID();
  putDateMedicine(
    {
      id: idMedicine,
      time: `${displayStringDate(
        medicine.date_start,
        'date',
      )} -> ${displayStringDate(medicine.date_end, 'date')}`,
      name: medicine.reminiscentName,
    },
    currentMedical.id,
    !!id,
    {medicine, oldMedicine},
  );
  _root_Collection()
    .doc(currentMedical.id)
    .collection('medicine')
    .doc(idMedicine)
    .set(medicine)
    .then(() => {
      popNavigate();
      getListTypeDisease();
    });
};
const refMedical = (id?: string) =>
  _root_Collection().doc(id || current_id_medical);

const putDateMedicine = (
  date: DateMedicine,
  medicalId: string,
  isUpdate: boolean,
  {medicine, oldMedicine}: {medicine: Medicine; oldMedicine: Medicine},
) => {
  refMedical(medicalId)
    .get()
    .then(data => {
      let {
        medicine_time = [],
        medicine_start,
        medicine_end,
      } = data.data() || {};

      (!medicine_start ||
        toMoment(medicine.date_start)?.isBefore(toMoment(medicine_start))) &&
        (medicine_start = medicine.date_start);

      (!medicine_end ||
        toMoment(medicine.date_end)?.isAfter(toMoment(medicine_end))) &&
        (medicine_end = medicine.date_end);

      isUpdate
        ? (medicine_time = medicine_time.map(item =>
            item.id === date.id ? date : item,
          ))
        : medicine_time.push(date);
      isUpdate &&
        dispatch({type: actionTypes.UPDATE_ITEM_DATE_MEDICINE, payload: date});
      refMedical(medicalId).update({
        medicine_time: medicine_time,
        medicine_start,
        medicine_end,
      });
    });
};
export const deleteDateMedicine = (id: string) => {
  showLoading();
  const refMedical = _root_Collection().doc(current_id_medical);
  refMedical.get().then(data => {
    let {medicine_time = []} = data.data();
    medicine_time = medicine_time.filter(item => item.id !== id);
    refMedical
      .update({medicine_time})
      .then(() => {
        showAlert('Xóa thành công', 'Bạn đã xóa toa thuốc thành công');
        getListDateMedicine();
      })
      .catch(() => showAlert('Xóa thất bại', 'Không thể xóa thuốc này'));
    refMedical.collection('medicine').doc(id).delete();
  });
};
export const getListDateMedicine = () => {
  refMedical()
    .get()
    .then((dt: {data(): Profile}) => {
      const listDateMedicine = dt.data()?.medicine_time || [];

      dispatch({
        type: actionTypes.SET_LIST_DATE_MEDICINE,
        payload: listDateMedicine,
      });
    });
};
export const getMedicine = ({
  id,
  callback,
  medicine_id,
}: {
  id: string;
  callback: any;
  medicine_id: string;
}) => {
  showLoading();
  _root_Collection()
    .doc(id)
    .collection('medicine')
    .doc(medicine_id || 'data')
    .get()
    .then(item => {
      hideLoading();
      callback(item.data());
    });
};
