import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import {create_UUID} from '../../utils/number';
import {navigate, popNavigate} from '../navigation/rootNavigation';
import {dispatch} from '../store';
import {isImageSystem} from '../ui/imagePath';
import {hideLoading, showLoading} from './alert';
import {getAuthCollection} from './auth';
import {getUrlFileInStorage, pushFilesGotoStorage} from './img_firestore';
import {deleteCardWithUser_ID} from './medical-profile';
import actionTypes from './types';

export interface UserItem {
  id?: string;
  avatar: {
    uri: string;
  };
  name: string;
  gender: string;
  birthday: FirebaseFirestoreTypes.Timestamp;
  note: string;
  avatarRender?: {
    uri: string;
  };
}
export let currentID_User_Profile = null;
export const setCurrentIdProfile = (id: string) => {
  currentID_User_Profile = id;
};
const getCollectionManagement = () =>
  getAuthCollection().collection('Management_User');

export const getDocumentManagement = (id?: string) =>
  getCollectionManagement().doc(id);
export const getListUser = (callback: any) => {
  getAuthCollection()
    .get()
    .then(data => {
      callback(data.data()?.listUser || {});
    });
};
export const registerUserMedicalProfile = (user: UserItem) => {
  showLoading();
  if (
    user?.avatar &&
    typeof user.avatar === 'object' &&
    isImageSystem(user.avatar)
  ) {
    const [avatar] = pushFilesGotoStorage([user.avatar]);
    user.avatar = avatar;
  }

  const doc_id = user.id || create_UUID();
  updateFieldUser({
    id: doc_id,
    name: user.name,
    status: user.id ? 'UPDATE' : 'NEW',
  });
  dispatch({
    type: user.id
      ? actionTypes.UPDATE_USER_MEDICAL
      : actionTypes.CREATE_USER_MEDICAL_PROFILE,
    payload: {currentUser: {...user, id: doc_id}},
  });
  //Success
  popNavigate();
  hideLoading();
  //setData In Firebase
  getCollectionManagement().doc(doc_id).set(user);
};
export const updateFieldUser = ({
  id,
  name = '',
  status = 'NEW',
}: {
  id: string;
  name: string;
  status: 'DELETE' | 'UPDATE' | 'NEW';
}) => {
  getAuthCollection()
    .get()
    .then(data => {
      let listUser = data.data()?.listUser || {};

      status !== 'NEW' &&
        delete listUser[
          Object.keys(listUser)[Object.values(listUser).indexOf(id)]
        ];

      status !== 'DELETE' && (listUser[name] = id);
      getAuthCollection().set({listUser});
    });
};
export const getAllUserManagement = async () => {
  showLoading();
  const allUser = [];
  const snap = await getCollectionManagement().get();

  snap.forEach(doc => {
    allUser.push({...(doc.data() || {}), id: doc.id});
  });

  await Promise.all(
    allUser.map(async item => {
      const [avatarRender] = await getUrlFileInStorage([item.avatar]);
      item.avatarRender = avatarRender;
      return item;
    }),
  );
  dispatch({
    type: actionTypes.GET_ALL_USER_MEDICAL_PROFILE,
    payload: {allUser},
  });
  hideLoading();
};
export const getListTypeDisease = (
  callback?: (type_disease: string[]) => void,
) => {
  getAuthCollection()
    .collection('Management_User')
    .doc(currentID_User_Profile)
    .get()
    .then(doc => {
      const type_disease = doc.data()?.type_disease || {};
      callback
        ? callback(type_disease)
        : dispatch({
            type: actionTypes.SET_LIST_TYPE,
            payload: {type_disease: Object.keys(type_disease)},
          });
    });
};
export const deleteUser = (user_id: string) => {
  updateFieldUser({id: user_id, status: 'DELETE', name: ''});
  getCollectionManagement().doc(user_id).delete();
  deleteCardWithUser_ID(user_id);
  dispatch({type: actionTypes.DELETE_USER_MEDICAL, payload: {user_id}});
};
// export const editUser = (user_id)
export const updateCurrentUser = (id: string) => {
  currentID_User_Profile = id;
  getListTypeDisease();
  navigate('ListTypeName');
};
