import storage from '@react-native-firebase/storage';
import {isParameter} from 'typescript';
import {create_UUID} from '../../utils/number';

export const pushFilesGotoStorage = (params = [], callback) =>
  params.map(item => {
    if (item?.uri.includes('file://')) {
      //Random name file
      const fileName = `${create_UUID()}.jpg`;
      storage()
        .ref(fileName)
        .putFile(item.uri)
        .then(() => callback && callback())
        .catch(err => console.log(err));
      return {uri: fileName};
    }
    return item;
  });
export const getUrlFileInStorage = async (params = [{uri: ''}]) =>
  await Promise.all(
    params.map(async item => {
      if (!!item && typeof item?.uri === 'string') {
        const url_remote =
          !item.uri.includes('file://') &&
          !item.uri.includes('http') &&
          (await storage().ref(item.uri).getDownloadURL());
        return {uri: url_remote} || item;
      }
      return item;
    }),
  );
export const getSingerFileInStorage = async (params: {uri: string}) => {
  if (!!params && typeof params?.uri === 'string') {
    const url_remote =
      !params.uri.includes('file://') && !params.uri.includes('http')
        ? await storage().ref(params.uri).getDownloadURL()
        : params.uri;
    return {uri: url_remote} || params;
  }
  return params;
};
