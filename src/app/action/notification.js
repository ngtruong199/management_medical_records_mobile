import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';
import moment from 'moment';
import PushNotification, {Importance} from 'react-native-push-notification';
import {toDate, toMoment, toTimeCompare} from '../../utils/date';
import {isIOS} from '../../utils/platform';
import {navigate} from '../navigation/rootNavigation';
import {showAlert} from './alert';
import {getAuthCollection, setUserId} from './auth';
import {currentID_User_Profile, setCurrentIdProfile} from './auth-management';
import {_general} from './data_general';
import {
  current_type_name,
  getMedicalDocument,
  Profile,
  setCurrentType,
  setCurrent_Id_Medical,
} from './medical-profile';

export interface NotificationData {
  time: FirebaseFirestoreTypes.Timestamp;
  message: string;
  id_Medical: string;
  type_name: string;
  title: string;
  ID_User_Profile: string;
  id: string;
  user_id: string;
}

PushNotification.createChannel(
  {
    channelId: 'medicalapp_05.2021', // (required)
    channelName: 'Medical App', // (required)
    channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
    soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
  },
  created => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
);

const minutesOfDay = (date: Date) => {
  const m = moment(date);
  return m.minutes() + m.hours() * 60;
};
const randomNumber = () => parseInt((Date.now() * Math.random()) / 10000);

const convertNameFunc = (name, return_string) => {
  // eslint-disable-next-line no-new-func
  return new Function(`let name='${name}';return \`${return_string}\``)();
};
export const pushNotificationLocal = (
  time: FirebaseFirestoreTypes.Timestamp[],
  dateStart: FirebaseFirestoreTypes.Timestamp,
  dateEnd: FirebaseFirestoreTypes.Timestamp,
  day_way: string,
  profile: Profile,
) => {
  const listTime: Date[] = [];
  const listId: number[] = [];
  const day_medicine = parseInt(day_way) || 1;
  //Message
  const message = convertNameFunc(
    profile?.disease_name || '' || '',
    _general?.notification?.message || '',
  );
  const title = convertNameFunc(
    profile?.type_name || '',
    _general?.notification?.title || '',
  );
  time?.forEach(item => {
    let date_pushNoti = toDate(item);
    [1, 2, 3, 4, 5, 6, 7, 8].forEach(() => {
      if (moment(date_pushNoti).isAfter(moment(toTimeCompare(dateEnd, false))))
        return;
      let date_reminder = date_pushNoti;
      [(1, 2, 3)].forEach(() => {
        if (minutesOfDay(date_reminder) > minutesOfDay(moment().toDate())) {
          const id = randomNumber();

          PushNotification.localNotificationSchedule({
            channelId: 'medicalapp_05.2021',
            id: id,
            date: date_reminder,
            title,
            message,
          });
          listTime.push(date_reminder);
          listId.push(id);
          date_reminder = moment(date_reminder).add(5, 'm').toDate();
        }
      });
      date_pushNoti = moment(date_pushNoti).add(day_medicine, 'd').toDate();
    });
  });
  return {listTime, listId, info: {title, message}};
};
export const delAllNotification = (refetch: () => void) => {
  const listPromise = [];
  getAuthCollection()
    .collection('Notification')
    .get()
    .then(ref => {
      ref.forEach(doc => {
        listPromise.push(doc.ref.delete());
      });
    });
  Promise.all(listPromise).then(() => refetch());
};
// export
export const clickNotification = (item: NotificationData) => {
  setCurrent_Id_Medical(item.id_Medical);
  setCurrentIdProfile(item.user_id);
  setCurrentType(item.type_name, true);

  getAuthCollection()
    .collection('ProfileMedical')
    .doc(item.id_Medical)
    .get()
    .then(_data => {
      const data = _data.data();
      !data
        ? showAlert(
            'Không thể mở hồ sơ này',
            '\t👉Nó có thể đã bị xóa .\n \t👉Bạn có muốn mở hồ sơ của loại bệnh này',
            () => {
              navigate('Medical_List');
            },
            () => {},
          )
        : navigate('Medical_Request', {
            current_medical: getMedicalDocument(_data),
            isEdit: true,
          });
    });
  //   .doc()
  //   .collection(item.type_name)
  //   .doc(item.id_Medical)
  //   .get()
  //   .then(data => {
  //   });
};
export const removePushNoti = (list: number[]) => {
  list.forEach(item => {
    PushNotification.cancelLocalNotifications({id: item});
  });
};
export const clickItem = (id, user_id) => {};
PushNotification.configure({
  onRegister: function (token) {
    console.log('TOKEN:', token);
  },

  onNotification: function (notification) {
    console.log('NOTIFICATION:', notification);

    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  onAction: function (notification) {
    console.log('ACTION:', notification.action);
    console.log('NOTIFICATION:', notification);
  },

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function (err) {
    console.error(err.message, err);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  popInitialNotification: true,

  requestPermissions: true,
});
