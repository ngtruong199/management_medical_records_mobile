import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Animated, {interpolate} from 'react-native-reanimated';
import {mix, useTransition} from 'react-native-redash/lib/module/v1';
import styled from 'styled-components/native';
import {isIOS, windowHeight, windowWidth} from '../../utils/platform';
import {TextMedium} from '../ui/themes/styles';
import _ from 'lodash';
import {FlatList} from 'react-native-gesture-handler';

const ListData = ({open, listData = [], onClick = value => {}}) => {
  const transition = useTransition(open);
  const IT_HEIGHT = _.isEmpty(listData)
    ? 0
    : listData.length > MAX_ITEM_HEIGHT
    ? MAX_ITEM_HEIGHT
    : listData.length;
  const height = mix(transition, 0, LIST_ITEM_HEIGHT * IT_HEIGHT);
  return (
    <ViewDataDropdown
      nestedScrollEnabled={true}
      style={[
        {
          overflow: 'hidden',
          paddingLeft: 12,
        },
        {height},
      ]}>
      {listData?.map((item, index) => (
        <ViewItemDropdown
          onPress={() => onClick(item)}
          key={index.toString()}
          activeOpacity={0.7}>
          <TitleInput>{item}</TitleInput>
        </ViewItemDropdown>
      ))}
    </ViewDataDropdown>
  );
};
export const DropListCustom = ({open=false,_height=0,children})=>{
  const transition = useTransition(open);
  const height = mix(transition, 0, _height);
  return (
    <DropdownNoShadow
      nestedScrollEnabled={true}
      style={[
        {
          overflow: 'hidden',
          paddingLeft: 12,
          width:windowWidth
        },
        {
          height
        }
      ]}>
      {children}
    </DropdownNoShadow>
  );
}

const MAX_ITEM_HEIGHT = 5;
const LIST_ITEM_HEIGHT = 50;

const TitleInput = styled(TextMedium)`
  color: #999;
  text-align: left;
  margin-bottom: 5px;
  margin-left: 5px;
  z-index: -1;
`;
const DropdownNoShadow = styled(Animated.ScrollView)`
  margin:0;
  padding:0;
`;
const ViewDataDropdown = styled(Animated.ScrollView)`
  border-color: #999;
  background-color: #fff;
  border-radius: 10px;
  z-index: 1;
  shadow-color: #777;
  shadow-radius: 5px;
  shadow-offset: 5px 5px;
  elevation: ${isIOS ? 2 : 5};
  shadow-opacity: 1;
`;
const ViewItemDropdown = styled.TouchableOpacity`
  border-bottom-width: 1px;
  border-bottom-color: #ded;
  padding-vertical: 8px;
  justify-content: center;
  height: 50px;
`;

export default ListData;
