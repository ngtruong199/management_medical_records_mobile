import React from 'react'
import Animated, { interpolateNode, Value } from 'react-native-reanimated'
import { withTimingTransition } from 'react-native-redash/lib/module/v1'
import Svg, { Path } from 'react-native-svg'
import styled from 'styled-components'
class DropdownIcon extends React.PureComponent {
  constructor (props) {
    super(props)
    this.animation = new Animated.Value(props.opened ? 1 : 0)
    this.transition = withTimingTransition(this.animation, {
      duration: 250
    })
    this.rotation = interpolateNode(this.transition, {
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg'],
      extrapolate: 'clamp'
    })
  }

  componentDidUpdate (prevProps) {
    if (prevProps.opened != this.props.opened) {
      this.animation.setValue(this.props.opened ? 1 : 0)
    }
  }

  render () {
    const { color } = this.props
    return (
      <AnimView style={{ transform: [{ rotate: this.rotation }] }}>
        <Svg
          x="0px"
          y="0px"
          width="20px"
          height="50px"
          viewBox="0 0 451.847 451.847"
          xmlSpace="preserve"
          enableBackground="new 0 0 451.847 451.847">
          <Path
            d="M225.923 354.706c-8.098 0-16.195-3.092-22.369-9.263L9.27 151.157c-12.359-12.359-12.359-32.397 0-44.751 12.354-12.354 32.388-12.354 44.748 0l171.905 171.915 171.906-171.909c12.359-12.354 32.391-12.354 44.744 0 12.365 12.354 12.365 32.392 0 44.751L248.292 345.449c-6.177 6.172-14.274 9.257-22.369 9.257z"
            fill="#777"
          />
        </Svg>
      </AnimView>
    )
  }
}

export default DropdownIcon

class Icon extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      width: this.props.width,
      height: this.props.height
    }
  }

  componentDidMount () {
    const { width, height } = this.state
    const { w, h } = this.props.icon.box
    const boxW = width || w
    const boxH = height || h
    this.setState({
      width: boxW,
      height: boxH
    })
  }

  render () {
    const { width, height } = this.state
    const { color, icon, style } = this.props
    const { path, box, ...res } = icon
    const { w, h } = box
    const viewBox = `0 0 ${w} ${h}`
    return (
      <IconStyle
        style={[this.state, style]}
        height={height}
        width={width}
        pointerEvents="none">
        <Svg height={height || 50} width={width || 50} viewBox={viewBox}>
          <Path
            fill={color}
            d={path}
            fillRule="evenodd"
            stroke={color || 'gray'}
            {...res}
          />
        </Svg>
      </IconStyle>
    )
  }
}
//
// Styled Components
//
const IconStyle = styled.View`
  overflow: hidden;
`

const AnimView = styled(Animated.View)`
  position: absolute;
  right: 16px;
  top: 0;
  bottom: 0;
  align-items: center;
  justify-content: center;
`
