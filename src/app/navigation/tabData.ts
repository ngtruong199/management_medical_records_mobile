import Chat from '../containers/chat';
import Notification from '../containers/notfication';
import Setting from '../containers/setting';

import Home from '../containers/home';
import Users from '../containers/users/users';

const datatab: TabScreen[] = [
  // {
  //   name: 'Home',
  //   component: Home,
  //   icon: {
  //     name: 'heartbeat',
  //     color: '#F80505',
  //   },
  // },
  {
    name: 'Users',
    component: Users,
    icon: {
      name: 'users',
      color: '#407BBF',
    },
  },
  {
    name: 'Chat',
    component: Chat,
    icon: {
      name: 'comment',
      color: '#27A5DD',
    },
  },
  {
    name: 'Notification',
    component: Notification,
    icon: {
      name: 'bell',
      color: '#F4D34C',
    },
  },
  {
    name: 'Setting',
    component: Setting,
    icon: {
      name: 'cogs',
      color: '#516771',
    },
  },
];
interface TabScreen {
  name: string;
  component: any;
  icon: {name: string; color: string};
}
export default datatab;
