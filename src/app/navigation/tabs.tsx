import React from 'react';
import {
  createBottomTabNavigator,
  BottomTabBar,
  BottomTabBarButtonProps,
} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import {_color} from '../ui/themes/Colors';
import {TouchableOpacity, View} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {isIphoneX} from 'react-native-iphone-x-helper';
import styled from 'styled-components/native';
import datatab from './tabData';

const SIZE_ICON_BOTTOM_BAR = 28;
const UNFOCUS_COLOR = _color.contentBlur;
const Tab = createBottomTabNavigator();

const CustomTabBar = (props: any) => {
  if (isIphoneX()) {
    return (
      <View>
        <IPX_View />
        <BottomTabBar {...props.props} />
      </View>
    );
  } else {
    return <BottomTabBar {...props.props} />;
  }
};
const Tabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          left: 0,
          bottom: isIphoneX() ? -20 : 10,
          right: 0,
          borderTopWidth: 0,
          backgroundColor: 'transparent',
          elevation: 0,
        },
      }}
      tabBar={props => <CustomTabBar props={props} />}>
      {datatab.map((item, index) => (
        <Tab.Screen
          key={`screen_${index}`}
          {...item}
          options={{
            tabBarIcon: ({focused}) => (
              <Icon
                name={item.icon.name}
                size={SIZE_ICON_BOTTOM_BAR}
                color={focused ? item.icon.color : UNFOCUS_COLOR}
              />
            ),
            tabBarButton: props => <TabBarCustomButton {...props} />,
          }}
        />
      ))}
    </Tab.Navigator>
  );
};
const TabBarCustomButton = (props: BottomTabBarButtonProps) => {
  const {accessibilityState, children, onPress} = props;
  if (accessibilityState?.selected) {
    return (
      <ContainerSVG>
        <SVGView>
          <CircleBottomBar />
          <Svg width={75} height={61} viewBox="0 0 75 61">
            <Path
              d="M75.2 0v61H0V0c4.1 0 7.4 3.1 7.9 7.1C10 21.7 22.5 33 37.7 33c15.2 0 27.7-11.3 29.7-25.9.5-4 3.9-7.1 7.9-7.1h-.1z"
              fill={_color.color_bottom}
            />
          </Svg>
          <CircleBottomBar />
        </SVGView>
        <CircleTouch
          style={{backgroundColor: _color.color_bottom}}
          onPress={onPress}>
          {children}
        </CircleTouch>
      </ContainerSVG>
    );
  } else {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          height: 60,
          backgroundColor: _color.color_bottom,
        }}
        activeOpacity={0.98}
        onPress={onPress}>
        {children}
      </TouchableOpacity>
    );
  }
};
export default Tabs;

const CircleBottomBar = styled.View`
  flex: 1;
  background-color: ${_color.color_bottom};
`;
const SVGView = styled.View`
  flex-direction: row;
  position: absolute;
  top: 0;
`;
const ContainerSVG = styled.View`
  flex: 1;
  align-items: center;
`;
const IPX_View = styled.View`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 30px;
  background-color: ${_color.white};
`;
const CircleTouch = styled.TouchableOpacity`
  top: -22.5px;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 50px;
  border-radius: 25px;
  background-color: ${_color.white};
`;
