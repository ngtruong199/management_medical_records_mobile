import {NavigationContainerRef} from '@react-navigation/native';
import * as React from 'react';

export const navigationRef = React.createRef<NavigationContainerRef>();

export function navigate(name: string, params?: any | undefined) {
  navigationRef.current?.navigate(name, params);
}
export function popNavigate() {
  navigationRef.current?.canGoBack() && navigationRef.current.goBack();
}
// setTimeout(() => navigate('Chat'), 10);
