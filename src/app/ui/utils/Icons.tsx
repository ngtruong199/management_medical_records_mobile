import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconF5 from 'react-native-vector-icons/FontAwesome5';
import IconFeather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Zocial from 'react-native-vector-icons/Zocial';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {StyleProp, ViewProps} from 'react-native';
import {IconProps} from 'react-native-vector-icons/Icon';
import styled from 'styled-components/native';
import _user_color, {_color} from '../themes/Colors';
export interface PropsIcons {
  name: string;
  size?: number;
  color?: string;
  onPress?: () => void;
  style?: StyleProp<ViewProps>;
  styleContainer?: ViewProps;
}
const Icons = [
  Icon,
  IconF5,
  MaterialIcons,
  MaterialCommunityIcons,
  IconFeather,
  Fontisto,
  Foundation,
  Octicons,
  Zocial,
  SimpleLineIcons,
];
const getIconWithName = ({
  name,
  size,
  style,
  color,
  isRound,
  styleContainer,
}: PropsIcons) => {
  const IconRS: IconProps = Icons.find(item => item?.hasIcon(name));
  return IconRS ? (
    isRound ? (
      <ContainerView style={styleContainer}>
        <IconRS name={name} size={size} style={[style]} color={_color.white} />
      </ContainerView>
    ) : (
      <IconRS
        name={name}
        size={size}
        style={[style, {marginHorizontal: 5}]}
        color={color}
      />
    )
  ) : (
    <Icon name={name} size={size} style={{borderRadius: 20}} color={color} />
  );
};
const ContainerView = styled.View`
  padding: 10px;
  border-radius: 30px;
  background-color: ${_user_color.primaryColor};
`;
export default getIconWithName;
