export interface Icons {
  name: string;
  color: string;
}
export const iconsBottomOption = {
  delete: {
    name: 'trash',
    color: '#A9403B',
  },
  edit: {
    name: 'edit',
    color: '#F7C36E',
  },
  create: {
    name: 'folder-plus',
    color: '#1F5CA5',
  },
  camera: {
    name: 'camera-retro',
    color: '#A6DAE6',
  },
  choosePicture: {
    name: 'images',
    color: '#EA5722',
  },
};
