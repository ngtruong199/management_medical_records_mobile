import * as React from "react"
import Svg, { Path } from "react-native-svg"

function _IconDashBoard({size}) {
  return (
    <Svg color={'white'} height="30px" viewBox="0 0 24 24" width="30px" {...props}>
      <Path fill="none" d="M0 0H24V24H0z" />
      <Path d="M5 11h4c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2zM5 21h4c1.1 0 2-.9 2-2v-4c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2zM13 5v4c0 1.1.9 2 2 2h4c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-4c-1.1 0-2 .9-2 2zM15 21h4c1.1 0 2-.9 2-2v-4c0-1.1-.9-2-2-2h-4c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2z" />
    </Svg>
  )
}

export default _IconDashBoard
