import React from 'react'
import { SvgCss } from 'react-native-svg'

const xml = `
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M9 9C11.0717 9 12.75 7.32171 12.75 5.25C12.75 3.17829 11.0717 1.5 9 1.5C6.92829 1.5 5.25 3.17829 5.25 5.25C5.25 7.32171 6.92829 9 9 9ZM2.25 15.75C2.25 16.1642 2.58579 16.5 3 16.5H15C15.4142 16.5 15.75 16.1642 15.75 15.75V14.25C15.75 11.9515 12.1268 10.5 9 10.5C5.87324 10.5 2.25 11.9515 2.25 14.25V15.75Z" fill="#C2D1D9"/>
    <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="2" y="1" width="14" height="16">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M9 9C11.0717 9 12.75 7.32171 12.75 5.25C12.75 3.17829 11.0717 1.5 9 1.5C6.92829 1.5 5.25 3.17829 5.25 5.25C5.25 7.32171 6.92829 9 9 9ZM2.25 15.75C2.25 16.1642 2.58579 16.5 3 16.5H15C15.4142 16.5 15.75 16.1642 15.75 15.75V14.25C15.75 11.9515 12.1268 10.5 9 10.5C5.87324 10.5 2.25 11.9515 2.25 14.25V15.75Z" fill="white"/>
    </mask>
    <g mask="url(#mask0)">
      <rect width="18" height="18" fill="white"/>
    </g>
  </svg>
`
export default ({ width = 18, height = 18 }) => {
  return <SvgCss xml={xml} width={width} height={height} />
}
