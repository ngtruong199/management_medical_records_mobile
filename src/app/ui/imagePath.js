const _imagePath = {
  CardO1: require('../../../assets/medical_card/card01.jpg'),
  Card02: require('../../../assets/medical_card/card02.jpeg'),
  Card03: require('../../../assets/medical_card/card03.jpeg'),
  Card04: require('../../../assets/medical_card/card04.jpg'),
  Card05: require('../../../assets/medical_card/card05.jpeg'),
  Card06: require('../../../assets/medical_card/card06.jpg'),
  Card07: require('../../../assets/medical_card/card07.jpeg'),
  Card08: require('../../../assets/medical_card/card08.jpg'),
  Card09: require('../../../assets/medical_card/card09.jpeg'),
  Card10: require('../../../assets/medical_card/card10.jpg'),
  Card11: require('../../../assets/medical_card/card11.jpeg'),
  Card12: require('../../../assets/medical_card/card12.jpg'),
};
export const listCard_UI = Object.values(_imagePath).map(item => item);
export const getCardNumber = number => {
  if (isNaN(number) && number <= 0) {
    return;
  }

  return listCard_UI[number - 1];
};
export const AvatarProfileDefault = require('../../../assets/avatar.jpeg');
export const DefaultImgChatGroup = require('../../../assets/team_chat.jpeg');
export default _imagePath;
export const isImageSystem = (imgFile: Object) =>
  imgFile.uri.includes('file://');
