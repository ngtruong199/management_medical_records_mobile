import React, {PureComponent} from 'react';
import {
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {isIOS, safeBottomPadding} from '../../../utils/platform';
import {_color} from '../themes/Colors';

const withKeyBoardAndroid = (WrappedComponent, passProps = {}) => {
  class KeyboardAwareView extends PureComponent {
    render() {
      return (
        <KeyboardAvoidingView
          contentContainerStyle={[{flex: 1}, passProps.style]}
          behavior="height"
          keyboardVerticalOffset={10}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ScrollView>
              <WrappedComponent {...passProps} {...this.props} />
            </ScrollView>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
    }
  }

  return KeyboardAwareView;
};
const withKeyboardIOS = (WrapperComponent, passProps = {}) => {
  class KeyboardAwareView extends PureComponent {
    state = {
      keyboardShowed: false,
    };

    componentDidMount() {
      this.keyboardDidShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardWillShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardWillHide,
      );
    }
    componentWillUnmount() {
      Keyboard.removeListener('keyboardWillShow', this._keyboardWillShow);
      Keyboard.removeListener('keyboardWillHide', this._keyboardWillHide);
    }
    _keyboardWillShow = () => {
      this.setState({keyboardShowed: true});
    };
    _keyboardWillHide = () => {
      this.setState({keyboardShowed: false});
    };

    render() {
      const {scrollEnabled = false} = passProps;
      const {keyboardShowed} = this.state;
      return (
        <KeyboardAwareScrollView
          contentContainerStyle={[{flex: 1}, passProps.style]}
          keyboardShouldPersistTaps={
            passProps.keyboardShouldPersistTaps || 'never'
          }
          showsVerticalScrollIndicator={false}
          scrollEnabled={keyboardShowed ? true : scrollEnabled}>
          <WrapperComponent {...passProps} {...this.props} />
        </KeyboardAwareScrollView>
      );
    }
  }
  return KeyboardAwareView;
};
const withKeyboardAwareView = (WrapperComponent, passprops = passProps) => {
  const withKeyboard = isIOS ? withKeyboardIOS : withKeyBoardAndroid;
  return withKeyboard(WrapperComponent, passprops);
};
export default withKeyboardAwareView;
export const passProps = {
  scrollEnabled: true,
  style: {backgroundColor: _color.background_Container},
};
