import React from 'react';
import {View, Text, ViewStyle, StyleProp} from 'react-native';
import styled from 'styled-components/native';
import {windowWidth} from '../../../utils/platform';
import {popNavigate} from '../../navigation/rootNavigation';
import BackIcon from '../icons/BackIcon';
import {_numberStyled} from '../themes/dimens';
import {TextTitle, TouchWhite, TouchWhiteNoShadow} from '../themes/styles';
interface Props {
  showBack: boolean;
  children: any;
  onBack: () => void;
  text: string;
  styleBack: StyleProp<ViewStyle>;
  styleHeader?: StyleProp<ViewStyle>;
  isCenter: boolean;
}
const Header = ({
  showBack = true,
  children,
  onBack,
  text,
  styleBack,
  styleHeader = {},
  isCenter = false,
}: Props) => {
  return (
    <HeaderContainer style={styleHeader}>
      {showBack && (
        <TouchWhiteNoShadow
          onPress={onBack || popNavigate}
          style={[{marginRight: 10}, styleBack]}>
          <BackIcon width={30} height={30} />
        </TouchWhiteNoShadow>
      )}
      {text ? (
        <TitleText isCenter={isCenter} style={!showBack && {marginLeft: 10}}>
          {text}
        </TitleText>
      ) : null}
      {children}
    </HeaderContainer>
  );
};
const HeaderContainer = styled.SafeAreaView`
  width: 100%;
  flex-direction: row;
  margin-bottom: 10px;
  align-items: center;
`;
const TitleText = styled(TextTitle)`
  color: #3f3f45;
  font-weight: bold;
`;

export default Header;
