import React from 'react';
import {StyleProp, Text, TouchableOpacity, ViewStyle} from 'react-native';
import {
  RoundTouch,
  RoundTouchDisable,
  TextLarge,
  Touch,
} from '../themes/styles';
import {handleTextSize, TextSize} from './hanleUI';
interface Props {
  onPress?: () => void;
  disable?: true | false;
  text: string;
  textSize?: TextSize;
  style?: StyleProp<ViewStyle>;
  disableRound?: boolean;
}
const TextButton: React.FC<Props> = ({
  onPress,
  text,
  style,
  disableRound,
  disable,
  textSize,
}) => {
  const Button = disableRound
    ? Touch
    : disable
    ? RoundTouchDisable
    : RoundTouch;
  const Text = handleTextSize(textSize);
  return (
    <Button
      disabled={disable}
      style={style}
      activeOpacity={0.7}
      onPress={onPress}>
      <Text style={{marginVertical: 6, textTransform: 'uppercase'}}>
        {text}
      </Text>
    </Button>
  );
};
export default TextButton;
