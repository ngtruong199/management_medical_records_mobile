import React, {useEffect} from 'react';
import {Image, Modal} from 'react-native';
import styled from 'styled-components';
import {windowHeight, windowWidth} from '../../../utils/platform';
import Header from './Header';

const ZoomImage = ({url = '', onClose}) => {
  return (
    <Modal
      animationType="fade"
      visible={!!url}
      transparent={true}
      onRequestClose={() => onClose && onClose()}>
      <Container>
        {url && <ImageView source={url} />}
        <Header
          onBack={onClose}
          styleHeader={{
            position: 'absolute',
            top: 0,
            left: 10,
            width: 70,
            padding: 0,
          }}
        />
      </Container>
    </Modal>
  );
};
const ImageView = styled(Image)`
  position: absolute;
  width: ${windowWidth}px;
  height: ${windowHeight}px;
`;
const Container = styled.View`
  flex: 1;
  background-color: #fff;
  justify-content: center;
  align-items: center;
  padding-horizontal: 15px;
`;
export default ZoomImage;
