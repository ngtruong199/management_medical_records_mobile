import React, {useState, useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
import {windowWidth} from '../../config/dimens';
import getString from '../../locate';
import {dismissOverlay} from '../../navigator/api';
import FadeView from '../layout/FadeView';
import {setIsExistPopup, getIsConnected} from '../../api/index';

export default ({
  title,
  content,
  action,
  closeAction,
  componentId,
  isFirstPopup,
}) => {
  const [isExist, setIsExist] = useState(true);
  useEffect(() => {
    if (getIsConnected()) {
      isFirstPopup === undefined ? setIsExist(true) : setIsExist(isFirstPopup);
    } else {
      isFirstPopup === undefined ? setIsExist(false) : setIsExist(isFirstPopup);
    }
  }, [isFirstPopup]);

  if (!isExist) {
    dismissOverlay(componentId);
    return null;
  }
  const closePopup = () => {
    if (action) {
      action();
    }
    setIsExistPopup(true);
    dismissOverlay(componentId);
  };
  const onClose = () => {
    if (closeAction) {
      closeAction();
    }
    setIsExistPopup(true);
    dismissOverlay(componentId);
  };
  const renderTitle = () => {
    if (!title || title.length === 0) {
      return null;
    }
    return <Title>{title}</Title>;
  };
  return (
    <Container>
      <ContentContainer>
        {renderTitle()}
        <Content>{content}</Content>
        <WrapBtn>
          <Button
            onPress={closePopup}
            style={{width: closeAction ? '50%' : '100%'}}>
            <ContentText>{getString('general', 'yes')}</ContentText>
          </Button>
          {closeAction && (
            <Button style={{width: '50%'}} onPress={onClose}>
              <ContentText>{getString('rating', 'later')}</ContentText>
            </Button>
          )}
        </WrapBtn>
      </ContentContainer>
    </Container>
  );
};

const Container = styled(FadeView)`
  flex: 1;
  background-color: #00000055;
  align-items: center;
  justify-content: center;
`;

const ContentContainer = styled.View`
  width: ${windowWidth - 50}px;
  align-self: center;
  padding-top: 30px;
  border-radius: 20px;
  background-color: #fff;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 18px;
  padding-bottom: 20px;
`;

const Content = styled.Text`
  font-size: 14px;
  padding-bottom: 30px;
  align-self: center;
  padding-horizontal: 20px;
`;

const Button = styled(TouchableOpacity)`
  border-top-width: 0.5px;
  border-color: #00000055;
  padding: 20px;
  width: 100%;
  height: 44px;
  justify-content: center;
`;

const ContentText = styled.Text`
  font-size: 16px;
  align-self: center;
`;

const WrapBtn = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
