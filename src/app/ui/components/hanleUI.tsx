import {TextLarge, TextMedium, TextSmall} from '../themes/styles';

export type TextSize = 'SMALL' | 'LARGE' | 'MEDIUM' | undefined;
export const handleTextSize = (size: TextSize) => {
  return size === 'LARGE'
    ? TextLarge
    : size === 'SMALL'
    ? TextSmall
    : TextMedium;
};
