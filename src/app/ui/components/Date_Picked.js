import React, {useState, useEffect} from 'react';
import {View, Button, Platform} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {windowWidth} from '../../../utils/platform';
import styled from 'styled-components/native';
import {
  ErrorText,
  RowView,
  TextLarge,
  TextMedium,
  TitleInput,
  Touch,
} from '../themes/styles';
import getIconWithName from '../utils/Icons';
import _user_color from '../themes/Colors';
import {locate_Date} from '../../../utils/other';
import BottomPopup from './BottomPopup';
import {
  displayStringDate,
  isBeforeDay_Now,
  time_now,
  toDate,
  toTimeStamp,
} from '../../../utils/date';

export const _date = 'date';
export const _time = 'time';

const DatePicked = ({
  isLastTimeNow = false,
  mode,
  date,
  setDate,
  onPlus,
  onMinus,
  title,
  style,
}) => {
  const onChange = selectedDate => {
    setError(isLastTimeNow && !isBeforeDay_Now(selectedDate));
    setDate && setDate(toTimeStamp(selectedDate));
  };
  const [error, setError] = useState(null);
  const [showPicked, setShow] = useState(false);
  const PickerContainer = title ? ContainerTitle : Container;
  return (
    <PickerContainer style={style} title={title}>
      <View>
        <TouchContainerPicker
          onPress={() => {
            setShow(!showPicked);
          }}>
          <TextPicker>
            {date ? displayStringDate(date, mode) : 'DD/MM/YYYY'}
          </TextPicker>
        </TouchContainerPicker>
        {error && <ErrorText>Ngày nhập phải bé hơn ngày hiện tại</ErrorText>}
      </View>

      <BottomPopup
        style={{alignItems: 'center'}}
        visible={showPicked}
        onToggle={() => {
          !date && setDate(time_now);
          setShow(false);
        }}>
        <DatePicker
          is24hourSource={'locale'}
          mode={mode || 'date'}
          locale={'vi-VN'}
          date={toDate(date)}
          onDateChange={onChange}
        />
      </BottomPopup>
      {onPlus && (
        <Touchable onPress={onPlus}>
          {getIconWithName({
            name: 'plus',
            size: 22,
            color: _user_color.primaryColor,
          })}
        </Touchable>
      )}
      {onMinus && (
        <Touchable onPress={onMinus}>
          {getIconWithName({
            name: 'minus',
            size: 22,
            color: _user_color.primaryColor,
          })}
        </Touchable>
      )}
    </PickerContainer>
  );
};

export const ListDatePicked = ({
  listDate = [],
  onSetDate,
  title,
  mode = 'date',
  withScroll = false,
}) => {
  const renderItem = ({item, index}) => {
    return (
      <DatePicked
        mode={mode}
        key={`list_item${(index || 0).toString()}`}
        date={item}
        setDate={date => {
          !index && !listDate.length
            ? listDate.push(date)
            : (listDate[index] = date);
          onSetDate([...listDate]);
        }}
        onPlus={() => {
          !index
            ? listDate.push(time_now)
            : listDate.splice(index + 1, 0, time_now);
          console.log(index);
          onSetDate([...listDate]);
        }}
        onMinus={() => {
          listDate.length && listDate.splice(index, 1);
          onSetDate([...listDate]);
        }}
      />
    );
  };

  return withScroll ? (
    <ScrollContainer
      ListHeaderComponent={
        title ? () => <TitleInput>{title.toUpperCase()}</TitleInput> : null
      }
      data={listDate}
      renderItem={renderItem}
      ListEmptyComponent={renderItem}
      extraData={listDate}
      keyExtractor={(item, index) => `list_item${index.toString()}`}
    />
  ) : (
    <ViewContainer>
      {title && <TitleInput>{title.toUpperCase()}</TitleInput>}
      {listDate.length
        ? listDate.map((item, index) => renderItem({item, index}))
        : renderItem({item: null, index: null})}
    </ViewContainer>
  );
};
const ContainerTitle = ({children, title, style}) => (
  <ViewContainer style={style}>
    <TitleInput>{title}</TitleInput>
    <Container>{children}</Container>
  </ViewContainer>
);
const Container = styled(RowView)`
  align-items: center;
`;

const ViewContainer = styled.SafeAreaView``;
const ScrollContainer = styled.FlatList``;
const Touchable = styled(Touch)`
  border-radius: 5px;
  background-color: #e3e3e3;
  padding: 6px;
  margin: 0;
  margin-left: 5px;
  margin-right: 10px;
  width: 40px;
`;
const TouchContainerPicker = styled.TouchableOpacity`
  background-color: #e3e3e3;
  border-radius: 5px;
  padding: 8px;
  width: 150px;
`;

const TextPicker = styled(TextLarge)`
  font-weight: 500;
  color: ${_user_color.primaryColor};
  padding-horizontal: 10px;
`;
export default DatePicked;
