import React, {useEffect, useRef, useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import {
  safePaddingTop,
  windowHeight,
  windowWidth,
} from '../../../utils/platform';
import ListData, {DropListCustom} from '../../animation/ListData';
import {_color} from '../themes/Colors';
import {TouchWhite} from '../themes/styles';
import getIconWithName from '../utils/Icons';

const SearchInput = ({style, rightItem, children}) => {
  const [SearchText, setSearchText] = useState('');
  const [isFocus, setFocus] = useState(false);
  const inputRef = useRef();
  useEffect(() => {
    !isFocus && inputRef.current.blur();
  }, [isFocus]);
  return (
    <Container isFocus={isFocus}>
      <DropListCustom _height={isFocus ? windowHeight : 60} open={true}>
        <ToolContainer>
          <AreaInput>
            {getIconWithName({
              name: 'search',
              size: 30,
              color: '#777',
              style: {position: 'absolute', top: 7.5},
            })}
            <SearchInputText
              ref={inputRef}
              onChangeText={setSearchText}
              value={SearchText}
              onFocus={() => setFocus(true)}
              placeholder={'Tìm kiếm nhóm chat'}
            />
          </AreaInput>

          {isFocus ? (
            <TouchWhite
              onPress={() => setFocus(false)}
              style={{
                marginVertical: 5,
                alignSelf: 'center',
                padding: 0,
                marginLeft: 'auto',
              }}>
              {getIconWithName({
                name: 'times-circle',
                size: 30,
                color: '#777',
              })}
            </TouchWhite>
          ) : (
            rightItem()
          )}
        </ToolContainer>
        {isFocus && children}
      </DropListCustom>
    </Container>
  );
};
const AreaInput = styled.View`
  flex: 1;
  margin-right: 10px;
  border-radius: 20px;
  border-width: 1px;
  border-color: #eee;
`;
const ToolContainer = styled.View`
  flex-direction: row;
`;
const Container = styled.View`
  background-color: ${_color.background_Container};
  height: ${({isFocus}) => (isFocus ? `${windowHeight}px` : 'auto')};
  width: ${windowWidth}px;
  padding-top: ${safePaddingTop}px;
  margin-bottom: 10px;
  padding-bottom: ${({isFocus}) => (isFocus ? '82px' : '0')};
`;
const SearchInputText = styled.TextInput`
  padding: 15px;
  padding-left: 40px;
  font-size: 16px;
  color: #111;
`;
export default React.memo(SearchInput);
