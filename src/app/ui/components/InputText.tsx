import React, {useState} from 'react';
import {_numberCss, _numberStyled} from '../themes/dimens';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {Fumi} from 'react-native-textinput-effects';
import {StyleProp, ViewStyle} from 'react-native';
import styled from 'styled-components/native';
import {color} from 'react-native-reanimated';
import {ShadowString} from '../themes/styles';
import _user_color from '../themes/Colors';

interface Props {
  onChangeText?: (value: string, field?: string) => void;
  value?: string;
  label?: string;
  iconName: string;
  material?: boolean;
  placeholder?: string;
  fieldName?: string;
  isPass?: true | false;
  style?: StyleProp<ViewStyle>;
  disable?: true | false;
  inputType?: 'number-pad' | 'email-address' | 'url';
}
const disableColor = '#3a3f4d';
const primaryColor = _user_color.primaryColor;
const InputText: React.FC<Props> = props => {
  const [isPlaceHolder, setPlaceHolder] = useState<boolean>(false);
  const {isPass, value, label, iconName, material, style, disable} = props;
  function _togglePlaceHolder() {
    setPlaceHolder(!isPlaceHolder);
  }
  function changText(text: string) {
    const {fieldName, onChangeText} = props;
    onChangeText &&
      (fieldName ? onChangeText(text, fieldName) : onChangeText(text));
  }

  const color = {color: disable ? '#7a7a7a' : primaryColor};
  return (
    <SafeView>
      <InputFumi
        keyboardType={props.inputType || 'default'}
        secureTextEntry={isPass}
        value={value || ''}
        onChangeText={changText}
        onFocus={_togglePlaceHolder}
        onBlur={_togglePlaceHolder}
        placeholder={isPlaceHolder ? props.placeholder : ''}
        label={label || 'unknow'}
        iconClass={material ? MaterialIcon : FontAwesomeIcon}
        iconName={iconName}
        iconColor={disable ? disableColor : primaryColor}
        iconSize={20}
        iconWidth={40}
        inputStyle={color}
        labelStyle={color}
        editable={!disable}
      />
    </SafeView>
  );
};
const SafeView = styled.SafeAreaView`
  margin-vertical: 10px;
  ${ShadowString}
  border-radius: ${_numberStyled['bd-radius']};
`;
const InputFumi = styled(Fumi)`
  border-radius: ${_numberStyled['bd-radius']};
`;
export default InputText;
