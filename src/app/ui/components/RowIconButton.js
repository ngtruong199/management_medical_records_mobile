import React from 'react';
import {View, Text} from 'react-native';
import styled from 'styled-components/native';
import ArrowRight from '../icons/ArrowRight';
import {_numberStyled} from '../themes/dimens';
import {TextMedium, TextSmall, Touch} from '../themes/styles';
import getIconWithName from '../utils/Icons';
import CusButton from './TextButton';

const RowIconButton = ({
  icon = {name: '', size: 30, isRound: true},
  onPress,
  disable,
  text,
  description,
  rightText,
  isNotification,
}) => {
  return (
    <RowButton onPress={onPress}>
      {getIconWithName({
        ...icon,
        style: {
          color: isNotification ? '#f00' : '#002851',
        },
        styleContainer: {backgroundColor: '#F1F3FD'},
      })}
      <View style={{alignItems: 'flex-start'}}>
        <TextButton>{text}</TextButton>
        {description ? <Description>{description}</Description> : null}
      </View>

      {!description ? (
        getIconWithName({
          name: 'chevron-right',
          size: 25,
          color: '#111',
          style: {marginLeft: 'auto'},
        })
      ) : (
        <TimeIcon style={{marginBottom: 20}}>{rightText}</TimeIcon>
      )}
    </RowButton>
  );
};
const Description = styled.Text`
  color: #555;
  font-size: 13px;
  margin-left: 20px;
`;
const RowButton = styled(Touch)`
  flex-direction: row;
  align-items: center;
  margin: 0;
  margin-right: 20px;
`;
const TextButton = styled(TextMedium)`
  color: #070506;
  margin-left: 20px;
`;
const TimeIcon = styled(TextSmall)`
  position: absolute;
  top: -5px;
  right: -30px;
  color: #19b5d4;
`;
export default RowIconButton;
