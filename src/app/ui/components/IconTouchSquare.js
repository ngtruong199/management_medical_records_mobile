import React from 'react'
import { View, Text } from 'react-native'
import { RoundTouch, TouchWhite } from '../themes/styles'
import getIconWithName from '../utils/Icons'

const IconTouchSquare = () => {
  return <TouchWhite>{getIconWithName({ name: 'user', size: 30 })}</TouchWhite>
}

export default IconTouchSquare
