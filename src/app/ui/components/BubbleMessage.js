import React, { PureComponent } from 'react'
import styled from 'styled-components'

export default class BubbleMessage extends PureComponent {
  render () {
    const { timeText, message, isRight = false } = this.props
    return (
      <BubbleMessageContainer isRight={isRight}>
        <TextBubble isRight={isRight}>{message}</TextBubble>
        <ViewTimeMessage>{timeText}</ViewTimeMessage>
      </BubbleMessageContainer>
    )
  }
}

const BubbleMessageContainer = styled.View`
  max-width: 70%;
  height: auto;
  padding: 10px;
  background-color: ${props => (props.isRight ? '#ffffff' : '#4294d8')};
  margin-right: 10px;
  margin-left: 10px;
  border-radius: 20px;
  border-bottom-right-radius: ${props => (props.isRight ? 2 : 20)}px;
  border-bottom-left-radius: ${props => (props.isRight ? 20 : 2)}px;
`

const TextBubble = styled.Text`
  color: ${props => (props.isRight ? '#4294d8' : '#ffffff')};
`

const ViewTimeMessage = styled.Text`
  font-weight: bold;
  font-size: 11px;
  align-self: flex-end;
  color: #94caf8;
  margin-top: 10px;
`
