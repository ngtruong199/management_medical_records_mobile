import React from 'react';
import {TextLarge, Touch, TextSmall, TextMedium} from '../themes/styles';
import {TextSize} from './hanleUI';
interface Props {
  onPress?: () => void;
  disable?: true | false;
  sizeText?: TextSize;
}
const CusButton: React.FC<Props> = props => {
  const {sizeText, disable, onPress} = props;
  const Text =
    sizeText === 'LARGE'
      ? TextLarge
      : sizeText === 'SMALL'
      ? TextSmall
      : TextMedium;
  return <Touch onPress={props.onPress}>{props.children}</Touch>;
};
export default CusButton;
