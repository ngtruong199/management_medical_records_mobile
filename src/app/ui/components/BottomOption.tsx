import React from 'react';
import {ViewProps} from 'react-native';
import {useSelector} from 'react-redux';
import styled from 'styled-components/native';
import {hideBottomOption} from '../../action/bottom-option';
import {StateBottomOption} from '../../reducers/bottom-option';
import getIconWithName from '../utils/Icons';
import BottomPopup from './BottomPopup';

export const BottomOption = () => {
  const bottomOption: StateBottomOption = useSelector(
    state => state.bottom_option,
  );

  const {listData, isShow} = bottomOption;
  return isShow ? (
    <BottomPopup
      style={{paddingBottom: 30}}
      visible={isShow}
      onToggle={hideBottomOption}>
      {listData.map((item, index) => (
        <Button
          key={`Option${index}`}
          onPress={() => {
            hideBottomOption();
            item.onPress();
          }}>
          {item.icon && getIconWithName({size: 30, ...item.icon})}
          <ButtonText>{item.title}</ButtonText>
        </Button>
      ))}
    </BottomPopup>
  ) : null;
};

const Button = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  padding-vertical: 20px;
`;
const ButtonText = styled.Text`
  margin-left: 10px;
  font-size: 19px;
  font-weight: 500;
  color: #111;
`;
