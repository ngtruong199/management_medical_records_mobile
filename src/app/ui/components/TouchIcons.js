import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import styled from 'styled-components/native';
import PlusIcon from '../icons/PlusIcon';
import {TouchWhite} from '../themes/styles';
import storage from '@react-native-firebase/storage';
import _ from 'lodash';

const TouchIcons = ({
  onPress,
  onLongPress,
  style,
  children,
  propsIcon,
  item,
}) => {
  const [uri, setSource] = useState();
  const Icon = item ? (item.uri ? ImageView : item) : PlusIcon;
  useEffect(() => {
    if (!!item && typeof item?.uri === 'string') {
      item.uri.includes('file://')
        ? setSource(item.uri)
        : storage()
            .ref(item.uri)
            .getDownloadURL()
            .then(_uri => {
              setSource(_uri);
            });
    }
  }, [item]);
  const inside_propsIcon = item?.uri
    ? propsIconsImage
    : {width: 70, height: 70};
  return (
    <TouchWhite onLongPress={onLongPress} style={style} onPress={onPress}>
      <Icon source={{uri: uri}} {...inside_propsIcon} />
      {children}
    </TouchWhite>
  );
};
const propsIconsImage = {width: 90, height: 90};
const ImageView = styled.Image`
  width: 105px;
  height: 105px;
  border-radius: 15px;
`;
export default TouchIcons;
