import React from 'react'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

const InputAddress = () => {
  return (
    <GooglePlacesAutocomplete
      onFail={err => console.log(err)}
      placeholder="Search"
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        console.log(data, details)
      }}
      disableScroll={true}
      numberOfLines={3}
      fetchDetails={true}
      keyboardShouldPersistTaps="handled"
      enablePoweredByContainer={false}
      enableHighAccuracyLocation={true}
      query={{
        key: 'AIzaSyC84YqPWm9HL0ggAJVNllBaQ_JbR0hQTBc',
        language: 'vi',
        components: 'country:vn'
      }}
    />
  )
}
export default InputAddress
