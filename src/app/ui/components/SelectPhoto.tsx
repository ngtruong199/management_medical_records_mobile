import React from 'react';
import {Modal} from 'react-native';
import styled from 'styled-components/native';
import {PermissionsAndroid} from 'react-native';
// import {TouchableOpacity} from './buttons/Touchable';
import {
  Callback,
  ImagePickerResponse,
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker';
import {CenterAreaContainer} from '../themes/styles';
import {getString} from '../../locate';
import BottomPopup from './BottomPopup';

export const requestCameraPermission = async () => {
  try {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
      title: 'Cool Photo App Camera Permission',
      message:
        'Cool Photo App needs access to your camera ' +
        'so you can take awesome pictures.',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    });
  } catch (err) {
    console.warn(err);
  }
};

// import {ShadowBox} from './box';
// import {showOverlay} from '../../navigator/api';
// import {timeout} from '../../../utils/threading';
interface State {
  hidePopup: boolean;
  openLibrary: boolean;
}
interface Props {
  visible: boolean;
  onDismiss: () => void;
  onSelectPhoto: ({uri}) => void;
}
export default class SelectPhoto extends React.PureComponent<Props, State> {
  state: State = {
    hidePopup: false,
    openLibrary: false,
  };

  componentDidUpdate(prevProps) {
    if (this.state.openLibrary && !this.props.visible) {
      this.state.openLibrary = false;
      this.doOpenLibrary();
    }
  }

  choosePhoto = () => {
    setTimeout(() => {
      launchImageLibrary(
        {mediaType: 'photo', maxWidth: 500, maxHeight: 500, quality: 0.5},
        (respone: ImagePickerResponse) => {
          !respone.didCancel && this.props.onSelectPhoto({uri: respone.uri});
          this.props.onDismiss();
        },
      );
    }, 300);
  };

  takePicture = () => {
    launchCamera(
      {
        cameraType: 'back',
        mediaType: 'photo',
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.5,
      },
      (respone: ImagePickerResponse) => {
        !respone.didCancel && this.props.onSelectPhoto({uri: respone.uri});
        this.props.onDismiss();
      },
    );
  };

  render() {
    const {visible, onDismiss} = this.props;
    return (
      <BottomPopup visible={visible || false} onToggle={onDismiss}>
        <Box>
          <Title>{getString('camera', 'title')}</Title>
          <Button onPress={this.takePicture}>
            <ButtonText>{getString('camera', 'camera')}</ButtonText>
          </Button>
          <Button onPress={this.choosePhoto}>
            <ButtonText>{getString('camera', 'select_photo')}</ButtonText>
          </Button>
          <Button onPress={onDismiss}>
            <ButtonText>{getString('camera', 'cancel')}</ButtonText>
          </Button>
        </Box>
      </BottomPopup>
    );
  }
}

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const Box = styled(CenterAreaContainer)`
  padding-horizontal: 50px;
  padding-vertical: 10px;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  font-size: 24px;
  font-weight: 700;
  color: #4294d8;
  margin-bottom: 30px;
`;

const Button = styled.TouchableOpacity`
  margin-vertical: 5px;
  margin-horizontal: 50px;
  border-radius: 30px;
  height: 50px;
  padding-vertical: 10px;
  width: 260px;
  background-color: #4294d8;
  align-items: center;
  justify-content: center;
`;

const ButtonText = styled.Text`
  font-size: 16px;
  color: #fff;
`;
