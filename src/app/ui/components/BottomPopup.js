import React, {PureComponent, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import {Modal, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';

import TextButton from './RoundTextButton';

const BottomPopup = ({children, visible, onToggle, style = {}}) => {
  return (
    <Modal animationType="fade" visible={visible} transparent={true}>
      <Container activeOpacity={1} onPress={() => onToggle(false)}>
        <ContainerItems style={style}>{children}</ContainerItems>
      </Container>
    </Modal>
  );
};
const ContainerItems = styled.View`
  padding-horizontal: 10px;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  background-color: #fff;
  padding-bottom: 20px;
`;
const Container = styled(TouchableOpacity)`
  flex: 1;
  background-color: #00000081;
  justify-content: flex-end;
`;
export default BottomPopup;
