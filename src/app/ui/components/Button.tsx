import React from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import {RoundTouch} from '../themes/styles';
interface Props {
  onPress?: () => void;
  disable?: true | false;
  style?: StyleProp<ViewStyle>;
}
const CusButton: React.FC<Props> = props => {
  return (
    <RoundTouch activeOpacity={0.7} {...props}>
      {props.children}
    </RoundTouch>
  );
};
export default CusButton;
