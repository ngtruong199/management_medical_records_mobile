import React, {Component, useEffect, useRef, useState} from 'react';
import {View, Text, TextInputProps} from 'react-native';
import {interpolate} from 'react-native-reanimated';
import {mix} from 'react-native-redash/lib/module/v1';
import styled from 'styled-components/native';
import {isIOS, windowWidth} from '../../../utils/platform';
import {constainString} from '../../../utils/string';
import DropdownIcon from '../../animation/DropdownIcon';
import ListData from '../../animation/ListData';
import _user_color from '../themes/Colors';
import {_numberStyled} from '../themes/dimens';
import {
  CheckError,
  ErrorText,
  ShadowString,
  TextLargest,
  TextMedium,
} from '../themes/styles';
import _ from 'lodash';

const unfocus_color = '#ccc';

const InputField = ({
  label,
  value,
  onChangeText,
  onSubmitText,
  onFocus,
  onBlur,
  placeholder,
  number = 1,
  isNumber = false,
  isEdit = true,
  numberOfLines,
  minHeight,
  isRow = false,
  dropdownData = null,
  onSubmitDropdown,
  width,
  checkError = '',
}) => {
  const [focus, setFocus] = useState(false);
  const [text, setText] = useState('');
  const [opened, setOpened] = useState(false);
  const [listDropdown, setListDropdown] = useState([]);
  const inputRef = useRef();
  useEffect(() => {
    if (dropdownData && focus) {
      let _list = dropdownData;

      isEdit &&
        text &&
        (_list = dropdownData.filter(item => constainString(item, text)));
      setOpened(true);
      setListDropdown(_list);
    }
  }, [text, focus]);
  useEffect(() => {
    setText(value);
  }, [value]);
  const pressItemDropdown = _value => {
    setFocus(false);
    onSubmitDropdown
      ? onSubmitDropdown(_value)
      : onSubmitText && onSubmitText(_value);
    setText(_value);
    setOpened(false);
  };
  return (
    <ContainerDropdown style={[dropdownData && {zIndex: 1}, {width}]}>
      <Container style={isRow && {flexDirection: 'row'}}>
        {label && (
          <TitleInput style={{alignSelf: isRow ? 'center' : 'flex-start'}}>
            {label.toUpperCase()}
          </TitleInput>
        )}
        <Input
          numberOfLines={numberOfLines || 1}
          multiline={!!numberOfLines || false}
          placeholder={placeholder}
          editable={isEdit}
          ref={inputRef}
          focus={focus}
          value={text}
          keyboardType={isNumber ? 'numeric' : 'default'}
          height={minHeight}
          style={number && {marginEnd: 10}}
          width={isRow ? width || 100 : width || (windowWidth * 0.9) / number}
          onFocus={() => setFocus(true)}
          onBlur={() => {
            onSubmitText && onSubmitText(text);
            setFocus(false);
          }}
          onChangeText={_text => {
            onChangeText && onChangeText(_text);
            setText(_text);
          }}
        />
        <CheckError value={value} checkError={checkError} />

        {dropdownData && (
          <TouchIcon
            onPress={() => {
              setOpened(!opened);
              setFocus(!focus);
            }}>
            <DropdownIcon opened={opened} />
          </TouchIcon>
        )}
      </Container>
      {!_.isEmpty(listDropdown) && (
        <ListData
          open={opened}
          listData={listDropdown}
          isRow={isRow}
          number={number}
          onClick={pressItemDropdown}
        />
      )}
    </ContainerDropdown>
  );
};

const TouchIcon = styled.TouchableOpacity`
  width: 45px;
  height: 47px;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  position: absolute;
  bottom: 0;
  right: 2.5px;
`;
const Input = styled.TextInput`
  width: ${({width}) => width}px;
  border-color: ${({focus}) =>
    focus ? _user_color.primaryColor : unfocus_color};
  padding: 12px;
  font-size: ${_numberStyled['f-largest']};
  color: #333;
  font-weight: 600;
  border-width: 1.5px;
  border-radius: 7px;
`;
const TitleInput = styled(TextMedium)`
  color: #999;
  text-align: left;
  margin-bottom: 5px;
  margin-left: 5px;
  z-index: -1;
`;
const Container = styled.View``;

const ContainerDropdown = styled.View`
  margin-vertical: 10px;
`;

export default InputField;
