import React from 'react';
import {ScrollViewProps, ViewProps, ViewStyle} from 'react-native';
import styled from 'styled-components/native';
import {safeBottomPadding} from '../../../utils/platform';
import {_numberStyled} from '../themes/dimens';
import {containerStyle, ScrollViewContainer} from '../themes/styles';
import RoundTextButton from './RoundTextButton';

interface Props {
  children: any;
  button?: ButtonSubmit;
  style?: ScrollViewProps;
  styleContainer?: ScrollViewProps;
  renderTop?: () => void;
}
interface ButtonSubmit {
  text: string;
  onPress: () => void;
  style: ViewStyle;
  disable: true | false;
}
const ScrollContainer = ({
  children,
  style,
  renderTop,
  button,
  styleContainer,
}: Props) => {
  return (
    <Container style={styleContainer}>
      {renderTop && renderTop()}
      <ScrollViewContainer
        showsVerticalScrollIndicator={false}
        contentContainerStyle={[containerStyle, style]}
        style={{paddingBottom: 0, paddingTop: 5}}>
        {children}
        {button && <CreateButton {...button} textSize={'LARGE'} />}
      </ScrollViewContainer>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  background-color: white;
`;

const CreateButton = styled(RoundTextButton)`
  margin-top: auto;
  border-radius: 10px;
  margin-bottom: ${safeBottomPadding}px;
`;

export default ScrollContainer;
