import React from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import CusButton from './Button';
import {handleTextSize, TextSize} from './hanleUI';
interface Props {
  Icon: React.ElementType | JSX.Element;
  text: string;
  onPress?: () => void;
  isFontIcons?: true | false;
  disable?: true | false;
  style?: StyleProp<ViewStyle>;
  styleText?: StyleProp<ViewStyle>;
  styleIcons?: StyleProp<ViewStyle>;
  sizeText?: TextSize;
}
const IconsButtonText: React.FC<Props> = ({
  Icon,
  styleIcons,
  styleText,
  sizeText,
  text,
  style,
  isFontIcons,
  ...props
}) => {
  const Text = handleTextSize(sizeText);
  const IconElement = isFontIcons ? (
    Icon
  ) : (
    <Icon style={styleIcons} width={30} height={30} />
  );
  return (
    <CusButton
      {...props}
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        },
        style,
      ]}>
      {IconElement}
      <Text style={[{marginLeft: 10, color: '#AEAEAF'}, styleText]}>
        {text}
      </Text>
    </CusButton>
  );
};
export const _style_button_round = {
  marginVertical: 10,
  borderRadius: 10,
};
export default IconsButtonText;
