import React from 'react';
import {
  StyleSheet,
  Modal,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {hideAlert} from '../../action/alert';
import {getString} from '../../locate';
const CustomAlertComponent = ({alertData}) => {
  const {
    title,
    message,
    onPressPositiveButton,
    onNegativeButtonPress,
    showAlert,
  } = alertData || {};
  const onPressNeg = () => {
    onNegativeButtonPress && onNegativeButtonPress();
    hideAlert();
  };
  const onPressPos = () => {
    onPressPositiveButton && onPressPositiveButton();
    hideAlert();
  };
  return (
    <Modal
      style={{zIndex: 1}}
      visible={showAlert || false}
      transparent={true}
      animationType={'fade'}>
      <View style={styles.mainOuterComponent}>
        {message ? (
          <View style={styles.mainContainer}>
            {/* First ROw - Alert Icon and Title */}
            <View style={styles.topPart}>
              <Image
                source={require('../../../../assets/ic_notification.png')}
                resizeMode={'contain'}
                style={styles.alertIconStyle}
              />
              <Text style={styles.alertTitleTextStyle}>{`${title}`}</Text>
            </View>
            {/* Second Row - Alert Message Text */}
            <View style={styles.middlePart}>
              <Text style={styles.alertMessageTextStyle}>{`${message}`}</Text>
            </View>
            {/* Third Row - Positive and Negative Button */}
            <View style={styles.bottomPart}>
              <TouchableOpacity
                onPress={onPressPos}
                style={styles.alertMessageButtonStyle}>
                <Text style={styles.alertMessageButtonTextStyle}>
                  {onNegativeButtonPress
                    ? getString('alert', 'yes')
                    : getString('alert', 'confirm')}
                </Text>
              </TouchableOpacity>

              {onNegativeButtonPress && (
                <TouchableOpacity
                  onPress={onPressNeg}
                  style={styles.alertMessageButtonStyle}>
                  <Text style={styles.alertMessageButtonTextStyle}>
                    {getString('alert', 'no')}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        ) : (
          <Loading size={'large'} color={'#fff'} />
        )}
      </View>
    </Modal>
  );
};

const mapStateToProps = state => ({
  alertData: state.alert,
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomAlertComponent);

// export default CustomAlertComponent;

const styles = StyleSheet.create({
  mainOuterComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000088',
  },
  mainContainer: {
    flexDirection: 'column',
    height: '35%',
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#404040',
    // borderWidth: 2,
    // borderColor: '#FF0000',
    borderRadius: 10,
    padding: 4,
  },
  topPart: {
    flex: 0.5,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    // borderWidth: 1,
    // borderColor: '#00FF00',
    paddingHorizontal: 2,
    paddingVertical: 4,
  },
  middlePart: {
    flex: 1,
    width: '100%',
    padding: 4,
    color: '#FFFFFF',
    fontSize: 16,
    marginVertical: 2,
  },
  bottomPart: {
    flex: 0.5,
    width: '100%',
    flexDirection: 'row',
    padding: 4,
    justifyContent: 'space-evenly',
  },
  alertIconStyle: {
    // borderWidth: 1,
    // borderColor: '#cc00cc',
    height: 35,
    width: 35,
  },
  alertTitleTextStyle: {
    flex: 1,
    textAlign: 'justify',
    color: '#FFFFFF',
    fontSize: 18,
    fontWeight: 'bold',
    // borderWidth: 1,
    // borderColor: '#660066',
    padding: 2,
    marginHorizontal: 2,
  },
  alertMessageTextStyle: {
    color: '#FFFFFF',
    textAlign: 'justify',
    fontSize: 18,
    paddingHorizontal: 6,
    paddingVertical: 2,
  },
  alertMessageButtonStyle: {
    width: '30%',
    borderRadius: 10,
    backgroundColor: '#80BFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alertMessageButtonTextStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
});
const Loading = styled.ActivityIndicator``;
