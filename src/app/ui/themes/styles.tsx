import React from 'react';
import {ScrollViewProps, ViewProps} from 'react-native';
import styled from 'styled-components/native';
import {
  isIOS,
  safeBottomPadding,
  safePaddingTop,
  windowHeight,
  windowWidth,
} from '../../../utils/platform';
import getIconWithName from '../utils/Icons';
import _user_color from './Colors';
import CusColor, {_color} from './Colors';
import {_numberCss, _numberStyled} from './dimens';
export const styleScrollContainer = {
  flexGround: 1,
  paddingTop: safePaddingTop,
  marginBottom: safeBottomPadding,
  backgroundColor: _color.background_Container,
};
export const ScrollContainer = styled.FlatList`
  flex-grow: 1;
  padding-top: ${safePaddingTop}px;
  margin-bottom: ${safeBottomPadding}px;
  background-color: ${_color.background_Container};
`;
export const ErrorText = styled.Text`
  margin-top: 5px;
  display: flex;
  font-size: 12px;
  font-weight: 400;
  color: #fa3231;
`;
export const ScrollWithSearch = styled.FlatList`
  flex-grow: 1;
  margin-bottom: ${safeBottomPadding}px;
  background-color: ${_color.white};
`;
export const ScrollViewContainer = styled.ScrollView`
  flex-grow: 1;
  padding-top: ${safePaddingTop}px;
  padding-bottom: ${safeBottomPadding}px;
  background-color: ${_color.white};
`;
export const Container = styled.View`
  flex: 1;
  align-items: center;
  padding-top: ${safePaddingTop}px;
  padding-horizontal: ${_numberStyled['pd-container']};
  background-color: ${_color.white};
`;
export const CenterContainer = styled.View`
  flex: 1;
  height: ${windowHeight}px;
  justify-content: center;
  padding-horizontal: ${_numberStyled['pd-container']};
`;
export const CenterAreaContainer = styled.View`
  justify-content: center;
  align-items: center;
`;
export const RowArea = styled.SafeAreaView`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
`;
export const TextSmall = styled.Text`
  font-size: ${_numberStyled['f-s']};
  text-align: center;
  color: ${_color.white};
  font-weight: 400;
`;
export const TextMedium = styled.Text`
  font-size: ${_numberStyled['f-m']};
  text-align: center;
  color: ${_color.white};
  font-weight: 600;
`;
export const TextLargest = styled.Text`
  font-size: ${_numberStyled['f-largest']};
  text-align: center;
  color: ${_color.white};
  font-weight: 500;
`;
export const TextLarge = styled.Text`
  font-size: ${_numberStyled['f-l']};
  text-align: center;
  color: ${_color.white};
  font-weight: bold;
`;
export const TextTitle = styled.Text`
  font-size: ${_numberStyled['f-title']};
  color: ${_color.title};
  align-self: center;
  margin-end: 8px;
`;
export const Description = styled(TextLarge)`
  color: #aaa;
  font-weight: 500;
  margin-bottom: 20px;
`;
export const shadow_with_color = (color?: string) => `
shadow-color: ${color || _user_color.primaryColor};
border-color:${color || _user_color.primaryColor};
border-width:0.2px;
shadow-radius: 5px;
shadow-offset: 5px 5px;
elevation: ${isIOS ? 2 : 5};
shadow-opacity: 1;
`;
export const ShadowString = `
  ${
    isIOS
      ? `shadow-color: #ccc7c7;
  border-color:#9fa0a0;
  border-width:0.2px;
  shadow-radius: 5px;
  shadow-offset: 0px 5px;
  shadow-opacity: 1;`
      : ''
  }
  elevation: ${isIOS ? 1 : 3};
  margin-bottom:5px;
`;
export const RoundTouch = styled.TouchableOpacity`
  background-color: ${CusColor.primaryColor};
  border-radius: ${_numberStyled['bd-radius']};
  padding: ${_numberStyled['pd-button']};
  margin-vertical: 15px;
  ${ShadowString}
`;
export const TouchWhiteNoShadow = styled.TouchableOpacity`
  border-radius: 15px;
  padding: ${_numberStyled['pd-button']};
  justify-content: center;
  align-items: center;
  margin-vertical: 15px;
`;
export const TouchWhite = styled(TouchWhiteNoShadow)`
  background-color: ${_color.white};
  ${ShadowString}
`;

export const RoundTouchDisable = styled(RoundTouch)`
  background-color: ${CusColor.disableColor};
`;
export const Touch = styled.TouchableOpacity`
  padding: ${_numberStyled['pd-button']};
  margin-vertical: 20px;
`;
export const RoundView = styled.View`
  border-color: ${CusColor.primaryColor};
  border-radius: ${_numberStyled['bd-radius']};
  border-width: 0.5px;
`;
export const RoundViewWithoutBorder = styled.View`
  border-radius: ${_numberStyled['bd-radius']};
  background-color: ${_color.white};
`;
export const RowView = styled.View`
  padding-vertical: 10px;
  flex-direction: row;
`;
export const RowTouch = styled.TouchableOpacity`
  padding-vertical: 10px;
  flex-direction: row;
`;
export const TitleInput = styled(TextMedium)`
  color: #999;
  text-align: left;
  margin-bottom: 5px;
  margin-top: 10px;
  margin-left: 5px;
  text-transform: uppercase;
`;
export const Title = styled(TextTitle)`
  font-weight: bold;
  margin-vertical: 5px;
  margin-left: 5px;
  color: black;
  text-align: left;
`;
export const ViewCard = styled(TouchWhiteNoShadow)`
  background-color: ${_user_color.primaryColor};
  align-items: flex-start;
  margin: 5px;
`;
export const TextName = styled(TextLargest)`
  padding: 10px;
  color: #fff;
  font-weight: bold;
`;
export const TouchPlus = styled(TouchWhiteNoShadow)`
  background-color: #f0f2f6;
  border-radius: 40px;
  position: absolute;
  right: 20px;
  bottom: ${windowHeight / 13}px;
`;
export const containerStyle = {
  flexGrow: 1,
  paddingBottom: 0,
  marginBottom: 0,
  justifyContent: 'flex-start',
  paddingHorizontal: _numberCss['pd-container'],
  backgroundColor: _color.background_Container,
};
export type CheckErrorInput = 'NotEmpty' | 'PositiveNumber';
export interface ErrorCheck {
  value?: string;
  checkError?: CheckErrorInput;
}
export const CheckError = ({value, checkError}: ErrorCheck) => {
  return checkError === 'PositiveNumber'
    ? (!parseInt(value) || parseInt(value) < 0) && (
        <ErrorText>Mục nhập này phải lớn hơn 0</ErrorText>
      )
    : checkError === 'NotEmpty'
    ? !value && <ErrorText>Mục nhập này Không được trống</ErrorText>
    : null;
};
