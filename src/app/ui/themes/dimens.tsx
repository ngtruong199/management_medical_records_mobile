import {windowWidth} from '../../../utils/platform';

const radius: number = 30;
const paddingButton: number = 10;
const fontSmall: number = 12;
const fontMedium: number = 14;
const fontLarge: number = 16;
const font_largest: number = 18;
const fontTitle: number = 22;
const font_large_Title: number = 24;
const paddingContainer: number = 20;
export const _numberStyled = {
  ['bd-radius']: `${radius}px`,
  ['pd-button']: `${paddingButton}px`,
  ['f-s']: `${fontSmall}px`,
  ['f-m']: `${fontMedium}px`,
  ['f-l']: `${fontLarge}px`,
  ['f-largest']: `${font_largest}px`,
  ['f-large-Title']: `${font_large_Title}px`,
  ['f-title']: `${fontTitle}px`,
  ['pd-container']: `${paddingContainer}px`,
};
export const _numberCss = {
  ['bd-radius']: radius,
  ['pd-button']: paddingButton,
  ['f-s']: fontSmall,
  ['f-m']: fontMedium,
  ['f-l']: fontLarge,
  ['f-largest']: font_largest,
  ['f-title']: fontTitle,
  ['f-large-Title']: font_large_Title,
  ['pd-container']: paddingContainer,
};
export const getStyleWrap = (numOfLine: number) => {
  const marginEnd = windowWidth / (numOfLine + 1) / (numOfLine + 2.5);
  return {
    width: windowWidth / (numOfLine + 1),
    height: windowWidth / (numOfLine + 1),
    marginEnd,
  };
};
