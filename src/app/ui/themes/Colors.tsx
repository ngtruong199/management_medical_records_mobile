type ColorType = 'Light' | 'Night';
interface ColorInterFace {
  disableColor: string;
  primaryColor: string;
  secondaryColor: string;
  errorColor: '#B00020';
}
const NightColor: ColorInterFace = {
  disableColor: '#6d7a8c',
  primaryColor: '#F6961F',
  secondaryColor: '#384F8A',
  errorColor: '#B00020',
};
const LightColor: ColorInterFace = {
  disableColor: '#bebebe',
  primaryColor: '#3375F5',
  secondaryColor: '#F6961F',
  errorColor: '#B00020',
};
let _user_color: ColorInterFace = LightColor;
export const setTheme = (colorType: ColorType) => {
  _user_color = colorType === 'Light' ? LightColor : NightColor;
};
export const _color = {
  white: '#fff',
  black: '#000',
  contentBlur: '#A5A5A5',
  title: '#454541',
  background_Container: '#FFFFFF',
  color_component: '#f6f6f8',
  color_bottom: '#f3f3f3',
};
export default _user_color;
