export type SeverAuthType =
  | 'auth/invalid-phone-number'
  | 'auth/popup-closed-by-user';

export type Collection = 'auth';
// export type InAppAuth =
export interface AuthInput {
  phoneNumber?: string;
  callback?: (param: any) => void;
}
