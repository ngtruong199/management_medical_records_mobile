import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import types from '../action/types';
// import {serviceActionHandler} from '../services';
import * as reducers from './reducers';

// The order of middle wares matters.
const middleWares = [thunk];

const reducer = combineReducers(reducers);
const appReducer = (state, action) => {
  const appState = state;
  //   if (action.type === types.SIGN_OUT) {
  //     const {navigator, network} = state;
  //     appState = {navigator, network};
  //   }
  return reducer(appState, action);
};
export const dispatch = ({type, payload = {}}) => {
  store.dispatch({type, payload});
};
const store = compose(applyMiddleware(...middleWares))(createStore)(appReducer);

export default store;
