export interface Auth {
  email?: string;
  password?: string;
  id?: string;
  date?: string;
  address?: string;
  avatar?: string;
  uid?: string;
  token?: string;
  displayName?: string;
  photoURL?: string;
  phoneNumber?: string;
}
export interface AuthData {
  email?: string;
  emailVerified?: boolean;
  isAnonymous?: boolean;
  metadata?: Metadata;
  providerId?: string;
  refreshToken?: string;
  tenantId?: null;
  uid?: string;
}
interface Metadata {
  creationTime?: number;
  lastSignInTime?: number;
}
