export interface HeaderItem {
  name: string;
  icon: string;
  item?: Post[];
  color?: string;
  id: string;
}
export interface Post {
  urlImage: string;
  title: string;
  description?: string;
}
export const header: HeaderItem[] = [
  {
    name: 'General',
    id: '1',
    icon: 'stethoscope',
    color: '#374fd6',
    item: [
      {
        urlImage: 'http://vku.udn.vn/slides/2020/bt.jpg',
        title:
          'VKU tiếp và làm việc với Trường Đại học Thủ Dầu Một - Bình Dương (20/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/12/avatar.jpg&w=400&h=280&q=100',
        title:
          'Họp Hội đồng Khoa học và Đào tạo Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn (25/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/27/avatar.jpg&w=400&h=280&q=100',
        title:
          'Vòng sơ loại Cuộc thi ROBOCAR-2021: ROBOCAR: ROAD TO VKU (27/03/2021)',
        description: 'abc',
      },
    ],
  },
  {
    name: 'Heart',
    id: '1',
    icon: 'heart',
    color: '#c03f3f',
    item: [
      {
        urlImage: 'http://vku.udn.vn/slides/2020/bt.jpg',
        title:
          'VKU tiếp và làm việc với Trường Đại học Thủ Dầu Một - Bình Dương (20/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/12/avatar.jpg&w=400&h=280&q=100',
        title:
          'Họp Hội đồng Khoa học và Đào tạo Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn (25/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/27/avatar.jpg&w=400&h=280&q=100',
        title:
          'Vòng sơ loại Cuộc thi ROBOCAR-2021: ROBOCAR: ROAD TO VKU (27/03/2021)',
        description: 'abc',
      },
    ],
  },
  {
    name: 'Tooth',
    id: '1',
    icon: 'user-md',
    color: '#60C7ED',
    item: [
      {
        urlImage: 'http://vku.udn.vn/slides/2020/bt.jpg',
        title:
          'VKU tiếp và làm việc với Trường Đại học Thủ Dầu Một - Bình Dương (20/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/12/avatar.jpg&w=400&h=280&q=100',
        title:
          'Họp Hội đồng Khoa học và Đào tạo Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn (25/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/27/avatar.jpg&w=400&h=280&q=100',
        title:
          'Vòng sơ loại Cuộc thi ROBOCAR-2021: ROBOCAR: ROAD TO VKU (27/03/2021)',
        description: 'abc',
      },
    ],
  },
  {
    name: 'Hopital',
    id: '1',
    icon: 'virus',
    color: '#3BB6AF',
    item: [
      {
        urlImage: 'http://vku.udn.vn/slides/2020/bt.jpg',
        title:
          'VKU tiếp và làm việc với Trường Đại học Thủ Dầu Một - Bình Dương (20/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/12/avatar.jpg&w=400&h=280&q=100',
        title:
          'Họp Hội đồng Khoa học và Đào tạo Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn (25/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/27/avatar.jpg&w=400&h=280&q=100',
        title:
          'Vòng sơ loại Cuộc thi ROBOCAR-2021: ROBOCAR: ROAD TO VKU (27/03/2021)',
        description: 'abc',
      },
    ],
  },
  {
    name: 'Ambulance',
    id: '1',
    icon: 'ambulance',
    color: '#BD0815',
    item: [
      {
        urlImage: 'http://vku.udn.vn/slides/2020/bt.jpg',
        title:
          'VKU tiếp và làm việc với Trường Đại học Thủ Dầu Một - Bình Dương (20/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/12/avatar.jpg&w=400&h=280&q=100',
        title:
          'Họp Hội đồng Khoa học và Đào tạo Trường Đại học Công nghệ Thông tin và Truyền thông Việt-Hàn (25/03/2021)',
        description: 'abc',
      },
      {
        urlImage:
          'http://vku.udn.vn/images/images.php?src=/uploads/2021/03/27/avatar.jpg&w=400&h=280&q=100',
        title:
          'Vòng sơ loại Cuộc thi ROBOCAR-2021: ROBOCAR: ROAD TO VKU (27/03/2021)',
        description: 'abc',
      },
    ],
  }
];
