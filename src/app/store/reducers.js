// import app from '../reducers/app';
import auth from '../reducers/auth';
import medical_profile from '../reducers/medical-profile';
import alert from '../reducers/alert';
import auth_management from '../reducers/auth-management';
import chat from '../reducers/chat';
import bottom_option from '../reducers/bottom-option';
// import profile from '../reducers/profile';
// import network from '../services/network/store';
// import language from '../reducers/language';
// import information from '../reducers/information';

/*
This file exports the reducers as an object which
will be passed onto combineReducers method at src/app.js
*/
export {
  auth,
  medical_profile,
  alert,
  auth_management,
  chat,
  bottom_option,
  // language,
};
