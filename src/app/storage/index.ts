import AsyncStorage from '@react-native-async-storage/async-storage';

export const setItemStorage = async (key: string, value: any) => {
  try {
    AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log(e);
  }
};
export const getItemStorage = async (
  key: string,
  callback: (value) => void,
) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      callback && callback(value);
      return value;
    }
  } catch (error) {}
};
