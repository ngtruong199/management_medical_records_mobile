import EN from './en';
import VI from './vi';
type languageTYPE = 'en' | 'vi';

let language: any = VI;

function changeToEN() {
  language = EN;
}
function changeToVI() {
  language = VI;
}
function toggleLanguage() {
  language === EN ? changeToVI() : changeToEN();
}
export const getString = (category: string, key: string) => {
  try {
    const text: string = language[category][key];
    return text || 'unknow';
  } catch (error) {
    return 'unknow';
  }
};

export {changeToEN, changeToVI, toggleLanguage};
